/* eslint-env jest */
/* eslint-disable consistent-return */

import { exec } from 'child_process';
import { resolve, join } from 'path';
import { readFileSync, exists } from 'fs';
import io from 'socket.io-client';
import server from '../../src/server/server';

const testPort = 7777;

const repoName = `fiha-t35t-${(new Date()).getTime()}`;

const reposDir = '/tmp';
const testImage = readFileSync(resolve(__dirname, '../../src/static/vf.icon.png'));

const emitCallback = next => (data) => {
  if (!data) return next(new Error('Missing data'));
  const { type, payload } = data;
  if (!data.type) return next(new Error('Missing data type'));

  if (type !== 'SUCCESS') {
    next(new Error(payload || `Emit Callback "${type}" Error`));
  } else {
    next(null, payload);
  }
};

const repoFileExists = (filepath, done) => exists(join(reposDir, repoName, filepath), ok => done(ok
  ? null
  : new Error(`Cannot find ${join(repoName, filepath)} in ${reposDir}`)));

const repoFileNotExists = (filepath, done) =>
  exists(join(reposDir, repoName, filepath), ok => done(ok
    ? new Error(`Found ${join(repoName, filepath)} in ${reposDir}`)
    : null));

describe('server', () => {
  let clientSocket;
  beforeAll((done) => {
    exec(`rm -rf ${reposDir}/fiha-t35t-*`, (err) => {
      if (err) {
        done(err);
        return;
      }
      server.listen(testPort, 'localhost', () => {
        clientSocket = io(`http://localhost:${testPort}`);
        clientSocket.on('connect', done);
        // eslint-disable-next-line no-console
        clientSocket.on('error', (...args) => console.info('error', ...args));
      });
    });
  });
  afterAll((done) => {
    server.close(done);
  });

  it('listens', (done) => {
    expect(clientSocket.connected).toBeTruthy();
    clientSocket.emit('HANDSHAKE', { hello: true }, (action) => {
      if (action.type !== 'HANDSHAKE_ACK') return done(new Error('no valid type'));
      return done();
    });
  });

  it('creates a repository', (done) => {
    clientSocket.emit('REPOSITORY_MAKE', repoName, emitCallback(done));
  });

  it('sets the origin of a repository', (done) => {
    clientSocket.emit('REPOSITORY_SET_REMOTE', {
      // name: 'origin',
      repoName,
      uri: 'git@github.com:zeropaper/fiha-setup-test.git',
    }, emitCallback((err) => {
      if (err) return done(err);
      done();
    }));
  });


  it('adds the worker setup script file to a repository', (done) => {
    const filepath = './worker/setup.js';
    clientSocket.emit('REPOSITORY_UPSERT_WORKER', {
      name: 'some-name',
      repoName,
      type: 'setup',
      content: '// worker setup',
    }, emitCallback((err) => {
      if (err) return done(err);
      repoFileExists(filepath, done);
    }));
  });

  it('adds the worker animation script file to a repository', (done) => {
    const filepath = './worker/animation.js';
    clientSocket.emit('REPOSITORY_UPSERT_WORKER', {
      repoName,
      type: 'animation',
      content: '// worker animation',
    }, emitCallback((err) => {
      if (err) return done(err);
      repoFileExists(filepath, done);
    }));
  });

  it('adds a layer setup script file to a repository', (done) => {
    const filepath = './layers/some-name-setup.js';
    clientSocket.emit('REPOSITORY_UPSERT_LAYER', {
      name: 'some-name',
      repoName,
      type: 'setup',
      content: '// some layer setup',
    }, emitCallback((err) => {
      if (err) return done(err);
      repoFileExists(filepath, done);
    }));
  });

  it('adds a layer animation script file to a repository', (done) => {
    const filepath = './layers/some-name-animation.js';
    clientSocket.emit('REPOSITORY_UPSERT_LAYER', {
      name: 'some-name',
      repoName,
      type: 'animation',
      content: '// some layer animation',
    }, emitCallback((err) => {
      if (err) return done(err);
      repoFileExists(filepath, done);
    }));
  });

  it('adds a image asset to a repository', (done) => {
    const filepath = './assets/vf.icon.png';
    clientSocket.emit('REPOSITORY_UPSERT_ASSET', {
      name: 'vf.icon.png',
      repoName,
      type: 'asset',
      content: testImage,
    }, emitCallback((err) => {
      if (err) return done(err);
      repoFileExists(filepath, done);
    }));
  });

  it('writes the fiha.json file', (done) => {
    const filepath = './fiha.json';
    clientSocket.emit('REPOSITORY_UPSERT_FIHAJSON', {
      repoName,
      content: '{}',
    }, emitCallback((err) => {
      if (err) return done(err);
      repoFileExists(filepath, done);
    }));
  });

  it('makes a commit', (done) => {
    clientSocket.emit('REPOSITORY_COMMIT', {
      repoName,
      message: 'Added stuff',
    }, emitCallback(done));
  });

  it('deletes layer script', (done) => {
    const filepath = './layers/some-name-setup.js';
    clientSocket.emit('REPOSITORY_DELETE_LAYER', {
      name: 'some-name',
      repoName,
      type: 'setup',
    }, emitCallback((err) => {
      if (err) return done(err);
      repoFileNotExists(filepath, done);
    }));
  });

  it('deletes layer script which does not exists', (done) => {
    const filepath = './layers/some-name-setup.js';
    clientSocket.emit('REPOSITORY_DELETE_LAYER', {
      name: 'some-name',
      repoName,
      type: 'setup',
    }, emitCallback((err) => {
      if (err) return done(err);
      repoFileNotExists(filepath, done);
    }));
  });

  it('deletes the a worker script', (done) => {
    const filepath = './worker/animation.js';
    clientSocket.emit('REPOSITORY_DELETE_WORKER', {
      repoName,
      type: 'animation',
    }, emitCallback((err) => {
      if (err) return done(err);
      repoFileNotExists(filepath, done);
    }));
  });

  it('makes an other commit', (done) => {
    clientSocket.emit('REPOSITORY_COMMIT', {
      repoName,
      message: 'Removed stuff',
    }, emitCallback(done));
  });
});
