/* eslint-disable */
describe('Communiation Network', () => {
  it('loads');

  describe('controller', () => {
    it('provides an event listener');
  });

  describe('worker', () => {
    it('provides an event listener');
  });

  describe('client', () => {
    it('provides an event listener');
  });

  describe('server', () => {
    it('provides an event listener');
  });

  describe('action emitted by', () => {
    describe('controller', () => {
      it('are only sent to the worker');
    });

    describe('worker', () => {

    });

    describe('client', () => {
    });

    describe('server', () => {
    });
  });

  describe('action recieved by', () => {
    describe('controller', () => {
    });

    describe('worker', () => {

    });

    describe('client', () => {
    });

    describe('server', () => {
    });
  });
});
