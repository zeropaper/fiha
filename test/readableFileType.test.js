/* eslint-env jest */
import { readableFileType } from '../src/controller/services/gist';

describe('readableFileType', () => {
  it('considers GLTF files readable', () => {
    expect(readableFileType('application/octet-stream', 'example.gltf')).toBe(true);
  });

  it('considers SVG files readable', () => {
    expect(readableFileType('image/svg+xml', 'example.svg')).toBe(true);
  });

  it('considers PNG files not readable', () => {
    expect(readableFileType('image/png', 'example.png')).toBe(false);
  });

  it('considers JPEG files not readable', () => {
    expect(readableFileType('image/jpg', 'example.jpg')).toBe(false);
  });
});
