/* eslint-disable max-len */
/* globals jest, describe, expect, beforeEach, it */
import Backend from '../src/controller/Backend';

// import GistWorkerPlugin from '../src/plugins/gist/worker/GistControllerWorkerPlugin';
// import LocalForageWorkerPlugin from '../src/plugins/localforage/worker/LocalForageControllerWorkerPlugin';
// import WebSocketWorkerPlugin from '../src/plugins/websocket/worker/RepoControllerWorkerPlugin';
import AssetstControllerWorkerPlugin from '../src/plugins/assets/worker/AssetsControllerWorkerPlugin';

const plugins = [
  // GistWorkerPlugin,
  // LocalForageWorkerPlugin,
  // WebSocketWorkerPlugin,
  AssetstControllerWorkerPlugin,
];

const makeBackend = worker => new Backend({
  worker,
  plugins,
});

// const socketMock = () => ({
//   emit: jest.fn(),
// });

const mockWorker = () => {
  const listeners = [];
  const messages = [];
  return {
    addEventListener: jest.fn((name, fn) => {
      listeners.push([name, fn]);
    }),
    postMessage: jest.fn((data) => {
      messages.push(data);
    }),
    importScript: jest.fn(),
    terminate: jest.fn(),
    mockMessage: (data) => {
      listeners.forEach((info) => {
        if (info[0] !== 'message') return;
        info[1]({ data });
      });
    },
    getMockedMessages: () => messages,
  };
};

// probably overkilled
const waitFor = (condition, timeout = 1000) => new Promise((resolve, reject) => {
  let started;
  const interval = setInterval(() => {
    started = started || Date.now();
    const now = Date.now();
    if (now - started >= timeout) {
      reject(new Error(`${timeout}ms Timeout Exceeded`));
      clearInterval(interval);
      return;
    }

    if (condition()) {
      resolve();
      clearInterval(interval);
    }
  }, 1000 / 60);
});

describe('backend', () => {
  let mockedWorker;
  let backend;

  beforeEach(() => {
    mockedWorker = mockWorker();
    backend = makeBackend(mockedWorker);
  });

  it('binds the worker', () => {
    expect(mockedWorker.addEventListener.mock.calls).toHaveLength(1);
    expect(mockedWorker.postMessage.mock.calls).toHaveLength(0);
  });

  describe('action', () => {
    describe('STORAGE_SAVE', () => {
      it('saves a setup', async () => {
        // console.info('before messages', mockedWorker.postMessage.mock.calls);
        expect(() => {
          mockedWorker.mockMessage({
            type: 'STORAGE_SAVE',
          });
        }).not.toThrow();

        mockedWorker.mockMessage({
          type: 'STORAGE_SAVE',
          payload: {
            id: 'test-id',
          },
        });
        expect(mockedWorker.addEventListener.mock.calls).toHaveLength(1);
        expect(mockedWorker.postMessage.mock.calls).toHaveLength(1);
        await waitFor(() => mockedWorker.postMessage.mock.calls.length);
        expect(mockedWorker.postMessage.mock.calls).toHaveLength(3);

        const lastMessage = mockedWorker.postMessage.mock.calls[0][0];
        expect(lastMessage.type).toBe('UPDATE_STATE');
        // console.info('messages', mockedWorker.postMessage.mock.calls[0]);
      });
    });


    describe('STORAGE_LOAD', () => {

    });


    describe('SET_BROADCAST_ID', () => {
      it('requires a payload with id', () => {
        expect(() => {
          mockedWorker.mockMessage({
            type: 'SET_BROADCAST_ID',
          });
        }).toThrow();

        expect(() => {
          mockedWorker.mockMessage({
            type: 'SET_BROADCAST_ID',
            payload: {
              id: 'test-id',
            },
          });
        }).not.toThrow();

        expect(mockedWorker.addEventListener.mock.calls).toHaveLength(1);
        expect(mockedWorker.postMessage.mock.calls).toHaveLength(2);
      });
    });


    describe('BROADCAST', () => {
      it('broadcasts', async () => {
        expect(() => {
          mockedWorker.mockMessage({
            type: 'BROADCAST',
            payload: {
              test: 'test',
            },
          });
        }).not.toThrow();
        const { calls } = backend.broadcastChannel.postMessage.mock;
        const lastBroadcast = calls[calls.length - 1][0];
        expect(lastBroadcast).toBeTruthy();
        expect(lastBroadcast).toEqual({ test: 'test' });
      });
    });


    describe('BROADCAST_FRAME', () => {
      it('broadcasts a frame', async () => {
        expect(() => {
          mockedWorker.mockMessage({
            type: 'BROADCAST_FRAME',
            payload: {
              test: 'test',
            },
          });
        }).not.toThrow();
      });
    });


    describe('UPDATE_DATA', () => {
      it('updates data', async () => {
        expect(() => {
          mockedWorker.mockMessage({
            type: 'UPDATE_DATA',
            payload: {
              test: 'test',
            },
          });
        }).not.toThrow();
      });
    });
  });
});
