/* globals describe, expect, it */
import functionCompiler from '../src/display/function-compiler';

describe('functionCompiler', () => {
  it('compiles valid code', () => {
    // eslint-disable-next-line
    const body = 'return `arg1: ${arg1}, arg2: ${arg2}`';
    const defaultFunc = () => {};
    const onExecutionError = () => {};
    const args = ['arg1', 'arg2'];
    const compiled = functionCompiler({
      name: 'test1',
      body,
      fallback: defaultFunc,
      onExecutionError,
      argumentNames: args,
    });
    expect(typeof compiled).toEqual('function');
    expect(compiled('a', 'b')).toEqual('arg1: a, arg2: b');
  });

  it('compiles invalid code to logger', () => {
    const body = 'b00m*!?!?';
    const defaultFunc = () => {};
    const args = ['arg1', 'arg2'];
    const compiled = functionCompiler({
      name: 'test2',
      body,
      fallback: defaultFunc,
      argumentNames: args,
    });
    expect(typeof compiled).toEqual('function');
    expect(compiled('a', 'b')).toEqual(undefined);
  });

  describe('compiled function', () => {
    it('cannot access "this"', () => {
      const compiled = functionCompiler({ name: 'test3', body: 'return this' });
      const result = compiled();
      expect(result).toEqual(undefined);
    });

    [
      'global',
      'top',
      'window',
      'frame',
      'document',
      'history',
    ].forEach((name) => {
      it(`cannot access the ${name} object`, () => {
        const compiled = functionCompiler({ name: 'test4', body: `return ${name}` });
        const result = compiled();
        expect(result).toEqual(undefined);
      });
    });
  });
});
