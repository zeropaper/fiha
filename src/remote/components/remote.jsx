import React from 'react';
import io from 'socket.io-client'; // eslint-disable-line
import './remote.scss';

const {
  addEventListener,
  removeEventListener,
  requestAnimationFrame,
} = window;

export default class RemoteUI extends React.Component {
  constructor(props) {
    super(props);

    this.initSocket();
    this.animate();
  }

  state = {
    connected: false,
    name: null,
  };

  componentDidMount() {
    this.mounted = true;
    if (this.socket) {
      this.socket.connect();
    }

    [
      'deviceorientation',
      'devicemotion',
      'mousemove',
      'keyup',
      'touchmove',
      'touchend',
    ].forEach(name => addEventListener(name, this[`${name}Listener`], {
      passive: true,
      capture: false,
    }));
  }

  componentWillUnmount() {
    this.mounted = false;
    if (this.socket) {
      this.socket.disconnect();
    }

    [
      'deviceorientation',
      'devicemotion',
      'mousemove',
      'keyup',
      'touchmove',
      'touchend',
    ].forEach(name => removeEventListener(name, this[`${name}Listener`], {
      passive: true,
      capture: false,
    }));
  }

  setName = evt => this.mounted && this.setState({ name: evt.target.value });

  setConnected = () => this.mounted && this.setState({ connected: true });

  setDisconnected = () => this.mounted && this.setState({ connected: false });

  data = {
    touches: [],
    motion: {},
    orientation: {},
    mouse: {},
    keys: {},
  };

  emit = (action) => {
    if (!this.socket) return;
    this.socket.emit(action.type, action.payload);
  };

  updateRemote = (payload) => {
    this.emit({
      type: 'UPDATE_REMOTE',
      payload,
    });
  };

  initSocket = () => {
    if (!process.env.FIHA_SOCKET_CLIENT) return;
    this.socket = io(process.env.FIHA_SOCKET_CLIENT, { autoConnect: false });
    this.socket.on('connect', this.setConnected);
    this.socket.on('reconnect', this.setConnected);
    this.socket.on('disconnect', this.setDisconnected);
  };

  refCanvas = (el) => {
    if (el) {
      this.ctx = el.getContext('2d');
      this.ctx.canvas.width = el.parentNode.clientWidth;
      this.ctx.canvas.height = el.parentNode.clientHeight;
    } else {
      this.ctx = null;
    }
  };

  drawTouch = (touch) => {
    const { ctx } = this;
    ctx.beginPath();
    ctx.arc(touch.x, touch.y, 20, 0, Math.PI * 2);
    ctx.closePath();
    ctx.fill();
  };

  animate = () => {
    if (!this.ctx) {
      requestAnimationFrame(this.animate);
      return;
    }

    const { ctx } = this;
    ctx.clearRect(0, 0, ctx.canvas.width, ctx.canvas.height);
    ctx.fillStyle = 'white';
    this.data.touches.forEach(this.drawTouch);
    this.updateRemote({
      ...this.data,
      name: this.state.name,
    });
    requestAnimationFrame(this.animate);
  };

  devicemotionListener = (evt) => {
    this.data.motion = {
      x: evt.accelerationIncludingGravity.x,
      y: evt.accelerationIncludingGravity.y,
      z: evt.accelerationIncludingGravity.z,
      alpha: evt.rotationRate.alpha,
      beta: evt.rotationRate.beta,
      gamma: evt.rotationRate.gamma,
    };
  };

  deviceorientationListener = (evt) => {
    this.data.orientation = {
      absolute: evt.absolute,
      alpha: evt.alpha,
      beta: evt.beta,
      gamma: evt.gamma,
    };
  };

  mousemoveListener = (evt) => { // eslint-disable-line
    // // eslint-disable-next-line no-console
    // console.log('mousemoveListener', ...Object.keys(evt));
  }

  keyupListener = (evt) => { // eslint-disable-line
    // // eslint-disable-next-line no-console
    // console.log('keyupListener', ...Object.keys(evt));
  };

  touchmoveListener = (evt) => {
    const changes = [];
    for (let t = 0; t < evt.touches.length; t += 1) {
      const touch = evt.touches.item(t);
      changes.push({
        id: touch.identifier,
        x: touch.pageX,
        y: touch.pageY,
      });
    }
    if (this.data.touches === changes) return;
    this.data.touches = changes;
  };

  touchendListerner = () => {
    this.data.touches = [];
  };

  render() {
    const { connected } = this.state;
    return [
      <div key="overlay" className="overlay">
        <div>
          Server:&nbsp;
          <span
            style={{
              color: connected ? 'lightgreen' : 'lightred',
            }}
          >
            {connected ? 'connected' : 'disconnected'}
          </span>
        </div>
        <div>
          <input className="form-control form-control-sm" type="text" onChange={this.setName} />
        </div>
        <div>ID: {this.socket.id}</div>
      </div>,
      <canvas key="canvas" ref={this.refCanvas} />,
    ];
  }
}

