import React from 'react';
import { render } from 'react-dom';
import { AppContainer } from 'react-hot-loader';
import Component from './components/remote';

window.history.scrollRestoration = 'manual';
window.onbeforeunload = function preventScrollRefresh() {
  window.scrollTo(0, 0);
};

const rootEl = document.getElementById('root');

const load = () => (render(
  <AppContainer>
    <Component />
  </AppContainer>,
  rootEl,
));

const handleHash = () => {
  rootEl.className = 'fiha fiha--remote';
  load();
};

window.addEventListener('hashchange', handleHash);

if (module.hot) {
  module.hot.accept(load);
}

handleHash();
