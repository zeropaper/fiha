import ControllerPlugin from '../../ControllerPlugin';

export default class KeyboardControllerPlugin extends ControllerPlugin {
  static storeId = 'keyboard';

  // tabs = [
  //   {
  //     title: 'Keyboard',
  //     component: () => (null),
  //   },
  // ];

  mount = () => {
    this.log('mounting');
    document.body
      .addEventListener('keydown', this.listener, this.listenerOptions);
    document.body
      .addEventListener('keyup', this.listener, this.listenerOptions);
    return this.unmount;
  };

  unmount = () => {
    this.log('unmounting');
    document.body
      .removeEventListener('keydown', this.listener, this.listenerOptions);
    document.body
      .removeEventListener('keyup', this.listener, this.listenerOptions);
  };

  listenerOptions = {
    capture: false,
    passive: true,
  };

  values = {};

  listener = (evt) => {
    const editor = document.getElementById('brace-editor');
    const inEditor = editor && editor.contains(evt.target);

    if (inEditor) return;

    const signature = [
      evt.key,
      evt.ctrlKey ? 'c' : null,
      evt.shiftKey ? 's' : null,
      evt.altKey ? 'a' : null,
    ]
      .filter(v => v)
      .map(v => v.toLowerCase(v))
      .reduce((uniques, val) => {
        if (!uniques.includes(val)) uniques.push(val);
        return uniques;
      }, [])
      .join('-');

    if (this.values[signature] === evt.type) return;
    this.values[signature] = evt.type;

    const val = evt.type === 'keydown' ? 127 : 0;
    this.log('evt', signature, val);
    this.setData({ [signature]: val });
    // to allow UI stuff as well?
    // this.setState(({ [`keyboard_${signature}`]: val }));
  };
}
