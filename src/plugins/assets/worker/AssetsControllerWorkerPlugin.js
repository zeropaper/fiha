import dataURI2Blob from '../../../util/dataURI2Blob';
import blobURI2DataURI from '../../../util/blobURI2DataURI';
import uniqueBy from '../../../util/uniqueBy';
import BackendPlugin from '../../BackendPlugin';

export const prepareAssetsForStorage = assets => Promise
  .all(assets.map(async ({ preview, ...asset }) => ({
    ...asset,
    dataURI: asset.dataURI || await blobURI2DataURI(preview),
  })));

export default class AssetsControllerWorkerPlugin extends BackendPlugin {
  storeId = 'assets';

  reducer = (state = [], action) => {
    let futureState = [];

    switch (action.type) {
      case 'ASSETS_REPLACE':
        this.broadcast(action);
        return action.payload;

      case 'ASSETS_UPDATE':
        futureState = uniqueBy([...state, ...action.payload], 'name');
        this.broadcast({
          type: action.type,
          payload: futureState,
        });
        return futureState;

      case 'ASSET_REMOVE':
        futureState = (state || [])
          .filter(asset => asset.name !== action.payload);
        this.broadcast({
          type: action.type,
          payload: futureState,
        });
        return futureState;

      default:
        return state;
    }
  };

  currentSetup = () => ({ assets: this.state.assets || [] });

  storeReset = data => this.dispatch({
    type: 'ASSETS_UPDATE',
    payload: (data.assets || this.state.assets).map(asset => ({
      ...asset,
      preview: URL.createObjectURL(dataURI2Blob(asset.dataURI, asset.type)),
    })),
  });

  prepareAssetsForStorage = () => prepareAssetsForStorage(this.state.assets);
}
