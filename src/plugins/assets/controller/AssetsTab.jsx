import React from 'react';
import Dropzone from 'react-dropzone';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

import blob2DataURI from '../../../util/blob2DataURI';
import './AssetsTab.scss';

const Preview = ({ preview, name, type }) => {
  let content = null;

  if (type.indexOf('image/') === 0) {
    content = <img src={preview} alt={`${name} preview`} />;
  }

  return (
    <div className="assets-manager__item-preview">
      {content}
    </div>
  );
};

Preview.propTypes = {
  name: PropTypes.string.isRequired,
  type: PropTypes.string.isRequired,
  preview: PropTypes.string.isRequired,
};

const Item = ({ file, remove, addSnippet }) => {
  const { name, preview, type } = file;

  return (
    <div className="col col-md-6 col-xl-4 assets-manager__item">
      <div className="assets-manager__item-inner">
        <button
          onClick={remove}
          className="assets-manager__item-remove"
          title="Remove"
        >
          <span>Remove</span>
        </button>

        <Preview {...{ preview, name, type }} />

        <div className="assets-manager__item-type text-center text-truncate py-1">
          {type}
        </div>

        <div className="assets-manager__item-name text-center text-truncate pb-1">
          {name}
        </div>

        <div className="text-center">
          <button
            onClick={addSnippet}
            className="assets-manager__item-snippet btn btn-sm btn-link"
            title="Add 'read' snippet"
          >
            <span>Add snippet</span>
          </button>

          <a
            className="assets-manager__item-download btn btn-sm btn-link"
            title="Download file"
            href={file.dataURI}
            download={file.name}
            type={file.type}
          >
            <span>Download</span>
          </a>
        </div>
      </div>
    </div>
  );
};

Item.propTypes = {
  file: PropTypes.object.isRequired,
  remove: PropTypes.func.isRequired,
  addSnippet: PropTypes.func.isRequired,
};

class AssetsManager extends React.Component {
  onDrop = async (files) => {
    await Promise.all(files.map(async (file, idx) => {
      // eslint-disable-next-line
      files[idx].dataURI = await blob2DataURI(file);
    }));

    this.props.updateAssets([
      ...this.props.files,
      ...files.map(file => ({
        name: file.name,
        type: file.type,
        size: file.size,
        lastModified: file.lastModified,
        dataURI: file.dataURI,
        preview: URL.createObjectURL(file),
      })),
    ]);
  };

  onRemove = name => (evt) => {
    evt.preventDefault();
    this.props.removeAsset(name);
  };

  render() {
    return (
      <section className="container-fluid assets-manager">
        <div className="row">
          <div className="col text-center py-3">
            <Dropzone onDrop={this.onDrop}>
              {({ getRootProps, getInputProps }) => (
                <button
                  className="assets-manager__dropzone btn btn-l btn-info"
                  {...getRootProps()}
                >
                  <input {...getInputProps()} />
                  <div>Drop your files or click here</div>
                </button>
              )}
            </Dropzone>
          </div>
        </div>

        <div className="assets-manager__list">
          <div className="assets-manager__list-inner">
            {(this.props.files || []).map(file => (
              <Item
                key={file.name}
                file={file}
                remove={this.onRemove(file.name)}
                addSnippet={() => this.props.addSnippet(`read('asset:${file.name}')`)}
              />
            ))}
          </div>
        </div>
      </section>
    );
  }
}

AssetsManager.propTypes = {
  files: PropTypes.array,
  updateAssets: PropTypes.func.isRequired,
  removeAsset: PropTypes.func.isRequired,
  addSnippet: PropTypes.func.isRequired,
};

AssetsManager.defaultProps = {
  files: [],
};

const mapStateToProps = ({ assets }) => ({
  files: assets,
});

export default connect(mapStateToProps)(AssetsManager);
