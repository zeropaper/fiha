import React, { lazy, Suspense } from 'react';

import ControllerPlugin from '../../ControllerPlugin';

const AssetsTab = lazy(() => import('./AssetsTab'));

export default class AssetsControllerPlugin extends ControllerPlugin {
  tabs = [
    {
      title: 'Assets',
      /* eslint-disable */
      component: props => (
        <Suspense fallback={<div>Loading...</div>}>
          <AssetsTab
            updateAssets={this.updateAssets}
            removeAsset={this.removeAsset}
            addSnippet={props.addSnippet}
          />
        </Suspense>
      ),
      /* eslint-enable */
    },
  ];

  updateAssets = (assets) => {
    this.log('updateAssets');

    this.emit({
      type: 'ASSETS_UPDATE',
      payload: assets.map(({
        preview,
        name,
        size,
        type,
        lastModified,
      }) => ({
        preview,
        name,
        size,
        type,
        lastModified,
      })),
    });
  };

  removeAsset = name => this.emit({ type: 'ASSET_REMOVE', payload: name });
}
