import ControllerPlugin from '../../ControllerPlugin';

export default class MouseControllerPlugin extends ControllerPlugin {
  static storeId = 'mouse';

  signalNames = [
    'mouse-x',
    'mouse-y',
  ];

  // tabs = [
  //   {
  //     title: 'Mouse',
  //     component: () => (null),
  //   },
  // ];

  mount = () => {
    const el = document.getElementById('fiha-display');
    this.log('mount el', el);
    if (!el) return () => {};

    this.element = el;

    const opts = {
      capture: false,
      passive: true,
    };

    el.addEventListener('mousemove', this.listener, opts);

    return () => el
      .removeEventListener('mousemove', this.listener, opts);
  };

  listenerOptions = {
    capture: false,
    passive: true,
  };

  listener = (evt) => {
    const {
      clientWidth,
      clientHeight,
    } = this.element;
    const data = {
      'mouse-x': (evt.offsetX / clientWidth) * 127,
      'mouse-y': (evt.offsetY / clientHeight) * 127,
    };

    this.log('mouse', data['mouse-x'], data['mouse-y']);
    this.setData(data);
  };
}
