import FihaPlugin from './FihaPlugin';

export default class BackendPlugin extends FihaPlugin {
  constructor(backend) {
    super();
    this.backend = backend;

    if (this.initialize) this.initialize();
  }

  // eslint-disable-next-line class-methods-use-this
  initialize() {}

  get pluginId() {
    return this.storeId || this.constructor.name;
  }

  /**
   * sets the worker data by merging
   * @example this.data = { something: 1 };
   */
  set data(data) {
    this.backend.set(data);
  }

  /**
   * gets the worker data
   * @example const allData = this.data;
   */
  get data() {
    return this.backend.data;
  }

  /**
   * gets the worker (redux) state
   * @example const workerState = this.state;
   * @see dispatch
   */
  get state() {
    return this.backend.state;
  }

  /**
   * sends an action to the UI
   * @example this.emit(action);
   */
  get emit() {
    return this.backend.emit;
  }

  /**
   * broadcasts an action (to the UI and all layers)
   * @example this.broadcast(action)
   */
  get broadcast() {
    return this.backend.broadcast;
  }

  /**
   * dispatches an action to the worker state and triggers UI update
   * @example this.dispatch(action)
   * @see state
   * @see batchDispatch
   */
  get dispatch() {
    return this.backend.store.dispatch;
  }

  /**
   * dispatches several actions to the worker state and triggers only 1 UI update
   * @example this.batchDispatch([action1, action2, action3])
   * @see state
   * @see dispatch
   */
  get batchDispatch() {
    return this.backend.batchDispatch;
  }

  get actionCallback() {
    return this.backend.actionCallback;
  }

  /**
   * sends a success notification to the UI
   * @example this.notifySuccess('Something went OK');
   */
  get notifySuccess() {
    return this.backend.notifySuccess;
  }

  /**
   * sends a failure notification to the UI
   * @example this.notifyFailure('Something went wrong');
   */
  get notifyFailure() {
    return this.backend.notifyFailure;
  }

  /**
   * processes an action on the worker
   * @example this.process(action)
   */
  get process() {
    return this.backend.workerReducer;
  }

  executeHook = (name, ...args) => {
    const hookProp = this[name];
    if (typeof hookProp === 'function') {
      return hookProp(...args);
    }

    if (typeof hookProp !== 'undefined') {
      return hookProp;
    }

    return null;
  };

  /* Hooks */

  /*
  scriptArguments = () => ({});

  currentSetup = () => ({});

  storeReset = () => {};

  broadcastPreReducer = () => {};

  broadcastPostReducer = () => {};

  workerPreReducer = () => {};

  workerPostReducer = () => {};
  */
}
