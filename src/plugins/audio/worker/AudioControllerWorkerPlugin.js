import BackendPlugin from '../../BackendPlugin';
import MathTools from '../../../display/math-tools';

const buffer = [];
const {
  arrayAvg,
  arrayDiff,
  arrayMax,
  arrayMin,
  arraySum,
} = MathTools;

let taps = [];
const bpmTest = () => {
  const tap = Date.now();
  taps.unshift(tap);
  taps = taps.filter((stamp, s) => s <= 279 && stamp > tap - 60000000);
  if (taps.length <= 4) return 60;
  const diffAll = (tap - taps[taps.length - 1]) / taps.length;
  const diffs = taps.map((val, v) => (tap - val) * (1 / ((v + 1) * 2)));
  const result = (arrayAvg(diffs) + diffAll) * 0.5;
  return result;
};


export default class Audio extends BackendPlugin {
  compute = (arr = [], prev = []) => ({
    vals: arr,
    diffs: arr.map((v, i) => (prev[i] ? prev[i] - v : v)),
    avg: arrayAvg(arr),
    diff: arrayDiff(arr),
    min: arrayMin(arr),
    max: arrayMax(arr),
    sum: arraySum(arr),
  });

  workerPostReducer = (action) => {
    const {
      actionCallback,
      data: { audio },
    } = this.backend;
    const { type, meta } = action;

    const now = performance.now();
    if (!this.captureStart && type === 'AUDIO_CAPTURE') {
      this.captureData = [];
      this.captureStart = now;
      this.captureCbId = meta.callback;
      console.info('Worker starts capture', this.captureCbId);
    }

    if (this.captureStart >= now - (1000 * 5)) {
      // console.info('Worker capture....');
      this.captureData.push(audio);
    } else if (this.captureStart) {
      this.captureStart = 0;
      console.info('Worker ends capture', this.captureCbId, this.captureData.length);
      actionCallback({
        type: 'AUDIO_CAPTURE',
        meta: { callback: this.captureCbId },
      }, this.captureData);
      this.captureCbId = null;
    }
  }

  processData = () => {
    const {
      data,
      data: { now, volume, frequencies },
    } = this.backend;

    const [last = { frequencies: [], volume: [], now: 0 }] = buffer;
    const current = {
      now,
      lastStamp: now - (last && +last.now),
      volume,
      frequencies,
      taps,
    };

    buffer.unshift(current);
    buffer.splice(699, 1);

    // TODO: deprecate this
    data.volume = volume;
    data.frequencies = frequencies;

    data.audio = {
      bpm: (data.audio || {}).bpm || 120,
      volume: this.compute(volume || [], last.volume || []),
      frequencies: this.compute(frequencies || [], last.frequencies || []),
      lastStamp: current.lastStamp,
      now,
    };

    const { prevVal } = this;
    const val = data.audio.frequencies.avg;
    this.skip = this.skip || 0;
    if (!this.skip && prevVal < val) {
      data.audio.beat = val - prevVal;
      data.audio.bpm = bpmTest();
      this.skip = 2;
      // console.info('data.audio.beat', data.audio.beat);
    }
    if (this.skip) {
      this.skip -= 1;
      if (!this.skip) {
        data.audio.beat = 0;
      }
    }
    this.prevVal = val;
  };
}
