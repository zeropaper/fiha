/* eslint-disable react/prop-types */
import React, { lazy, Suspense } from 'react';

import ControllerPlugin from '../../ControllerPlugin';

const AudioTab = lazy(() => import('./AudioTab'));

export default class AudioControllerPlugin extends ControllerPlugin {
  static storeId = 'audio';

  constructor(context) {
    super(context);

    this.settings = {
      minDecibels: -180,
      maxDecibels: 120,
      smoothingTimeConstant: 0.85,
      fftSize: 1024,
      ...(context.audio || {}),
    };

    this.state = { bpm: 120 };
  }

  tabs = [
    {
      title: 'Audio',
      component: props => (
        <Suspense fallback={<div>Loading...</div>}>
          <AudioTab
            {...props}
            audio={this}
            onBpmChange={this.onBpmChange}
            bpm={props.controllerState.bpm}
          />
        </Suspense>
      ),
    },
  ];

  signalNames = [
    ['frequencies', []],
    ['volume', []],
  ];

  get frequencies() {
    const { analyser, freqArray } = this;
    analyser.getByteFrequencyData(freqArray);
    return Array.from(freqArray);
  }

  // aka timeDomain
  get volume() {
    const { analyser, volArray } = this;
    analyser.getByteTimeDomainData(volArray);
    return Array.from(volArray);
  }

  get data() {
    const { volume, frequencies } = this;
    return {
      volume,
      frequencies,
    };
  }

  mount = () => {
    const context = new AudioContext();
    this.context = context;
    this.analyser = context.createAnalyser();

    const eventOpts = {
      capture: false,
      passive: true,
    };

    window.addEventListener('mousemove', this.resume, eventOpts);

    context.addEventListener('statechange', () => {
      if (context.state !== 'running') return;

      window.removeEventListener('mousemove', this.resume, eventOpts);
    }, eventOpts);

    this.setupAnalyzer();

    const intervalId = setInterval(() => this.setData(this.data), 8);

    return () => {
      clearInterval(intervalId);
      // eslint-disable-next-line no-console
      context.close().catch(err => console.warn(err));
      window.removeEventListener('mousemove', this.resume, eventOpts);
    };
  };

  onBpmChange = (bpm) => {
    this.setData({ bpm });
    this.setState({ bpm });
    this.emit({
      type: 'SET_BPM',
      payload: { bpm },
    });
  };

  resume = () => this.context.resume();

  setupAnalyzer = () => {
    const { analyser } = this;
    const {
      minDecibels,
      maxDecibels,
      smoothingTimeConstant,
      fftSize,
    } = this.settings;

    try {
      analyser.minDecibels = minDecibels;
      analyser.maxDecibels = maxDecibels;
      analyser.smoothingTimeConstant = smoothingTimeConstant;
      analyser.fftSize = fftSize;
    } catch (err) {
      // eslint-disable-next-line no-console
      console.warn(err);
    }

    this.freqArray = new Uint8Array(analyser.frequencyBinCount);
    this.volArray = new Uint8Array(analyser.frequencyBinCount);

    this.connectAudio();
  };

  connectAudio = () => {
    if (this.source) return;
    const capture = { audio: true };
    const { context, analyser } = this;

    const error = (err) => {
      // eslint-disable-next-line no-console
      console.warn(err);
    };
    const success = (stream) => {
      const source = context.createMediaStreamSource(stream);
      source.connect(analyser);
      this.source = source;
    };

    if (navigator.mediaDevices && navigator.mediaDevices.getUserMedia) {
      navigator.mediaDevices.getUserMedia(capture).then(success).catch(error);
    } else if (navigator.getUserMedia) {
      navigator.getUserMedia(capture, success, error);
    }
  };

  updateSettings = (settings) => {
    this.settings = {
      ...this.settings,
      ...settings,
    };

    this.setupAnalyzer();
  };
}
