const { join } = require('path');
const devIp = require('dev-ip')().pop();

const {
  NODE_ENV,
  HOME,
  FIHA_SETUP_REPOS_PATH,
  PUBLIC_URL: feOrigin = 'http://localhost:8080',
  HOST = devIp || '0.0.0.0',
  PORT = '9090',
} = process.env;

const reposDir = NODE_ENV === 'test'
  ? '/tmp'
  : (
    FIHA_SETUP_REPOS_PATH
    || join(HOME, 'fiha-repositories')
  );

module.exports = {
  reposDir,
  feOrigin,
  beURL: `http://${HOST}:${PORT}`,
};
