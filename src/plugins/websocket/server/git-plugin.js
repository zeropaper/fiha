const { dirname, join, sep } = require('path');
const fs = require('fs');
const mime = require('mime');
const watch = require('glob-watcher');
const { exec } = require('child_process');
const glob = require('glob');

const { reposDir, beURL } = require('./config');

const cwd = repoName => join(reposDir, repoName);

const err2res = (err, se, so) => ({
  type: 'ERROR',
  payload: err.stack
    || `${so}\n${se}`.trim()
    || (new Error('...')).stack,
});

// eslint-disable-next-line consistent-return
const execDone = (done) => {
  const { stack } = (new Error('Execution Error'));

  return (err, so, se) => {
    if (err) {
      const error = {
        ...err,
        execStack: stack,
      };
      // eslint-disable-next-line no-console
      console.info(' - - - - - - - - - \n%s:\n%s\n%s\n%s\n%s\n - - - - - - - - - ', err.cmd, err.message, stack, so, se);
      done(err2res(error, se, so));
      return;
    }

    done({
      type: 'SUCCESS',
      payload: {
        stdout: so,
        stderr: se,
      },
    });
  };
};

const repoExec = (repoName, cmd, done) => exec(cmd, { cwd: cwd(repoName) }, done);

const repoExists = (repoName, done) => fs.exists(join(reposDir, repoName, '.git'), done);

// eslint-disable-next-line consistent-return
const ensureRepo = (repoName, done) => repoExists(repoName, (exists) => {
  if (exists) return done();

  const repoDir = cwd(repoName);
  exec(`mkdir -p ${repoDir} && cd ${repoDir} && git init`, done);
});

// eslint-disable-next-line consistent-return
const saveFile = (repoName, filepath, content, done) => ensureRepo(repoName, (err) => {
  if (err) return done(err);

  repoExec(
    repoName,
    `mkdir -p ${dirname(filepath)}`,
    // eslint-disable-next-line consistent-return, no-shadow
    (err) => {
      if (err) return done(err);

      fs.writeFile(join(cwd(repoName), filepath), content, done);
    }); // eslint-disable-line
});

const upsertFile = ({
  repoName,
  type,
  name,
  content,
}, done) => saveFile(repoName, type ? join(type, name) : name, content, execDone(done));

const deleteFile = ({
  repoName,
  type,
  name,
}, done) => {
  const filepath = join(cwd(repoName), join(type, name));
  // eslint-disable-next-line consistent-return
  fs.stat(filepath, (err) => {
    if (err) return done({ type: 'SUCCESS' });

    // eslint-disable-next-line no-shadow
    fs.unlink(filepath, (err) => {
      if (err) {
        done(err2res(err));
        return;
      }
      done({ type: 'SUCCESS' });
    });
  });
};

const fileInfo = (filepath, stat) => {
  const [repoName, type, filename] = filepath.split(sep);
  if (type === 'fiha.json') {
    return {
      type: 'fihajson',
      role: 'fihajson',
      repoName,
      filename: type,
    };
  }

  if ((stat && stat.isDirectory()) || !filename) return false;

  if (type === 'assets') {
    return {
      type,
      mimeType: mime.getType(filename),
      repoName,
      filename,
      role: 'asset',
      id: filename,
    };
  }

  const nameParts = filename.split('.').slice(0, -1).join('.').split('-');
  const role = nameParts.pop();
  return {
    id: nameParts.join('-'),
    repoName,
    type,
    role,
    filename,
  };
};

const watchers = {};
const onWorkerConnection = (sendWorker, dir = reposDir) => {
  if (watchers[dir]) return watchers[dir];
  // eslint-disable-next-line no-console
  console.info('create watcher for %s', dir);
  const watcher = watch(['**/*'], { cwd: dir });
  watchers[dir] = watcher;

  watcher.on('change', (filepath, stat) => {
    const info = fileInfo(filepath, stat);
    if (!info) return;

    if (info.types === 'assets') {
      sendWorker({
        type: 'REPO_CHANGE_FILE',
        payload: {
          mimeType: info.mimeType,
          type: info.type,
          role: info.role,
          repoName: info.repoName,
          id: info.id,
          url: `${beURL}/download?repo=${info.repoName}&directory=${info.type}&name=${info.id}`,
        },
      });
      return;
    }

    fs.readFile(join(reposDir, filepath), (err, buffer) => {
      if (err) return;
      sendWorker({
        type: 'REPO_CHANGE_FILE',
        payload: {
          mimeType: info.mimeType,
          type: info.type,
          role: info.role,
          repoName: info.repoName,
          id: info.id,
          buffer,
        },
      });
    });
  });

  watcher.on('add', (filepath, stat) => {
    const info = fileInfo(filepath, stat);
    if (!info) return;
    // eslint-disable-next-line no-console
    console.info('%s added in %s', filepath, dir);
    fs.readFile(join(reposDir, filepath), (err, buffer) => {
      if (err) return;
      sendWorker({
        type: 'REPO_ADD_FILE',
        payload: {
          mimeType: info.mimeType,
          type: info.type,
          role: info.role,
          repoName: info.repoName,
          id: info.id,
          buffer,
        },
      });
    });
  });

  watcher.on('unlink', (filepath, stat) => {
    const info = fileInfo(filepath, stat);
    if (!info) return;
    // eslint-disable-next-line no-console
    console.info('%s removed in %s', filepath, dir);
    sendWorker({
      type: 'REPO_UNLINK_FILE',
      payload: {
        type: info.type,
        role: info.role,
        repoName: info.repoName,
        id: info.id,
      },
    });
  });

  return watcher;
};

const readSetup = async (repoName) => {
  // eslint-disable-next-line
  const setup = require(join(reposDir, repoName, 'fiha.json'));

  const layers = await Promise.all(setup.layers.map(layer => (new Promise((res) => {
    fs.readFile(join(reposDir, repoName, 'layers', `${layer.id}-setup.js`), 'utf8', (err, setupScript) => {
      // eslint-disable-next-line no-shadow
      fs.readFile(join(reposDir, repoName, 'layers', `${layer.id}-animation.js`), 'utf8', (err, animationScript) => {
        res({
          ...layer,
          setup: setupScript || '',
          animation: animationScript || '',
        });
      });
    });
  }))));

  return new Promise((res) => {
    fs.readFile(join(reposDir, repoName, 'worker', 'setup.js'), 'utf8', (err, setupScript) => {
      // eslint-disable-next-line no-shadow
      fs.readFile(join(reposDir, repoName, 'worker', 'animation.js'), 'utf8', (err, animationScript) => {
        res({
          ...setup,
          layers,
          signal: {
            setup: setupScript || '',
            animation: animationScript || '',
          },
        });
      });
    });
  });
};

// eslint-disable-next-line no-unused-vars
const actions = (sendWorker) => {
  const returned = {};

  returned.HANDSHAKE = (data, done) => {
    if (!done) return;

    done({
      type: 'HANDSHAKE_ACK',
      payload: { check: true },
    });
  };

  returned.REPOSITORY_MAKE = (repoName, done) => ensureRepo(repoName, execDone(done));

  returned.REPOSITORY_SET_REMOTE = ({
    repoName,
    uri,
    name = 'origin',
  }, done) => repoExec(repoName, `git remote add ${name} ${uri}`, execDone(done));

  // returned.REPOSITORY_UPSERT_FILE = upsertFile;

  returned.REPOSITORY_UPSERT_ASSET = ({
    repoName,
    name,
    content,
  }, done) => upsertFile({
    repoName,
    type: 'assets',
    name,
    content,
  }, done);

  returned.REPOSITORY_DELETE_ASSET = ({
    repoName,
    name,
  }, done) => deleteFile({
    repoName,
    type: 'assets',
    name,
  }, done);

  returned.REPOSITORY_UPSERT_FIHAJSON = ({
    repoName,
    content,
  }, done) => upsertFile({
    repoName,
    name: 'fiha.json',
    content,
  }, done);

  returned.REPOSITORY_UPSERT_LAYER = ({
    repoName,
    type,
    name,
    content,
  }, done) => upsertFile({
    repoName,
    type: 'layers',
    name: `${name}-${type}.js`,
    content,
  }, done);

  returned.REPOSITORY_DELETE_LAYER = ({
    repoName,
    type,
    name,
  }, done) => deleteFile({
    repoName,
    type: 'layers',
    name: `${name}-${type}.js`,
  }, done);

  returned.REPOSITORY_UPSERT_WORKER = ({
    repoName,
    type,
    content,
  }, done) => upsertFile({
    repoName,
    type: 'worker',
    name: `${type}.js`,
    content,
  }, done);

  returned.REPOSITORY_DELETE_WORKER = ({
    repoName,
    type,
  }, done) => deleteFile({
    repoName,
    type: 'worker',
    name: `${type}.js`,
  }, done);

  returned.REPOSITORY_COMMIT = ({
    repoName,
    message = 'bad commit message',
  }, done) => repoExec(repoName, `git add . && git commit -m "${message}"`, (err, stdout, stderr) => {
    execDone(done)(err && stdout.indexOf('nothing to commit') > -1 ? null : err, stdout, stderr);
  });

  returned.LOAD_REPO = async (payload, done) => {
    try {
      const setup = await readSetup(payload.id);
      done({
        type: 'SUCCESS',
        payload: setup,
      });
    } catch (err) {
      done({
        type: 'ERROR',
        payload: {
          message: err.message,
          stack: err.stack,
          payload,
        },
      });
    }
  };

  returned.LIST_REPOS = (options, done) => glob('**/fiha.json', {
    cwd: reposDir,
  // eslint-disable-next-line consistent-return
  }, (err, files) => {
    if (err) {
      return done({
        type: 'ERROR',
        payload: err.stack,
      });
    }

    done({
      type: 'SUCCESS',
      payload: files.map(file => dirname(file)),
    });
  });

  return returned;
};

module.exports = {
  actions,
  onWorkerConnection,
};
