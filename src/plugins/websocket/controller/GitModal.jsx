import React from 'react';
import { connect } from 'react-redux';
import StorageModal, { mapStateToProps } from '../../StorageModal';

// eslint-disable-next-line
const warn = console.warn.bind(console);

class GitModal extends StorageModal {
  list = () => this.props.list(({
    setups,
    error,
  }) => this.setState({
    keys: setups || [],
    error: error && {
      message: (
        <div>
          {error.message}
          <hr />
          <p>
            The Git storage is only available on local usage of Visual Fiha.
          </p>

          <p>
            Follow&nbsp;
            <a
              target="_blank"
              rel="noopener noreferrer"
              href="https://gitlab.com/zeropaper/fiha#install"
            >
              these instructions
            </a>
            &nbsp;to setup Visual Fiha locally.
          </p>
        </div>
      ),
    },
  }))
}

GitModal.propTypes = {
  ...StorageModal.propTypes,
};

GitModal.defaultProps = {
  id: null,
};

export default connect(mapStateToProps)(GitModal);
