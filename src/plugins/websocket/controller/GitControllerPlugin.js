import React from 'react';
import ControllerPlugin from '../../ControllerPlugin';
import GitModal from './GitModal';

export default class LocalForageControllerPlugin extends ControllerPlugin {
  /* eslint-disable react/jsx-filename-extension */
  modal = wanted => (wanted.toLowerCase() === 'git' && (() => (
    <GitModal
      save={this.saveSetup}
      list={this.listSetups}
      load={this.loadSetup}
    />
  )));
  /* eslint-enable react/jsx-filename-extension */

  toolbarMenu = {
    storage: [
      {
        action: 'git',
        text: 'Git…',
      },
    ],
  };

  // get shortcuts() {
  //   return [
  //     {
  //       ctrl: true,
  //       key: 's',
  //       callback: this.saveSetup,
  //       title: 'Save setup',
  //     },
  //     {
  //       alt: true,
  //       key: 'n',
  //       callback: this.newSetup,
  //       title: 'New setup',
  //     },
  //     {
  //       ctrl: true,
  //       key: 'r',
  //       callback: this.loadSetup,
  //       title: 'Reload setup',
  //     },
  //     {
  //       ctrl: true,
  //       key: 'o',
  //       callback: this.openStorageDialog,
  //       title: 'Open setup',
  //     },
  //   ];
  // }

  closeStorageDialog = () => this.setState({
    storageModalOpen: false,
  });

  openStorageDialog = () => this.setState({
    storageModalOpen: 'git',
  });

  listSetups = (done) => {
    this.emit({ type: 'GIT_LIST_REPOS' }, done);
  };

  newSetup = (evt) => {
    if (evt) evt.preventDefault();
    this.emit({ type: 'STORAGE_NEW' });
    this.closeStorageDialog();
  };

  saveSetup = (id) => {
    if (id && typeof id !== 'string') throw new Error('ID must be a string if specified');
    const props = this.getProps();
    const setupId = id || props.setupId;
    if (!setupId) return;

    this.emit({
      type: 'GIT_SAVE_REPO',
      payload: {
        id: setupId,
        storageType: 'git',
      },
    }, () => this.closeStorageDialog());
  };

  loadSetup = (id) => {
    if (typeof id !== 'string') throw new Error('Repo setup ID must be a string');

    this.emit({
      type: 'GIT_LOAD_REPO',
      payload: { id },
    }, () => this.closeStorageDialog());
  };
}
