/* globals io */
import series from 'async/series';

import BackendPlugin from '../../BackendPlugin';
import blob2DataURI from '../../../util/blob2DataURI';

const buff2Str = buffer => String.fromCharCode.apply(null, new Uint8Array(buffer));

export default class Repo extends BackendPlugin {
  initialize() {
    const serverURL = process.env.FIHA_SOCKET_CLIENT;
    if (!serverURL) return;
    const scriptURL = `${serverURL}/socket.io.slim.js`;

    try {
      this.backend.worker.importScripts(scriptURL);
      setTimeout(() => this.connect(serverURL), 0);
    } catch (e) {
      // eslint-disable-next-line no-console
      console.log(`socket script import failed, no available server at ${scriptURL}`, e.message);
    }
  }

  connect(serverURL) {
    const { backend } = this;
    const socket = io(serverURL);

    this.socket = socket;

    backend.set({ ws: {} });

    socket.emit('HANDSHAKE', {
      obj: true,
      // eslint-disable-next-line
    }, (...args) => console.info('handshake response', ...args));

    socket.on(...this.invoke('REPO_CHANGE_FILE', this.repoChangeFile));
    socket.on(...this.invoke('REPO_ADD_FILE', this.repoChangeFile));
    socket.on(...this.invoke('REPO_UNLINK_FILE', this.repoUnlinkFile));
    socket.on(...this.invoke('NEW_REMOTE', this.newRemote));
    socket.on(...this.invoke('REMOVE_REMOTE', this.removeRemote));
    socket.on(...this.invoke('UPDATE_REMOTE', this.updateRemote));
  }

  invoke = (type, func) => [type, (payload) => {
    const action = { type, payload };
    const { executeHook } = this;
    executeHook('socketPreReducer', action);

    func(payload);

    executeHook('socketPostReducer', action);
  }];

  socketEmit = (action, next = () => {}) => (new Promise((res, rej) => {
    if (!this.socket) {
      const err = new Error('No socket');
      rej(err);
      next(err);
      return;
    }

    this.socket.emit(action.type, action.payload, (response) => {
      const { type, payload } = response;

      if (type === 'SUCCESS') {
        res(payload);
        next(null, payload);
      } else {
        if (response.payload && response.payload.error) {
          const { error } = response.payload;
          const err = new Error(error.message);
          err.original = action;
          err.originalStack = error.stack;
          rej(err);
          next(err);
          return;
        }
        const err = new Error((payload && payload.error) || `${action.type} Error`);
        err.original = action;
        err.stdout = payload && payload.stdout;
        err.stderr = payload && payload.stderr;
        rej(err);
        next(err);
      }
    });
  }));

  repoChangeFile = (payload) => {
    if (payload.repoName !== this.state.app.setupId) return;

    if (payload.type === 'fihajson') {
      this.readFihaJson(buff2Str(payload.buffer));
    } else if (payload.type === 'layers') {
      this.process({
        type: 'EDIT_LAYER',
        payload: {
          [payload.role]: buff2Str(payload.buffer),
          id: payload.id,
        },
      });
    } else if (payload.type === 'worker') {
      this.process({
        type: 'UPDATE_WORKER_SCRIPTS',
        payload: {
          [payload.role]: buff2Str(payload.buffer),
        },
      });
    } else if (payload.type === 'assets') {
      const promise = payload.url
        ? fetch(payload.url).then(response => response.blob())
        : Promise.resolve(new File([payload.buffer], payload.id, {
          type: payload.mimeType,
        }));

      promise.then(blob => blob2DataURI(blob)
        .then(dataURI => this.process({
          type: 'ASSETS_UPDATE',
          payload: [
            {
              name: payload.id,
              size: blob.size,
              type: blob.type,
              lastModified: blob.lastModified,
              lastModifiedDate: blob.lastModifiedDate,
              preview: URL.createObjectURL(blob),
              dataURI,
            },
          ],
        })))
        .catch((err) => { throw err; });

      // const file = new File([payload.buffer], payload.id, { type: payload.mimeType });
      // blob2DataURI(file)
      //   .then((dataURI) => {
      //     this.process({
      //       type: 'ASSETS_UPDATE',
      //       payload: [
      //         {
      //           name: payload.id,
      //           size: file.size,
      //           type: file.type,
      //           lastModified: file.lastModified,
      //           lastModifiedDate: file.lastModifiedDate,
      //           preview: URL.createObjectURL(file),
      //           dataURI,
      //         },
      //       ],
      //     });
      //   })
      //   .catch((err) => { throw err; });
    }
  };

  repoUnlinkFile = (payload) => {
    if (payload.repoName !== this.state.app.setupId) return;

    if (payload.type === 'layers') {
      this.process({
        type: 'EDIT_LAYER',
        payload: {
          [payload.role]: '',
          id: payload.id,
        },
      });
    } else if (payload.type === 'worker') {
      this.process({
        type: 'UPDATE_WORKER_SCRIPTS',
        payload: {
          [payload.role]: '',
        },
      });
    } else {
      this.process({
        type: 'ASSETS_REMOVE',
        payload: payload.id,
      });
    }
  };

  newRemote = (payload) => {
    const { backend } = this;
    const workerData = backend.data;
    const base = (workerData.ws || {});
    workerData.ws = {
      ...base,
      [payload.id]: payload,
    };
    backend.set(workerData);
    backend.emit({
      type: 'NEW_REMOTE',
      payload,
    });
  };

  removeRemote = (payload) => {
    const { backend } = this;
    const workerData = backend.data;
    workerData.ws[payload.id] = undefined;
    backend.set(workerData);
    backend.emit({
      type: 'REMOVE_REMOTE',
      payload,
    });
  };

  updateRemote = (payload) => {
    const { backend } = this;
    const workerData = backend.data;
    const base = (workerData.ws || {});
    workerData.ws = {
      ...base,
      [payload.id]: {
        ...(base[payload.id] || {}),
        ...payload.data,
      },
    };
    backend.set(workerData);
  };

  serverSaveLayer = ({
    payload: { animation, setup, id },
  }) => {
    const {
      currentSetup: { id: repoName },
    } = this.backend;
    if (
      typeof animation === 'undefined'
      && typeof setup === 'undefined'
    ) return;

    this.socketEmit({
      type: 'REPOSITORY_UPSERT_LAYER',
      payload: {
        repoName,
        type: typeof animation !== 'undefined'
          ? 'animation'
          : 'setup',
        name: id,
        content: animation || setup,
      },
    });
  };

  serverUpdateAssets = async (assets) => {
    const {
      currentSetup: { id: repoName },
    } = this.backend;

    const url = `${process.env.FIHA_SOCKET_CLIENT}/upload`;

    const upload = ({ name, dataURI }) => fetch(url, {
      method: 'POST',
      mode: 'cors',
      headers: {
        'Content-type': 'application/json',
        Accept: 'aplication/json',
      },
      body: JSON.stringify({
        repo: repoName,
        directory: 'assets',
        name,
        dataURI,
      }),
    })
      .then(() => ({}));

    return Promise.all(assets.map(upload));
  };

  readFihaJson = (content, done) => {
    let parsed;
    try {
      parsed = JSON.parse(content);
    } catch (err) {
      done(err);
    }

    const layerById = id => this.state.layers.find(layer => id === layer.id) || {};
    const assetByName = name => this.state.assets.find(asset => name === asset.name) || {};
    this.batchDispatch([
      {
        type: 'REPLACE_LAYERS',
        payload: parsed.layers.map(layer => ({ ...layer, ...layerById(layer.id) })),
      },
      // {}, worker scripts?
      {
        type: 'SET_SETUP_ID',
        payload: {
          id: parsed.id,
          storageType: 'git',
        },
      },
      {
        type: 'ASSETS_REPLACE',
        payload: parsed.assets.map(asset => ({ ...asset, ...assetByName(asset.name) })),
      },
    ]);
  };

  saveFihaJson = (done) => {
    const { currentSetup } = this.backend;
    this.socketEmit({
      type: 'REPOSITORY_UPSERT_FIHAJSON',
      payload: {
        repoName: currentSetup.id,
        type: 'fihajson',
        name: 'fiha.json',
        content: JSON.stringify({
          ...currentSetup,
          signal: undefined,
          layers: currentSetup.layers.map(layer => ({
            ...layer,
            setup: undefined,
            animation: undefined,
          })),
          assets: currentSetup.assets.map(asset => ({
            name: asset.name,
          })),
        }, null, 2),
      },
    }, done);
  };

  saveWorkerScripts = (done) => {
    const {
      currentSetup: { signal, id: repoName },
    } = this.backend;

    return series({
      setup: nextOp => this.socketEmit(signal.setup ? {
        type: 'REPOSITORY_UPSERT_WORKER',
        payload: {
          repoName,
          type: 'setup',
          content: signal.setup,
        },
      } : {
        type: 'REPOSITORY_DELETE_WORKER',
        payload: {
          repoName,
          type: 'setup',
        },
      }, nextOp),
      animation: nextOp => this.socketEmit(signal.animation ? {
        type: 'REPOSITORY_UPSERT_WORKER',
        payload: {
          repoName,
          type: 'animation',
          content: signal.animation,
        },
      } : {
        type: 'REPOSITORY_DELETE_WORKER',
        payload: {
          repoName,
          type: 'animation',
        },
      }, nextOp),
    }, done);
  };

  saveLayersScripts = (done) => {
    const {
      currentSetup: { layers, id: repoName },
    } = this.backend;

    const tasks = {};
    layers.forEach((layer) => {
      tasks[layer.id] = nextLayer => series({
        setup: nextOp => this.socketEmit(layer.setup ? {
          type: 'REPOSITORY_UPSERT_LAYER',
          payload: {
            repoName,
            name: layer.id,
            type: 'setup',
            content: layer.setup,
          },
        } : {
          type: 'REPOSITORY_DELETE_LAYER',
          payload: {
            repoName,
            name: layer.id,
            type: 'setup',
          },
        }, nextOp),
        animation: nextOp => this.socketEmit(layer.animation ? {
          type: 'REPOSITORY_UPSERT_LAYER',
          payload: {
            repoName,
            name: layer.id,
            type: 'animation',
            content: layer.animation,
          },
        } : {
          type: 'REPOSITORY_DELETE_LAYER',
          payload: {
            repoName,
            name: layer.id,
            type: 'animation',
          },
        }, nextOp),
      }, nextLayer);
    });

    return series(tasks, done);
  };

  saveSetup = () => {
    const {
      currentSetup: { assets, id: repoName },
    } = this.backend;

    if (!repoName) {
      // eslint-disable-next-line no-console
      console.error('No repository name');
      return;
    }

    const tasks = {
      repo: done => this.socketEmit({
        type: 'REPOSITORY_MAKE',
        payload: repoName,
      }, done),
      worker: this.saveWorkerScripts,
      layers: this.saveLayersScripts,
      assets: done => this.serverUpdateAssets(assets)
        .then(res => done(null, res)).catch(done),
      config: this.saveFihaJson,
      commit: done => this.socketEmit({
        type: 'REPOSITORY_COMMIT',
        payload: { repoName },
      }, done),
    };

    series(tasks, (err) => {
      if (err) {
        this.notifyFailure(err.message || 'Unknown error');
      } else {
        this.notifySuccess(`Commited to Git repository ${repoName}`);
      }
    });
  };

  prepareError = err => (err && {
    message: err.message,
    stack: err.stack,
    original: err.original,
    originalStack: err.originalStack,
  });

  workerPostReducer = (action) => {
    const {
      actionCallback,
      sendSetup,
    } = this.backend;
    const { type, payload } = action;

    switch (type) {
      case 'GIT_LIST_REPOS':
        this.socketEmit({
          type: 'LIST_REPOS',
        }, (error, setups) => actionCallback(action, {
          error: this.prepareError(error),
          setups,
        }));
        break;

      case 'GIT_LOAD_REPO':
        this.socketEmit({
          type: 'LOAD_REPO',
          payload,
        }, (error, setup) => {
          this.dispatch({
            type: 'SET_SETUP_ID',
            payload: {
              id: setup.id,
              storageType: 'git',
            },
          });
          sendSetup({
            setup: {
              id: setup.id,
            },
            ...setup,
            assets: [],
          });
          actionCallback(action, { error: this.prepareError(error), setup });
        });
        break;

      case 'GIT_SAVE_REPO':
      case 'STORAGE_SAVE':
        this.saveSetup();
        break;

      default:
    }
  };

  broadcastPostReducer = (action) => {
    const { type, payload } = action;
    const {
      currentSetup: { id: repoName },
    } = this.backend;

    switch (type) {
      case 'CAPTURED_DISPLAY':
        fetch(payload.url)
          .then(resp => resp.arrayBuffer())
          .then(content => this.socketEmit({
            type: 'REPOSITORY_UPSERT_CAPTURE',
            payload: {
              repoName,
              name: `${payload.id}.png`,
              content,
            },
          }))
          // eslint-disable-next-line no-console
          .catch(err => console.warn('worker CAPTURED_DISPLAY error', err));
        break;
      default:
    }
  };
}
