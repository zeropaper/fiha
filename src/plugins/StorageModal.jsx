import React from 'react';
import PropTypes from 'prop-types';
import StorageItem from './StorageModal.Item';
import InputGroup from './StorageModal.InputGroup';

import './StorageModal.scss';

// eslint-disable-next-line
const warn = console.warn.bind(console);

class StorageModal extends React.Component {
  state = {
    id: '',
    keys: [],
    error: null,
  };

  componentDidMount() {
    this.list();
  }

  setStateValue = name => (evt) => {
    this.setState({
      [name]: evt.target.value.trim(),
    });
  };

  list = () => {};

  filterList = (key) => {
    const { id } = this.state;
    if (!id) return true;
    return key.indexOf(id) > -1;
  };

  handleSaveEvent = (evt) => {
    if (evt && evt.preventDefault) evt.preventDefault();
    this.props.save(this.state.id);
  };

  load = id => this.props.load(id);

  render() {
    const { keys, id, error } = this.state;
    const localElements = [];

    if (error) {
      return [
        <div className="storage-error alert alert-danger">
          {error.message}
        </div>,
      ];
    }

    localElements.push((
      <form key="idform" onSubmit={evt => evt.preventDefault()} className="container-fluid py-3">
        <div className="row mb-3">
          <div className="col">
            <InputGroup
              className="storage-new"
              onChange={this.setStateValue('id')}
              value={id}
              placeholder="storage-id"
              pattern="[a-zA-Z0-9_-]+"
              autoFocus
              addons={[
                {
                  text: 'Save',
                  onClick: this.handleSaveEvent,
                  className: 'btn-primary',
                },
              ]}
            />
          </div>
        </div>
      </form>
    ));

    localElements.push((
      <ul key="list" className="container-fluid d-block storage-list">
        {keys.filter(this.filterList).map(key => (
          <StorageItem
            key={key}
            id={key}
            save={this.props.save}
            load={this.props.load}
            current={this.props.id === key}
          />
        ))}
      </ul>
    ));

    return localElements;
  }
}

StorageModal.propTypes = {
  id: PropTypes.string,
  save: PropTypes.func.isRequired,
  load: PropTypes.func.isRequired,
};

StorageModal.defaultProps = {
  id: null,
};

export const mapStateToProps = ({
  app: { setupId: id },
}) => ({
  id,
});

export default StorageModal;
