export default class FihaPlugin {
  mounted = false;

  log = (...args) => {
    const name = this.constructor.name.replace('Plugin', '');
    if (
      !localStorage
      || !localStorage.fihaLogged
      || localStorage.fihaLogged.split(',').map(s => s.trim()).indexOf(name) < 0
    ) return;
    // eslint-disable-next-line no-console
    console.log(`[${name}]`, ...args);
  };

  mount = async () => {
    this.mounted = true;

    const unmount = async () => {
      this.mounted = false;
    };
    return unmount;
  };
}
