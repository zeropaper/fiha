import React from 'react';
import PropTypes from 'prop-types';

const StorageItem = ({
  id,
  load,
  save,
  current,
  description,
  createdAt,
  updatedAt,
}) => {
  const loadClick = () => load(id);
  const saveClick = () => save(id);

  let loadBtnClassname = 'btn btn-primary storage-list__load';
  let saveBtnClassname = 'btn btn-secondary storage-list__load';
  if (current) {
    loadBtnClassname = 'btn btn-secondary storage-list__load';
    saveBtnClassname = 'btn btn-primary storage-list__load';
  }

  return (
    <li className="row storage-list__item">
      <div
        title={createdAt ? `created ${createdAt}${updatedAt && `, updated ${updatedAt}`}` : null}
        className="col col-form-label storage-list__id"
      >
        {id}
        {description && ` ${description}`}
      </div>

      <div className="col btn-group d-flex flex-grow-0 justify-content-end storage-list__actions">
        <button className={loadBtnClassname} onClick={loadClick}>Load</button>
        <button className={saveBtnClassname} onClick={saveClick}>Save</button>
      </div>
    </li>
  );
};

StorageItem.propTypes = {
  id: PropTypes.string.isRequired,
  load: PropTypes.func.isRequired,
  save: PropTypes.func.isRequired,
  current: PropTypes.bool,
  description: PropTypes.string,
  createdAt: PropTypes.string,
  updatedAt: PropTypes.string,
};

StorageItem.defaultProps = {
  current: false,
  description: null,
  createdAt: null,
  updatedAt: null,
};

export default StorageItem;
