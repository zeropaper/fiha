import ControllerPlugin from '../../ControllerPlugin';
import korgkp3 from './devices/korg/kp3';
import korgnk2 from './devices/korg/nk2';
import akailpd8 from './devices/akai/lpd8';
import novationlpm from './devices/novation/lpm';

import MIDIMapping from './MIDIMapping';
import MIDITabLoader from './MIDITabLoader';

const midiMappings = {
  'KORG INC.': {
    'KP3 MIDI 1': korgkp3,
    // linux
    'nanoKONTROL2 MIDI 1': korgnk2,
    // MacOS
    'nanoKONTROL2 SLIDER/KNOB': korgnk2,
  },
  'AKAI professional LLC': {
    'LPD8 MIDI 1': akailpd8,
  },
  'Focusrite A.E. Ltd': {
    'Launchpad Mini MIDI 1': novationlpm,
  },
};

export default class MIDIControllerPlugin extends ControllerPlugin {
  static storeId = 'midi';

  static get capable() {
    return !!navigator.requestMIDIAccess;
  }

  get signalNames() {
    if (!MIDIControllerPlugin.capable) return [];
    const names = [];
    // eslint-disable-next-line no-console
    console.info(this.inputs);
    return names;
  }

  get tabs() {
    if (!MIDIControllerPlugin.capable || !this.inputs.length) return [];
    return [
      {
        title: 'MIDI',
        /* eslint-disable */
        component: MIDITabLoader,
        /* eslint-enable */
      },
    ];
  }

  inputs = [];

  midiMappings = midiMappings || {};

  mount = async () => {
    if (navigator.requestMIDIAccess) {
      try {
        await this.request();
      } catch (err) {
        // eslint-disable-next-line no-console
        console.warn(err.stack);
      }
    }

    return () => {
      if (this.MIDIAccess) {
        this.inputs.forEach(input => input.unbindEvents());
        this.MIDIAccess.removeEventListener('statechange', this.onStateChange);
        this.MIDIAccess = null;
      }
    };
  };

  getMappings = (manufacturer, name) => {
    const m = this.midiMappings;
    if (!m[manufacturer] || !m[manufacturer][name]) {
      return null;
    }
    return m[manufacturer][name];
  }

  addInput = (input) => {
    const mappings = this.getMappings(input.manufacturer, input.name);
    if (!mappings) {
      if (input.name !== 'Midi Through Port-0') {
        // eslint-disable-next-line
        console.warn('MIDI: Unrecognized controller %s from %s', input.name, input.manufacturer);
      }
    } else {
      this.inputs.push(new MIDIMapping({
        mappings,
        device: input,
        setData: this.setData,
      }));
    }
  };

  onStateChange = () => {
    const { MIDIAccess } = this;

    this.inputs.forEach(input => input.unbindEvents());

    this.inputs = [];
    MIDIAccess.inputs.forEach(this.addInput);
    this.setState({ midiInputs: MIDIAccess.inputs.length });
  };

  request = async () => {
    const MIDIAccess = await navigator.requestMIDIAccess();
    if (this.MIDIAccess !== MIDIAccess) {
      if (this.MIDIAccess) {
        this.MIDIAccess.removeEventListener('statechange', this.onStateChange);
      }
      MIDIAccess.addEventListener('statechange', this.onStateChange);
      this.MIDIAccess = MIDIAccess;
    }
    this.onStateChange();
  };
}
