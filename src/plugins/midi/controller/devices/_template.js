import toPrct from '../to-prct'; // eslint-disable-line

const mappings = {
  prefix: '<something>',

  type: {
    128: 'noteOn',
    144: 'noteOff',
    176: 'change',
    192: 'search',
    248: 'idle',
  },

  note: {
  },

  velocity: {
    0: (type, note, velocity) => {
      if (note > 23) {
        return false;
      }
      return velocity;
    },

    127: (type, note, velocity) => {
      if (note > 23) {
        return true;
      }
      return toPrct(velocity);
    },
  },

  signalNames: [
  ],
};


const MIDImapper = (data) => {
  const type = data[0] || 0;
  if (type === 248) { return {}; }

  const note = data[1] || 0;
  const velocity = data[2] || 0;

  const name = mappings.note[note];
  // console.info('MIDI evt on %s (%s) => %s', name, note, velocity, type);
  return {
    name,
    velocity,
    type,
  };
};

MIDImapper.info = mappings;

export default MIDImapper;
