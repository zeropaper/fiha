import React from 'react';
import PropTypes from 'prop-types';
import { Tab, Tabs, TabList, TabPanel } from 'react-tabs';

import MIDIMapping from './MIDIMapping';
import './MIDITab.scss';

export function cleanClassName(str) {
  return str.toLowerCase().split(/[^a-z0-9]/g).join('-');
}

function MIDIInputButton({ prefix, item, addSnippet }) {
  return (
    <button
      className="btn btn-small btn-link"
      title="add read snippet"
      onClick={() => addSnippet(`read('${prefix}-${item}', 0)`)}
    >
      {item}
    </button>
  );
}

MIDIInputButton.propTypes = {
  prefix: PropTypes.string.isRequired,
  item: PropTypes.string.isRequired,
  addSnippet: PropTypes.func.isRequired,
};

export function MIDIInput({ input, addSnippet }) {
  const manufacturer = cleanClassName(input.device.manufacturer);
  const name = cleanClassName(input.device.name);
  const className = `midi__device midi__device--${manufacturer}--${name}`;
  const { info: { prefix, note } } = input.mappings;

  const makeItem = item => (
    <li key={item} className="midi__note">
      <MIDIInputButton prefix={prefix} item={item} addSnippet={addSnippet} />
    </li>
  );

  return (
    <div className={className}>
      <h3 className="midi__prefix">{prefix}-</h3>
      <ul className="midi__notes">
        {Object.values(note).map(makeItem)}
      </ul>
    </div>
  );
}

MIDIInput.propTypes = {
  addSnippet: PropTypes.func.isRequired,
  input: PropTypes.instanceOf(MIDIMapping).isRequired,
};


export default function MIDITab({ midi, addSnippet }) {
  const { inputs } = midi;

  const tabs = inputs.map(input => (
    <Tab key={input.device.name}>
      {input.device.name.replace('MIDI 1', '')}
    </Tab>
  ));

  const panels = inputs.map(input => (
    <TabPanel key={input.device.name}>
      <MIDIInput input={input} addSnippet={addSnippet} />
    </TabPanel>
  ));

  return (
    <Tabs className="vertical-tabs">
      <TabList>
        {tabs}
      </TabList>

      {panels}
    </Tabs>
  );
}

MIDITab.propTypes = {
  addSnippet: PropTypes.func.isRequired,
  midi: PropTypes.object.isRequired,
};
