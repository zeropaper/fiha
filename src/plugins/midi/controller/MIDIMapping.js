export default class MIDIMapping {
  constructor({
    device,
    mappings,
    setData,
  }) {
    this.device = device;
    this.mappings = mappings;
    this.setData = setData;
    this.bindEvents();
  }

  listener = (data) => {
    const id = `${data.prefix}-${data.name}`.replace(/[^a-z0-9]/ig, '-');
    this.setData({ [id]: data.velocity });
  };

  onMidiMessage = (evt) => {
    const info = this.mappings(evt.data);
    if (info.type === 248) return;
    this.listener(info);
  }

  bindEvents = () => this.device.addEventListener('midimessage', this.onMidiMessage);

  unbindEvents = () => this.device.removeEventListener('midimessage', this.onMidiMessage);
}
