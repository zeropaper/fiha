import React, { Suspense, lazy } from 'react';
import PropTypes from 'prop-types';

const MIDITab = lazy(() => import('./MIDITab'));

const MIDITabLoader = props => (
  <Suspense fallback={<div>Loading...</div>}>
    <MIDITab
      midi={this}
      addSnippet={props.addSnippet}
    />
  </Suspense>
);

MIDITabLoader.propTypes = {
  addSnippet: PropTypes.func.isRequired,
};

export default MIDITabLoader;
