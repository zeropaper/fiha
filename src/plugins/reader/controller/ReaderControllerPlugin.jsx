import React, { lazy, Suspense } from 'react';
import uniq from 'lodash.uniq';

import ControllerPlugin from '../../ControllerPlugin';

const ReaderTab = lazy(() => import('./ReaderTab'));

export default class ReaderControllerPlugin extends ControllerPlugin {
  static storeId = 'reader';

  tabs = [
    {
      title: 'Reader',
      /* eslint-disable */
      component: props => (
        <Suspense fallback={<div>Loading...</div>}>
          <ReaderTab
            addKey={this.addReaderKey}
            removeKey={this.removeReaderKey}
            read={this.read}
            keys={props.controllerState.readerKeys}
          />
        </Suspense>
      ),
      /* eslint-enable */
    },
  ];

  addReaderKey = value => this.setState(prevState => ({
    readerKeys: uniq([...prevState.readerKeys || [], value]),
  }));

  removeReaderKey = value => this.setState(prevState => ({
    readerKeys: prevState.readerKeys.filter(key => key !== value),
  }));
}
