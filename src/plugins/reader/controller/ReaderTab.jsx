import React from 'react';
import PropTypes from 'prop-types';

const Item = ({ item, remove }) => (
  <div className="row align-items-center">
    <span className="col-3 d-flex">
      <button
        onClick={remove}
        className="btn btn-sm btn-danger text-center flex-grow-1"
        data-key={item}
        title="Remove"
      >
        {item}
      </button>
    </span>

    <span className="col value" data-key={item}>
      {' '}
    </span>

    {/* <span className="col-1 d-flex">
      <button
        onClick={() => addSnippet(`read('${item}')`)}
        className="btn btn-sm btn-link text-center flex-grow-1"
        title="Add read() snippet"
      >
        +
      </button>
    </span> */}
  </div>
);

Item.propTypes = {
  item: PropTypes.string.isRequired,
  remove: PropTypes.func.isRequired,
};

export default class Reader extends React.Component {
  componentDidMount() {
    window.requestAnimationFrame(this.animate);
  }

  animate = () => {
    if (this.list) {
      this.list.querySelectorAll('.value[data-key]')
        .forEach((el) => {
          // eslint-disable-next-line no-param-reassign
          el.textContent = this.props.read(el.getAttribute('data-key'));
        });
    }
    window.requestAnimationFrame(this.animate);
  }

  refList = (list) => {
    this.list = list;
  }

  addKey = (evt) => {
    evt.preventDefault();
    const input = evt.target.querySelector('input');
    const { value } = input;

    this.props.addKey(value);

    input.value = '';
  };

  removeKey = (evt) => {
    const value = evt.target.getAttribute('data-key');
    this.props.removeKey(value);
  }

  render() {
    return (
      <section className="container-fluid">
        <header className="row py-3">
          <form onSubmit={this.addKey} className="col">
            <input
              placeholder="key"
              type="text"
              name="new-key"
              className="form-control form-control-sm"
            />
          </form>
        </header>

        <div className="row pb-3">
          <div className="col" ref={this.refList}>
            {
              this.props.keys.map(key => (
                <Item key={key} item={key} remove={this.removeKey} />
              ))
            }
          </div>
        </div>
      </section>
    );
  }
}

Reader.propTypes = {
  addKey: PropTypes.func.isRequired,
  removeKey: PropTypes.func.isRequired,
  keys: PropTypes.array.isRequired,
  // addSnippet: PropTypes.func.isRequired,
  read: PropTypes.func.isRequired,
};
