/* eslint-disable react/jsx-filename-extension, react/react-in-jsx-scope */
import { deprecate } from 'util';
import FihaPlugin from './FihaPlugin';

export default class ControllerPlugin extends FihaPlugin {
  constructor(context) {
    super(context);

    const {
      dispatch,
      read,
      emit,
      broadcast,
      setState,
      getState,
      getProps,
      setData,
    } = context;

    this.dispatch = deprecate(dispatch, 'use emit instead');
    this.read = read;
    this.emit = emit;
    this.broadcast = broadcast;
    this.setState = setState;
    this.getState = getState;
    this.getProps = getProps;
    this.setData = setData;
  }

  get pluginId() {
    return this.storeId
      || this.constructor.name.replace('ControllerPlugin', '').toLowerCase();
  }

  // eslint-disable-next-line class-methods-use-this
  get signalNames() {
    return [
      // 'someName',
    ];
  }
  // or
  // signalNames = ['someName'];

  // eslint-disable-next-line class-methods-use-this
  get tabs() {
    return [
      // {
      //   title: 'Plugin Name',
      //   component: () => 'This could be a React component.',
      // },
    ];
  }

  // eslint-disable-next-line class-methods-use-this
  get workerActionHandlers() {
    return {};
  }

  // eslint-disable-next-line class-methods-use-this
  get broadcastActionHandlers() {
    return {};
  }

  // eslint-disable-next-line class-methods-use-this
  get modal() {
    return (<div>Something</div>);
  }

  // eslint-disable-next-line class-methods-use-this
  get toolbarMenu() {
    return {};
  }

  // eslint-disable-next-line class-methods-use-this
  get shortcuts() {
    return [];
  }
}
