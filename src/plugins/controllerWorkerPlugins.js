import AudioWorkerPlugin from './audio/worker/AudioControllerWorkerPlugin';
import GistWorkerPlugin from './gist/worker/GistControllerWorkerPlugin';
import LocalForageWorkerPlugin from './localforage/worker/LocalForageControllerWorkerPlugin';
import WebSocketWorkerPlugin from './websocket/worker/RepoControllerWorkerPlugin';
import AssetstControllerWorkerPlugin from './assets/worker/AssetsControllerWorkerPlugin';

export default [
  AudioWorkerPlugin,
  GistWorkerPlugin,
  LocalForageWorkerPlugin,
  WebSocketWorkerPlugin,
  AssetstControllerWorkerPlugin,
];
