import configStorage from '../../../controller/fiha-config-storage';

import GistService, { anonGistLoad, blobURI2DataURI } from '../../../controller/services/gist';
import BackendPlugin from '../../BackendPlugin';

const gistService = new GistService(configStorage);

export default class Gist extends BackendPlugin {
  prepareAssets = saving => Promise.all(saving.assets.map((asset) => {
    if (asset.dataURI) return asset;
    return blobURI2DataURI(asset.preview)
      .then(dataURI => ({ ...asset, dataURI }));
  }));

  workerPostReducer = (action) => {
    const {
      sendSetup,
      currentSetup,
      actionCallback,
      notifySuccess,
      notifyFailure,
    } = this.backend;
    const { type, payload } = action;

    switch (type) {
      case 'GIST_CREDENTIALS':
        gistService
          .credentials(payload)
          .then(() => notifySuccess(payload && payload.token
            ? 'credentials saved'
            : 'credentials removed'))
          .catch(notifyFailure);
        break;

      case 'GIST_SAVE':
        this.prepareAssets(currentSetup)
          .then(assets => gistService.save({ ...currentSetup, assets })
            .then((response) => {
              notifySuccess(`Gist "${response.id}" saved`);
              actionCallback(action, response);
              this.dispatch({
                type: 'SET_SETUP_ID',
                payload: {
                  id: response.id,
                  storageType: 'gist',
                },
              });
            }))
          .catch(notifyFailure);
        break;

      case 'GIST_LOAD':
        gistService.user
          .then((user) => {
            if (!user) {
              anonGistLoad({
                action,
                sendSetup,
                actionCallback,
                notifySuccess,
                notifyFailure,
              });
            } else {
              gistService
                .load(payload.id)
                .then((loaded) => {
                  notifySuccess(`Gist "${payload.id}" loaded`);
                  actionCallback(action, loaded);
                  this.dispatch({
                    type: 'SET_SETUP_ID',
                    payload: {
                      id: payload.id,
                      storageType: 'gist',
                    },
                  });
                })
                .catch(notifyFailure);
            }
          })
          .catch(() => {
            anonGistLoad({
              action,
              sendSetup,
              actionCallback,
              notifySuccess,
              notifyFailure,
            });
          });
        break;

      case 'GIST_APPLY':
        // console.info('GIST_APPLY', payload);
        sendSetup(payload);
        break;


      default:
        // // eslint-disable-next-line no-console
        // console.info('client Gist', type, payload);
    }
  };
}
