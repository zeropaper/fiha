import React from 'react';

import ControllerPlugin from '../../ControllerPlugin';

import GistModal from './GistModal';
import GistService from '../../../controller/services/gist';
import configStorage from '../../../controller/fiha-config-storage';

export default class GistControllerPlugin extends ControllerPlugin {
  /* eslint-disable react/jsx-filename-extension */
  modal = wanted => (wanted === 'gist' && (() => (
    <GistModal
      referenceDialog={this.referenceDialog}
      saveToken={this.saveToken}
      clearToken={this.clearToken}
      applyGist={this.applyGist}
      loadGist={this.loadGist}
      saveGist={this.saveGist}
      listGists={this.listGists}
    />
  )));
  /* eslint-enable react/jsx-filename-extension */

  mount = async () => {
    this.gistService = new GistService(configStorage);
    this.user = await this.gistService.user;
    return () => {};
  };

  toolbarMenu = {
    storage: [
      {
        action: 'gist',
        text: 'Gist…',
        shortcut: 'CTRL + SHIFT + G',
      },
    ],
  };

  get shortcuts() {
    return [
      {
        shift: true,
        ctrl: true,
        key: 's',
        callback: this.saveGist,
      },
      // {
      //   shift: true,
      //   ctrl: true,
      //   key: 'g',
      //   callback: this.loadGist,
      // },
    ];
  }

  // a94564ad8f44380fef7ca4785d53d71fce8c2da2
  setDialogState = (...args) => {
    if (!this.dialog) return;
    this.dialog.setState(...args);
  };

  referenceDialog = (dialog) => {
    this.dialog = dialog;
    this.setDialogState(this.user);
  };

  closeStorageDialog = () => this.setState({
    storageModalOpen: false,
  });

  saveToken = (evt) => {
    evt.preventDefault();
    if (!evt.target.checkValidity()) {
      evt.target.parentNode.classList.add('error');
      return;
    }
    evt.target.parentNode.classList.remove('error');

    const token = evt.target.elements.item(0).value;
    if (!token) return;

    this.gistService.credentials({ token })
      .then(info => this.setDialogState(info))
      .catch(() => this.setDialogState({ login: '' }));
  };

  clearToken = (evt) => {
    evt.preventDefault();
    this.gistService.credentials(false)
      .then(() => this.setDialogState({ login: '' }))
      .catch(() => this.setDialogState({ login: '' }));
  };

  listGists = () => this.gistService.list()
  // eslint-disable-next-line no-console
    .catch(err => console.warn(err.message))

  loadGist = (id, cb = () => {}) => {
    const props = this.getProps();
    const setupId = id || props.id;
    if (!setupId) return;

    this.emit({
      type: 'GIST_LOAD',
      payload: { id: setupId },
    }, cb);
  };

  applyGist = (payload) => {
    this.emit({
      type: 'GIST_APPLY',
      payload,
    });
    this.closeStorageDialog();
  };

  saveGist = (id, description, cb = () => {}) => {
    const props = this.getProps();
    const setupId = id || props.id;
    // if (!setupId) return;

    this.emit({
      type: 'GIST_SAVE',
      payload: {
        id: setupId,
        description,
      },
    }, cb);
  };
}
