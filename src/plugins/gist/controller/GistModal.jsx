import React from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import moment from 'moment';
import StorageItem from '../../StorageModal.Item';
import InputGroup from '../../StorageModal.InputGroup';

// eslint-disable-next-line
const warn = console.warn.bind(console);

export const GistTokenUser = ({
  name,
  login,
  avatarUrl,
  clearToken,
}) => (
  <div className="container-fluid py-3">
    <div className="row">
      <div className="col-2">
        <img className="img-fluid" src={avatarUrl} alt="avatar" />
      </div>

      <div className="col d-flex flex-column justify-content-between">
        <div className="row">
          <div className="col">
            <div>{name}</div>
            <div>aka <code>{login}</code></div>
          </div>
        </div>

        <div className="row">
          <div className="col">
            <button
              className="btn btn-link"
              onClick={clearToken}
            >
              Clear token
            </button>
          </div>
        </div>
      </div>
    </div>
  </div>
);

GistTokenUser.propTypes = {
  name: PropTypes.string.isRequired,
  login: PropTypes.string.isRequired,
  avatarUrl: PropTypes.string.isRequired,
  clearToken: PropTypes.func.isRequired,
};

class StorageGist extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      id: props.id || '',
      login: '',
      avatarUrl: '',
      name: '',
      description: '',
      current: null,
      gists: null,
    };
  }

  componentDidMount() {
    const { referenceDialog, listGists } = this.props;
    referenceDialog(this);
    listGists()
      .then(gists => this.setState({ gists }))
      .catch(() => this.setState({ gists: null }));
  }

  setStateValue = name => evt => this.setState({
    [name]: evt.target.value,
  });

  renderHistory = entry => (
    <li key={entry.version} className="py-1">
      <div className="d-flex justify-content-between align-items-baseline">
        <button
          className="btn btn-link mr-1"
          onClick={() => this.props.loadGist(`${this.state.id}/${entry.version}`)}
        >
          {moment(entry.updated).fromNow()}
        </button>
        <a
          href={`https://github.com/${entry.author}`}
          target="_blank"
          rel="noopener noreferrer"
        >
          by {entry.author}
        </a>
      </div>
    </li>
  );

  render() {
    const gistElements = [];
    const {
      id,
      description,
      current,
      login,
      avatarUrl,
      name,
      gists,
    } = this.state;

    if (!login) {
      gistElements.push((
        <form
          key="gisttoken"
          className="container-fluid pb-3 pt-3"
          onSubmit={this.props.saveToken}
        >
          <div className="row">
            <div className="col">
              <InputGroup
                type="password"
                placeholder="Token"
                addons={[
                  {
                    type: 'submit',
                    text: 'Save token',
                  },
                ]}
                help={[
                  <div key="line-1">⚠ The token will saved locally (in your browser storage) ⚠</div>,
                  <div key="line-2">To generate a token:</div>,
                  <ul key="do">
                    <li>
                      <a
                        rel="noopener noreferrer"
                        target="_blank"
                        href="https://github.com/settings/tokens"
                      >
                        Go to the GitHub &quot;Personal access tokens&quot; page
                      </a>
                    </li>
                    <li>Click the &quot;Generate new token&quot;</li>
                    <li>Only select &quot;gist&quot; scope</li>
                  </ul>,
                ]}
              />
            </div>
          </div>
        </form>
      ));
    } else {
      gistElements.push((
        <GistTokenUser
          key="gistuser"
          name={name}
          login={login}
          avatarUrl={avatarUrl}
          clearToken={this.props.clearToken}
        />
      ));
    }

    const gistAddons = [];
    if (login) {
      gistAddons.push({
        className: 'btn-primary',
        onClick: () => this.props.saveGist(id, description, this.updateCurrent),
        text: id ? 'Update' : 'Create',
      });
    }
    if (id) {
      gistAddons.push({
        className: 'btn-secondary',
        onClick: () => this.props.loadGist(id),
        text: 'Load',
      });
    }

    const helpTxt = `Leave empty to create a new Gist.
      Specify an ID if you want to load or update an existing Gist.
      If you are not the owner of the Gist,
      it will be forked and then updated.`;
    gistElements.push((
      <form key="gistform" className="container-fluid pb-3">
        <div className="row pb-3">
          <div className="col">
            <InputGroup
              placeholder="Gist ID"
              defaultValue={id}
              onChange={this.setStateValue('id')}
              addons={gistAddons}
              help={helpTxt}
            />
          </div>
        </div>
        <div className="row">
          <div className="col">
            <textarea
              className="form-control"
              placeholder="Description of the setup."
              defaultValue={description}
              rows={Math.min((description || '').split('\n').length, 10)}
              onChange={this.setStateValue('description')}
            />
          </div>
        </div>
      </form>
    ));

    if (current) {
      gistElements.push((
        <div key="current" className="container-fluid pb-3 flex-grow-1 overflow-auto">
          <div className="row">
            <div className="col-4">
              <h3>History</h3>
              <div style={{ overflow: 'auto', maxHeight: '310px' }}>
                <ol reversed>
                  {
                    (current.history || [])
                      .map(this.renderHistory)
                  }
                </ol>
              </div>
            </div>
            <div className="col">
              <h3>Layers</h3>
              <ul>
                {
                  (current.layers || [])
                    .map(layer => <li key={layer.id}>{layer.id} {layer.type}</li>)
                }
              </ul>
            </div>
            <div className="col">
              <h3>Assets</h3>
              <ul>
                {
                  (current.assets || [])
                    .map(asset => <li key={asset.name}>{asset.name} {asset.type}</li>)
                }
              </ul>
            </div>
          </div>
        </div>
      ));

      gistElements.push((
        <div key="apply" className="container-fluid pb-3">
          <div className="row">
            <div className="col">
              <button
                className="btn btn-primary"
                onClick={() => this.props.applyGist(current)}
              >
                Apply setup
              </button>
            </div>
          </div>
        </div>
      ));
    }

    if (gists) {
      if (!gists.length) {
        gistElements.push((
          <div>
            No gists found
          </div>
        ));
      } else {
        gistElements.push((
          <ul key="list" className="container-fluid d-block storage-list">
            {gists.map(gist => (
              <StorageItem
                key={gist.id}
                id={gist.id}
                save={this.props.saveGist}
                load={this.props.loadGist}
                current={this.props.id === gist.id}
                description={`(${gist.files.length} files) ${gist.description}`}
                createdAt={gist.createdAt && moment(gist.createdAt).fromNow()}
                updatedAt={gist.updatedAt && moment(gist.updatedAt).fromNow()}
              />
            ))}
          </ul>
        ));
      }
    } else {
      gistElements.push((
        <div>Loading gists</div>
      ));
    }

    return gistElements;
  }
}

StorageGist.propTypes = {
  id: PropTypes.string,
  applyGist: PropTypes.func.isRequired,
  loadGist: PropTypes.func.isRequired,
  saveGist: PropTypes.func.isRequired,
  saveToken: PropTypes.func.isRequired,
  clearToken: PropTypes.func.isRequired,
  referenceDialog: PropTypes.func.isRequired,
  listGists: PropTypes.func.isRequired,
};

StorageGist.defaultProps = {
  id: null,
};

export default connect(({ app: { setupId, storageType } }) => ({
  id: storageType === 'gist' ? setupId : null,
}))(StorageGist);
