import AudioControllerPlugin from './audio/controller/AudioControllerPlugin';
import MIDIControllerPlugin from './midi/controller/MIDIControllerPlugin';
import KeyboardControllerPlugin from './keyboard/controller/KeyboardControllerPlugin';
import MouseControllerPlugin from './mouse/controller/MouseControllerPlugin';
import AssetsControllerPlugin from './assets/controller/AssetsControllerPlugin';
import ReaderControllerPlugin from './reader/controller/ReaderControllerPlugin';
import GistControllerPlugin from './gist/controller/GistControllerPlugin';
import LocalForageControllerPlugin from './localforage/controller/LocalforageControllerPlugin';
import GitControllerPlugin from './websocket/controller/GitControllerPlugin';

export default [
  AudioControllerPlugin,
  MIDIControllerPlugin,
  KeyboardControllerPlugin,
  MouseControllerPlugin,
  AssetsControllerPlugin,
  ReaderControllerPlugin,
  GistControllerPlugin,
  LocalForageControllerPlugin,
  GitControllerPlugin,
];
