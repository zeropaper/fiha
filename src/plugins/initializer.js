export default async (Plugins, context) => {
  const plugins = Plugins.map(Plugin => (new Plugin(context)));
  const unmounters = await Promise
    .all(plugins.map(instance => instance.mount()));

  return {
    plugins,
    unmount: async () => {
      await Promise.all(unmounters);
    },
  };
};
