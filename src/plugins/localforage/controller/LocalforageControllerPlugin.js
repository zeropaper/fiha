import React from 'react';

import ControllerPlugin from '../../ControllerPlugin';

import LocalForageModal from './LocalForageModal';

export default class LocalForageControllerPlugin extends ControllerPlugin {
  /* eslint-disable react/jsx-filename-extension */
  modal = wanted => (wanted.toLowerCase() === 'localforage' && (() => (
    <LocalForageModal
      save={this.saveSetup}
      new={this.newSetup}
      load={this.loadSetup}
    />
  )));
  /* eslint-enable react/jsx-filename-extension */

  toolbarMenu = {
    storage: [
      {
        action: 'localforage',
        text: 'Local DB…',
      },
    ],
  };

  get shortcuts() {
    return [
      {
        ctrl: true,
        key: 's',
        callback: this.saveSetup,
        title: 'Save setup',
      },
      {
        alt: true,
        key: 'n',
        callback: this.newSetup,
        title: 'New setup',
      },
      {
        ctrl: true,
        key: 'r',
        callback: this.loadSetup,
        title: 'Reload setup',
      },
      {
        ctrl: true,
        key: 'o',
        callback: this.openStorageDialog,
        title: 'Open setup',
      },
    ];
  }

  closeStorageDialog = () => this.setState({
    storageModalOpen: false,
  });

  openStorageDialog = () => this.setState({
    storageModalOpen: 'localforage',
  });

  newSetup = (evt) => {
    if (evt) evt.preventDefault();
    this.emit({ type: 'STORAGE_NEW' });
    this.closeStorageDialog();
  };

  saveSetup = (id) => {
    if (id && typeof id !== 'string') throw new Error('ID must be a string if specified');
    const props = this.getProps();
    const setupId = id || props.setupId;
    if (!setupId) return;

    this.emit({
      type: 'STORAGE_SAVE',
      payload: {
        id: setupId,
        // storageType: 'localForage',
      },
    });
    this.closeStorageDialog();
  };

  loadSetup = (id) => {
    if (id && typeof id !== 'string') throw new Error('ID must be a string if specified');
    const props = this.getProps();
    const setupId = id || props.setupId;
    if (!setupId) return;

    this.emit({
      type: 'STORAGE_LOAD',
      payload: {
        id: setupId,
      },
    });
    this.closeStorageDialog();
  };
}
