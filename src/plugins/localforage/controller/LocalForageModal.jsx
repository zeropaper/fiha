import React from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import StorageItem from '../../StorageModal.Item';
import InputGroup from '../../StorageModal.InputGroup';

import storage from '../../../controller/fiha-storage';

// eslint-disable-next-line
const warn = console.warn.bind(console);

class LocalForageModal extends React.Component {
  state = {
    id: '',
    keys: [],
  };

  componentDidMount() {
    this.list();
  }

  list = () => {
    storage
      .keys()
      .then(keys => this.setState({ keys }))
      .catch(warn);
  }

  filterList = (key) => {
    const { id } = this.state;
    if (!id) return true;
    return key.indexOf(id) > -1;
  }

  handleSaveEvent = (evt) => {
    if (evt && evt.preventDefault) evt.preventDefault();
    this.props.save(this.state.id);
  };

  load = id => this.props.load(id);

  render() {
    const { keys, id } = this.state;
    const saveImportLabel = 'Save';
    const localElements = [];

    const setStateValue = name => (evt) => {
      // eslint-disable-next-line no-console
      console.log('set', name, evt.target.checkValidity());
      this.setState({
        [name]: evt.target.value.trim(),
      });
    };

    localElements.push((
      <form key="idform" onSubmit={evt => evt.preventDefault()} className="container-fluid py-3">
        <div className="row mb-3">
          <div className="col">
            <InputGroup
              className="storage-new"
              onChange={setStateValue('id')}
              value={id}
              placeholder="storage-id"
              pattern="[a-zA-Z0-9_-]+"
              autoFocus
              addons={[
                {
                  text: saveImportLabel,
                  onClick: this.handleSaveEvent,
                  className: 'btn-primary',
                },
              ]}
            />
          </div>
        </div>
      </form>
    ));

    localElements.push((
      <ul key="list" className="container-fluid d-block storage-list">
        {keys.filter(this.filterList).map(key => (
          <StorageItem
            key={key}
            id={key}
            save={this.props.save}
            load={this.props.load}
            current={this.props.id === key}
          />
        ))}
      </ul>
    ));

    return localElements;
  }
}

LocalForageModal.propTypes = {
  id: PropTypes.string,
  save: PropTypes.func.isRequired,
  load: PropTypes.func.isRequired,
};

LocalForageModal.defaultProps = {
  id: null,
};

const mapStateToProps = ({
  app: { setupId: id },
}) => ({
  id,
});

export default connect(mapStateToProps)(LocalForageModal);
