import storage from '../../../controller/fiha-storage';
import configStorage from '../../../controller/fiha-config-storage';
import { prepareAssetsForStorage } from '../../assets/worker/AssetsControllerWorkerPlugin';

import BackendPlugin from '../../BackendPlugin';

export default class LocalForage extends BackendPlugin {
  async initialize() {
    const lastSetupId = await configStorage.getItem('lastSetup');
    if (lastSetupId) {
      this.process({
        type: 'STORAGE_LOAD',
        payload: {
          id: lastSetupId,
        },
      });
    }
  }

  prepareAssets = saving => prepareAssetsForStorage(saving.assets || []);

  workerPostReducer = (action) => {
    const {
      sendDefault,
      sendSetup,
      currentSetup,
    } = this.backend;
    const { type, payload } = action;

    switch (type) {
      case 'STORAGE_SAVE':
        this.prepareAssets(currentSetup)
          .then(assets => storage.setItem(payload.id, {
            ...currentSetup,
            id: payload.id,
            assets,
          })
            .then((response) => {
              this.dispatch({
                type: 'SET_SETUP_ID',
                payload: {
                  id: payload.id,
                  storageType: 'localForage',
                },
              });
              this.notifySuccess(`${response.id} saved`);
              this.actionCallback(action, response);
              configStorage.setItem('lastSetup', response.id);
            }))
          .catch(this.notifyFailure);
        break;

      case 'STORAGE_LOAD':
        storage.getItem(payload.id)
          .then((found) => {
            if (!found) {
              sendDefault();
              return;
            }
            this.dispatch({
              type: 'SET_SETUP_ID',
              payload: {
                id: payload.id,
                storageType: 'localForage',
              },
            });

            sendSetup({
              setup: {
                id: payload.id,
              },
              ...found,
            });

            this.actionCallback(action, { ...found });
            this.notifySuccess(`${payload.id} loaded`);
            configStorage.setItem('lastSetup', payload.id);
          })
          .catch(this.notifyFailure);
        break;

      case 'STORAGE_NEW':
        sendSetup({
          id: 'blank',
          setup: { id: 'blank' },
          layers: [],
        });
        break;


      default:
        // // eslint-disable-next-line no-console
        // console.info('client ws', type, payload);
    }
  };
}
