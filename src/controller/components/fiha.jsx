import React from 'react';
import ReactModal from 'react-modal';
import result from 'lodash.result';
import { ToastContainer, toast } from 'react-toastify';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

import FihaEditor from './Editor/Editor';
import Toolbar from './Toolbar/Toolbar';

import initializer from '../../plugins/initializer';
import Plugins from '../../plugins/controllerPlugins';

import AppTabs from './fiha.appTabs';
import defaultShortcuts from './fiha.default-shortcuts';

import './fiha.scss';

const isMac = navigator.platform.toLowerCase().indexOf('mac') > -1;

const {
  BroadcastChannel,
  addEventListener,
  removeEventListener,
} = window;

class Fiha extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      bpm: 120,
      readerKeys: [],
      displays: {},
      controlDisplayHeight: null,
    };

    this.broadcastChannel = new BroadcastChannel(props.broadcastChannelId);

    this.broadcast = (action) => {
      if (this.emit) {
        this.emit({
          type: 'BROADCAST',
          payload: action,
        });
      }
    };

    // eslint-disable-next-line no-underscore-dangle
    this._hookCache = {};
    this.initializeController();
  }

  componentDidMount() {
    addEventListener('keydown', this.shortcutListener);

    const opts = { passive: true, capture: false };

    this.broadcastChannel.removeEventListener('message', this.broadcastChannelListener, opts);
    this.broadcastChannel.addEventListener('message', this.broadcastChannelListener, opts);

    this.props.worker.removeEventListener('message', this.workerListener, opts);
    this.props.worker.addEventListener('message', this.workerListener, opts);

    this.emit({
      type: 'SET_BROADCAST_ID',
      payload: {
        id: this.props.broadcastChannelId,
      },
    });

    this.animate();
  }

  async componentWillUnmount() {
    if (this.aFRID) cancelAnimationFrame(this.aFRID);
    removeEventListener('keydown', this.shortcutListener);
    try {
      await this.unmountPlugins();
    } catch (err) {
      // eslint-disable-next-line no-console
      console.warn(err.stack);
    }

    const opts = { passive: true, capture: false };
    if (this.props.worker) {
      this.props.worker.removeEventListener('message', this.workerListener, opts);
    }
    if (this.broadcastChannel) {
      this.broadcastChannel.removeEventListener('message', this.broadcastChannelListener, opts);
      this.broadcastChannel.close();
    }
  }

  setData = (update) => {
    this.data = {
      ...this.data,
      ...update,
    };
  };

  get modalContent() {
    const {
      closeStorageDialog,
      state: { storageModalOpen },
    } = this;

    if (!storageModalOpen) return null;

    const PluginModal = this
      .hook('modal', storageModalOpen)
      .filter(v => v).pop();

    if (PluginModal) {
      return (
        <React.Suspense fallback={<div>Loading…</div>}>
          <PluginModal close={closeStorageDialog} />
        </React.Suspense>
      );
    }

    return null;
  }

  async initializeController() {
    this.data = {};
    this.workerCallbacks = {};

    const {
      plugins,
      unmountPlugins,
    } = await initializer(Plugins, {
      dispatch: this.props.dispatch,
      read: this.read,
      emit: this.emit,
      broadcast: this.broadcast,
      setData: this.setData,
      setState: (...args) => this.setState(...args),
      getProps: () => this.props,
      getState: () => this.state,
    });
    this.plugins = plugins;
    this.unmountPlugins = unmountPlugins;
    this.shortcuts = [
      ...defaultShortcuts,
      ...this.cachedHook('shortcuts')
        .reduce((acc, val) => [...acc, ...(val || [])], []),
    ];
  }

  hook = (name, ...rest) => (this.plugins || [])
    .map((plugin) => {
      if (rest.length && typeof plugin[name] === 'function') {
        return plugin[name](...rest);
      }
      return result(plugin, name);
    });

  cachedHook = (name, ...rest) => {
    // eslint-disable-next-line no-multi-assign, no-underscore-dangle
    this._hookCache[name] = this._hookCache[name] = this.hook(name, ...rest);
    // eslint-disable-next-line no-underscore-dangle
    return this._hookCache[name];
  };

  emit = (action, callback = null) => {
    const sent = { ...action };
    if (callback) {
      const id = `cb${(performance.now() * 10000).toFixed()}`;
      sent.meta = { ...(sent.meta || {}), callback: id };
      this.workerCallbacks[id] = callback;
    }
    this.props.worker.postMessage(sent);
  };

  animate = () => {
    if (!this.props.animate) {
      this.aFRID = requestAnimationFrame(this.animate);
      return;
    }

    this.emit({
      type: 'UPDATE_DATA',
      payload: this.data,
    });
    this.aFRID = requestAnimationFrame(this.animate);
  };

  read = key => (this.workerData || {})[key];

  shortcutListener = (evt) => {
    const ctrlKey = isMac
      ? evt.metaKey
      : evt.ctrlKey;

    const found = (this.shortcuts || []).find(sc =>
      sc.key.toLowerCase() === evt.key.toLowerCase() &&
      !sc.ctrl === !ctrlKey &&
      !sc.alt === !evt.altKey &&
      !sc.shift === !evt.shiftKey);

    if (!found) return;

    const func = (found.callback || this[found.name] || this.props[found.name]);
    if (!func) return;
    evt.preventDefault();

    try {
      func(...(found.args || []));
    } catch (err) {
      // eslint-disable-next-line no-console
      console.error(`Shortcut error ${err.message}`);
    }
  }

  downloadDataURL = (url, name = 'download.png') => {
    const a = document.createElement('a');
    a.href = url;
    a.download = name;

    document.body.appendChild(a);
    a.click();
    document.body.removeChild(a);
  };

  broadcastChannelListener = (evt) => {
    const { data } = evt;
    const { payload } = data;

    switch (data.type) {
      case 'UPDATE_DATA':
        this.workerData = payload;
        break;

      case 'CAPTURED_DISPLAY':
        this.downloadDataURL(payload.url);
        break;

      case 'REGISTER_DISPLAY':
        this.setState(({ displays }) => ({
          displays: {
            ...displays,
            [payload.id]: payload,
          },
        }));
        break;

      case 'RESIZE_DISPLAY':
        this.setState(({ displays }) => ({
          displays: {
            ...displays,
            [payload.id]: {
              ...displays[payload.id],
              width: payload.width,
              height: payload.height,
            },
          },
        }));

        if (payload.id === 'main' && payload.width) {
          this.setState({
            controlDisplayHeight: this.displayComponent.clientWidth
              * (payload.height / payload.width),
          });
        }
        break;

      case 'REMOVE_DISPLAY':
        this.setState(({ displays }) => ({
          ...displays,
          [payload.id]: undefined,
        }));
        break;

      default:
    }

    this.hook('onBroadcast', data);
  };

  workerListener = (evt) => {
    const { dispatch } = this.props;
    const { data } = evt;
    const { payload, meta } = data;
    switch (data.type) {
      case 'WORKER_CALLBACK':
        break;

      case 'NOTIFICATION':
        toast[payload.type || 'warning'](payload.message || '???');
        break;

      case 'UPDATE_STATE':
        dispatch(data);
        break;

      default:
        toast.warning(`Unknown "${data.type}" action`);
    }

    this.hook('onMessage', data);

    const cbId = (meta && meta.callback);
    if (cbId && typeof this.workerCallbacks[cbId] === 'function') {
      this.workerCallbacks[cbId](payload);
      delete this.workerCallbacks[cbId];
    }
  };

  openDisplay = () => {
    window.open(
      `/display/?did=${(Math.random() * 10000).toFixed()}&bcid=${this.props.broadcastChannelId}`,
      'fiha-display',
      'menubar=no,toolbar=no,location=no',
    );
  };

  openStorageMenu = storageName => this.setState({
    storageModalOpen: storageName,
  });

  closeStorageDialog = () => this.setState({
    storageModalOpen: false,
  });

  refDisplay = (el) => {
    this.displayComponent = el;
  };

  captureDisplay = () => {
    this.broadcast({ type: 'CAPTURE_DISPLAY' });
  };

  registerEditor = (editor) => {
    this.editor = editor;
  };

  addSnippet = (snippet) => {
    if (!this.editor) return;
    this.editor.addSnippet(snippet);
  };

  refIframe = (el) => {
    this.iframeEl = el;
  };

  renderToolbar = () => {
    const toolbarProps = {
      plugins: this.plugins || [],
      emit: this.emit,
      openStorageMenu: this.openStorageMenu,
      openDisplay: this.openDisplay,
      captureDisplay: this.captureDisplay,
    };

    return (
      <Toolbar key="toolbar" {...toolbarProps} />
    );
  };

  renderModal = () => {
    const { storageModalOpen } = this.state;
    const modalProps = {
      onRequestClose: this.closeStorageDialog,
      isOpen: !!storageModalOpen, //eslint-disable-line
      shouldCloseOnOverlayClick: true,
      contentLabel: 'Storage Dialog',
      portalClassName: 'storage-dialog',
      closeTimeoutMS: 218,
      className: {
        base: 'storage-dialog__content',
        afterOpen: 'storage-dialog__content--after-open',
        beforeClose: 'storage-dialog__content--before-close',
      },
      overlayClassName: {
        base: 'storage-dialog__overlay',
        afterOpen: 'storage-dialog__overlay--after-open',
        beforeClose: 'storage-dialog_u_overlay--before-close',
      },
    };

    return (
      <ReactModal key="modal" {...modalProps} ariaHideApp={false}>
        {this.modalContent}
      </ReactModal>
    );
  };

  renderEditor = () => {
    const { type, role } = this.props.edit;
    return (
      <FihaEditor
        emit={this.emit}
        registerEditor={this.registerEditor}
        key={`${type}-${role}`}
      />
    );
  }

  render() {
    const {
      displays,
      bpm,
      readerKeys,
      controlDisplayHeight,
    } = this.state;

    const iframeUrl = `./display/?bcid=${this.props.broadcastChannelId}&did=controller`;

    return [
      this.renderToolbar(),

      <ToastContainer key="toast-container" />,

      this.renderModal(),

      <div key="layout" className="fiha-info__holder fiha-layout d-flex align-items-stretch">
        <div key="col-1" className="display-n-controls">
          <div ref={this.refDisplay} className="display-layers">
            <iframe
              id="fiha-display"
              title="display"
              src={iframeUrl}
              style={{ height: `${controlDisplayHeight}px` }}
            />
          </div>

          <AppTabs
            emit={this.emit}
            read={this.read}
            addSnippet={this.addSnippet}
            controllerState={{
              displays,
              bpm,
              readerKeys,
            }}
            shortcuts={this.shortcuts || []}
            plugins={this.plugins || []}
          />
        </div>

        {this.renderEditor()}
      </div>,
    ];
  }
}

Fiha.propTypes = {
  dispatch: PropTypes.func.isRequired,
  animate: PropTypes.bool.isRequired,
  broadcastChannelId: PropTypes.string.isRequired,
  worker: PropTypes.instanceOf(Worker).isRequired,
  layers: PropTypes.array.isRequired,
  signal: PropTypes.shape({
    setup: PropTypes.string,
    animation: PropTypes.string,
  }),
  edit: PropTypes.shape({
    value: PropTypes.string,
    id: PropTypes.string,
    type: PropTypes.string,
    role: PropTypes.string,
  }),
};

Fiha.defaultProps = {
  signal: {
    setup: '',
    animation: '',
  },
  edit: {
    value: '',
    id: '',
    type: '',
    role: '',
  },
};


const mapStateToProps = ({
  layers = [],
  editor,
  app,
  workerScripts,
}) => ({
  layers,
  edit: editor,
  signal: workerScripts,
  ...app,
});

export default connect(mapStateToProps)(Fiha);
