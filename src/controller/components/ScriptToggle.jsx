/* eslint-disable react/prop-types */
import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

import './ScriptToggle.scss';

const ScriptRoleToggle = ({
  editor,
  id,
  type,
  emit,
  hasSetup,
  hasAnimation,
}) => {
  const toggle = wanted => () => emit({
    type: 'EDITOR_SCRIPT',
    payload: {
      id,
      type,
      role: wanted,
    },
  });

  return (
    <span className="script-toggle btn-group btn-group-sm">
      <button
        title="Setup script"
        className={`btn btn-${
          (id === editor.id && editor.role === 'setup'
            ? 'warning'
            : 'secondary')}${editor.errors[`${id}__setup`] ? ' has-error' : ''} ${!hasSetup ? 'empty' : ''}`.trim()}
        onClick={toggle('setup')}
      >
        S
      </button>

      <button
        title="Animation script"
        className={`btn btn-${
          (id === editor.id && editor.role === 'animation'
            ? 'warning'
            : 'secondary')}${editor.errors[`${id}__animation`] ? ' has-error' : ''} ${!hasAnimation ? 'empty' : ''}`}
        onClick={toggle('animation')}
      >
        A
      </button>
    </span>
  );
};

ScriptRoleToggle.propTypes = {
  editor: PropTypes.shape({
    id: PropTypes.string,
    type: PropTypes.string,
    role: PropTypes.string,
  }).isRequired,
  id: PropTypes.string,
  type: PropTypes.string,
  emit: PropTypes.func.isRequired,
  hasSetup: PropTypes.bool,
  hasAnimation: PropTypes.bool,
};

ScriptRoleToggle.defaultProps = {
  id: null,
  type: null,
  hasSetup: null,
  hasAnimation: null,
};

const mapStateToProps = ({ editor }) => ({
  editor,
});

export default connect(mapStateToProps)(ScriptRoleToggle);
