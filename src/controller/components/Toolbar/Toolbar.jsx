import React from 'react';
import { PortalWithState } from 'react-portal';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

import './Toolbar.scss';

export function storageMenu({
  plugins,
  portalStyle,
  selectItem,
  closePortal,
}) {
  const clickHandler = (evt) => {
    selectItem(evt);
    closePortal();
  };

  return (
    <div key="storagemenu" style={portalStyle} className="toolbar-menu">
      <ul className="toolbar-menu__items">
        {plugins
          .map(plugin => (
            plugin.toolbarMenu
            && plugin.toolbarMenu.storage
          ))
          .reduce((acc, val) => [...acc, ...(val || [])], [])
          .filter(v => v)
          .map(item => (
            <li key={item.action} className="toolbar-menu__item">
              <a
                data-action={item.action}
                className="toolbar__button"
                onClick={clickHandler}
                title={item.shortcut}
              >
                {item.text}
              </a>
            </li>
          ))}

        {/*
        <li className="toolbar-menu__item">
          <a
            data-action="new"
            className="toolbar__button"
            onClick={clickHandler}
            title="ALT + N"
          >
            New
          </a>
        </li>

        <li className="toolbar-menu__item">
          <a
            data-action="save"
            className="toolbar__button"
            onClick={clickHandler}
            title="CTRL + S"
          >
            Save
          </a>
        </li>
        <li className="toolbar-menu__item">
          <a
            data-action="revert"
            className="toolbar__button"
            onClick={clickHandler}
            title="CTRL + R"
          >
            Revert
          </a>
        </li>
        */}
      </ul>
    </div>
  );
}

storageMenu.propTypes = {
  plugins: PropTypes.array.isRequired,
  portalStyle: PropTypes.object.isRequired,
  selectItem: PropTypes.func.isRequired,
  closePortal: PropTypes.func.isRequired,
};


class Toolbar extends React.Component {
  state = {
    top: 0,
    left: 0,
  };

  openStorageMenu = openPortal => (evt) => {
    const toolbarBox = evt.target.closest('.toolbar').getBoundingClientRect();
    const itemBox = evt.target.getBoundingClientRect();
    this.setState({
      top: toolbarBox.bottom,
      left: itemBox.left,
    });
    openPortal(evt);
  };

  selectStorageMenuItem = (evt) => {
    const link = evt.target;
    const action = link.getAttribute('data-action');

    // if (action === 'new') {
    //   this.props.newSetup();
    //   return;
    // }

    this.props.openStorageMenu(action);
  };

  toggleAnimation = () => this.props.emit({ type: 'TOGGLE_ANIMATION' });

  changeScale = value => () => this.props.emit({
    type: 'DISPLAY_SCALE',
    payload: {
      scale: value,
    },
  });

  render() {
    const {
      plugins,
      scale,
      animate,
      openDisplay,
      setupId,
      storageType,
      captureDisplay,
    } = this.props;

    const portalStyle = {
      position: 'absolute',
      top: `${this.state.top - 1}px`,
      left: `${this.state.left}px`,
      right: 'auto',
      bottom: 'auto',
      borderRadius: '0',
    };

    const storageMenuPortal = ({
      openPortal,
      closePortal,
      isOpen,
      portal,
    }) => [
      <a
        key="storagebtn"
        className={`toolbar__button${isOpen ? ' toolbar__item--open' : ''}`}
        onClick={this.openStorageMenu(openPortal)}
      >
        Storage
      </a>,
      portal(storageMenu({
        plugins,
        closePortal,
        portalStyle,
        selectItem: this.selectStorageMenuItem,
      })),
    ];

    return (
      <header className="toolbar">
        <ul className="toolbar__items">
          <li className="toolbar__item">
            <img className="toolbar__logo" src="static/vf.icon.svg" alt="Fiha logo" />
          </li>
          <li className="toolbar__item">
            <PortalWithState closeOnOutsideClick closeOnEsc>
              {storageMenuPortal}
            </PortalWithState>
          </li>

          <li className="toolbar__item">
            <a className="toolbar__button" onClick={this.toggleAnimation}>{animate ? 'Pause' : 'Play'}</a>
          </li>

          <li className="toolbar__item">
            <a className="toolbar__button" onClick={openDisplay}>Open display</a>
          </li>

          <li className="toolbar__item toolbar__item--btn-group">
            <div className="btn-group btn-group-sm">
              <button
                className="btn btn-secondary"
                disabled={scale === 0.5}
                onClick={this.changeScale(0.5)}
              >
                0.5
              </button>
              <button
                className="btn btn-secondary"
                disabled={scale === 0.625}
                onClick={this.changeScale(0.625)}
              >
                0.625
              </button>
              <button
                className="btn btn-secondary"
                disabled={scale === 0.75}
                onClick={this.changeScale(0.75)}
              >
                0.75
              </button>
              <button
                className="btn btn-secondary"
                disabled={scale === 0.875}
                onClick={this.changeScale(0.875)}
              >
                0.875
              </button>
              <button
                className="btn btn-secondary"
                disabled={scale === 1}
                onClick={this.changeScale(1)}
              >
                1
              </button>
              <button
                className="btn btn-secondary"
                onClick={captureDisplay}
              >
                capture
              </button>
            </div>
          </li>

          <li className="toolbar__item toolbar__item--divider" />

          <li className="toolbar__item toolbar__item--spacer" />
          <li className="toolbar__item">
            <span className="toolbar__label">
              {storageType}: {setupId}
            </span>
          </li>
        </ul>
      </header>
    );
  }
}

Toolbar.propTypes = {
  plugins: PropTypes.array.isRequired,
  emit: PropTypes.func.isRequired,
  openStorageMenu: PropTypes.func.isRequired,
  animate: PropTypes.bool,
  openDisplay: PropTypes.func.isRequired,
  setupId: PropTypes.string,
  scale: PropTypes.number.isRequired,
  captureDisplay: PropTypes.func.isRequired,
  storageType: PropTypes.string.isRequired,
};

Toolbar.defaultProps = {
  setupId: null,
  animate: false,
};

const mapStateToProps = ({
  app: { animate, scale, setupId, storageType },
}) => ({
  animate,
  scale,
  setupId,
  storageType,
});

export default connect(mapStateToProps)(Toolbar);
