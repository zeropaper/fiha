import React from 'react';
import PropTypes from 'prop-types';
import Editor from '@monaco-editor/react';

import { connect } from 'react-redux';

const CodeEditor = ({
  id,
  type,
  role,
  language,
  value,
  editorDidMount,
  onChange,
  width,
  height,
}) => (
  <Editor
    width={width || '100%'}
    height={height || '100%'}
    key={[type, id, role].join('-')}
    theme="dark"
    className="fiha-editor__monaco"
    value={value}
    language={language}
    editorDidMount={(getValue, mounted) => {
      editorDidMount(getValue, mounted);
      if (mounted) {
        mounted.onDidChangeModelContent(ev => onChange(ev, getValue()));
      }
    }}
    options={{
      renderWhitespace: 'all',
      wordWrap: 'bounded',
      colorDecorators: true,
      formatOnPaste: true,
      formatOnType: true,
      automaticLayout: false,
    }}
  />
);

CodeEditor.propTypes = {
  id: PropTypes.string.isRequired,
  type: PropTypes.string.isRequired,
  role: PropTypes.string.isRequired,
  value: PropTypes.string.isRequired,
  editorDidMount: PropTypes.func.isRequired,
  onChange: PropTypes.func.isRequired,
  language: PropTypes.string,
  width: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  height: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
};

CodeEditor.defaultProps = {
  language: 'javascript',
  width: null,
  height: null,
};

const mapStateToProps = ({
  editor: {
    id,
    type,
    role,
  },
}) => ({
  id,
  type,
  role,
});

export default connect(mapStateToProps)(CodeEditor);
