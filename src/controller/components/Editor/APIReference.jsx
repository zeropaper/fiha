import React from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';

import './APIReference.scss';
import { apiReferences } from '../apiDefinitions';

export const Snippet = ({
  value,
  // fontSize = 14,
  // maxLines = 30,
  // tabSize = 2,
}) => (
  <pre>{value}</pre>
);

Snippet.propTypes = {
  value: PropTypes.string,
  // fontSize: PropTypes.number,
  // maxLines: PropTypes.number,
  // tabSize: PropTypes.number,
};

Snippet.defaultProps = {
  value: '',
  // fontSize: 14,
  // maxLines: 30,
  // tabSize: 2,
};


export const APIReferenceTopicItem = ({
  topic,
  addSnippet,
  showReference,
  description,
  focused,
  type,
  category,
}) => (
  <li
    className={`api-reference__list-item ${
      focused ? 'api-reference__list-item--focused' : ''
    }`}
  >
    <a
      className="api-reference__add-snippet-link"
      onClick={addSnippet}
      title="Add snippet in editor"
      href="#"
    >
      +
    </a>

    <span
      className={`api-reference__details-link api-reference__name api-reference__name--${type}`}
      onClick={showReference}
      title={description}
    >
      <span>{topic}</span>
      {type === 'function' && '()'}
    </span>
    {category && (
      <span
        className={`api-reference__category api-reference__category--${category}`}
      >
        {category}
      </span>
    )}
  </li>
);

APIReferenceTopicItem.propTypes = {
  showReference: PropTypes.func.isRequired,
  addSnippet: PropTypes.func.isRequired,
  topic: PropTypes.string.isRequired,
  description: PropTypes.string,
  focused: PropTypes.bool,
  type: PropTypes.string,
  category: PropTypes.string,
};

APIReferenceTopicItem.defaultProps = {
  description: '',
  focused: false,
  type: '',
  category: '',
};


export const APIReferenceTopicDetails = ({
  addSnippet,
  topic,
  usage,
  snippet,
  description,
  type,
  link,
}) => (
  <div className="api-reference__details">
    <div className="api-reference__meta">
      <span className={`api-reference__name api-reference__name--${type}`}>
        <span>{topic}</span>
        {type === 'function' ? '()' : ''}
      </span>

      {link && (
        link.indexOf('https://developer.mozilla.org') === 0 ?
        (
          <a
            href={link}
            className="api-reference__link api-reference__link--mdn"
            target="_blank"
            rel="noopener noreferrer"
            title={`MDN documentation for ${topic}`}
          >
            <span className="invisible">MDN documentation for {topic}</span>
          </a>
        ) :
        (
          <a
            href={link}
            className="api-reference__link"
            target="_blank"
            rel="noopener noreferrer"
            title={`Documentation for ${topic}`}
          >
            Docs
          </a>
        )
      )}
    </div>

    <div className="api-reference__description">{description}</div>

    {usage && (<Snippet value={usage} />)}

    {snippet && snippet !== usage && (<Snippet value={snippet} />)}

    {snippet && (
      <div key="button" className="api-reference__add-snippet-button">
        <button className="form-control form-control-sm" onClick={addSnippet}>
          Add snippet
        </button>
      </div>
    )}
  </div>
);

APIReferenceTopicDetails.propTypes = {
  topic: PropTypes.string.isRequired,
  addSnippet: PropTypes.func.isRequired,
  usage: PropTypes.string,
  snippet: PropTypes.string,
  description: PropTypes.string,
  type: PropTypes.string,
  link: PropTypes.string,
};

APIReferenceTopicDetails.defaultProps = {
  usage: '',
  snippet: '',
  description: '',
  type: '',
  link: '',
};


class APIReference extends React.Component {
  state = {
    searched: null,
    topic: null,
  };

  onSearchChange = evt => this.setState({
    searched: evt.target.value,
  });

  filtered = () => {
    const { searched } = this.state;
    return Object.keys(apiReferences[this.props.type])
      .filter(item => item.indexOf(searched || '') > -1);
  };

  addTopicSnippet = topic => (evt) => {
    if (evt) evt.preventDefault();
    const {
      props: { addSnippet },
    } = this;

    const info = apiReferences[this.props.type][topic];
    const { snippet } = info;

    addSnippet(snippet || (info.type === 'function' ? `${topic}()` : topic));
  };

  handleShowTopic = topic => (evt) => {
    if (evt) evt.preventDefault();
    this.setState({ topic });
  };

  render() {
    const { open, type } = this.props;
    const topicInfo = apiReferences[type][this.state.topic];

    return (
      <div className={`api-reference${open ? ' api-reference--open' : ''}`}>
        <input
          placeholder="Search API"
          className="api-reference__search-input form-control form-control-sm"
          type="search"
          onChange={this.onSearchChange}
        />
        <ul className="api-reference__list">
          {this.filtered().map(topic => (
            <APIReferenceTopicItem
              key={topic}
              focused={this.state.topic === topic}
              showReference={this.handleShowTopic(topic)}
              addSnippet={this.addTopicSnippet(topic)}
              topic={topic}
              {...apiReferences[type][topic]}
            />
          ))}
        </ul>

        {topicInfo && (
          <APIReferenceTopicDetails
            {...topicInfo}
            addSnippet={this.addTopicSnippet(this.state.topic)}
            topic={this.state.topic}
          />
        )}
      </div>
    );
  }
}

APIReference.propTypes = {
  open: PropTypes.bool,
  type: PropTypes.string.isRequired,
  addSnippet: PropTypes.func.isRequired,
};

APIReference.defaultProps = {
  open: false,
};

const mapStateToProps = ({ editor: { type } }) => ({ type });

export default connect(mapStateToProps)(APIReference);
