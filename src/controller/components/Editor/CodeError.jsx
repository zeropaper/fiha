/* eslint-disable react/prop-types */
/* eslint-disable react/no-array-index-key */
import React from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';

import './CodeError.scss';

export const readStack = (stack, lineCorrection = 0) => {
  try {
    const [message = 'Unparseable message', calls = ''] = (stack || '').split('at eval').map(s => s.trim());
    // eslint-disable-next-line no-unused-vars
    const [junk1, linecolBegin = ''] = calls.split(', <anonymous>:');
    const [originalLine = lineCorrection, column = ''] = linecolBegin.split(':');
    return {
      message,
      line: parseInt(originalLine, 10) - lineCorrection,
      column: column.split(')')[0] || 0,
    };
  } catch (err) {
    return {
      message: err.message || 'Unparseable error stack',
      line: 0,
      column: 0,
    };
  }
};

const CodeError = ({
  editorError,
  executionErrors,
}) => (
  <div className="codeerror">
    {editorError && (
      <div className="codeerror__item codeerror__item--syntax">
        <strong className="codeerror__title">Syntax Error:</strong>
        {editorError.details
          ? editorError.details.map((error, e) => (
            <div key={`${error.reason}--${error.line}--${e}`}>
              line&nbsp;
              {error.line}
              :&nbsp;
              {error.reason}
            </div>
          ))
          : (
            <detail className="codeerror__message">
              <summary>{editorError.message || 'Unknown syntax error'}</summary>
              <pre>{editorError.stack || ''}</pre>
            </detail>
          )}
      </div>
    )}

    {(executionErrors || []).map(({
      display,
      stack,
      lineCorrection,
    }) => {
      const err = readStack(stack || '', lineCorrection || 0);

      return (
        <div className="codeerror__item codeerror__item--execution" key={display || 'no-display'}>
          <div className="codeerror__display">{display || 'no-display'}</div>

          <strong className="codeerror__title">Execution Error:</strong>

          <div className="codeerror__message">
            {err.message || 'Unknown execution error'} ({err.line || 0}:{err.column || 0})
          </div>
        </div>
      );
    })}
  </div>
);

CodeError.propTypes = {
  editorError: PropTypes.shape({
    message: PropTypes.string,
    stack: PropTypes.string,
  }),
  executionErrors: PropTypes.arrayOf(PropTypes.shape({
    stack: PropTypes.string,
    line: PropTypes.number,
    col: PropTypes.number,
  })).isRequired,
};

CodeError.defaultProps = {
  editorError: null,
};

const mapStateToProps = ({
  editor: {
    role,
    id,
    errors,
  },
}) => ({
  executionErrors: Object.keys(errors || {})
    .filter(key => key.indexOf(`${id}__${role}`) === 0 && errors[key])
    .map(key => errors[key]),
});

export default connect(mapStateToProps)(CodeError);
