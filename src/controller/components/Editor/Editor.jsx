import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

import './Editor.scss';

import Editor from './CodeEditor';
import { validateCode } from '../../../display/function-compiler';
import { argumentNames } from '../apiDefinitions';
import APIReference from './APIReference';
import ScriptRoleToggle from '../ScriptToggle';
import CodeError from './CodeError';

const EmptyEditor = () => (
  <div className="fiha-editor d-flex align-items-center justify-content-center">
    <div>
      Select a layer script.
    </div>
  </div>
);

class FihaEditor extends Component {
  state = {
    value: this.props.value,
    autoApply: this.props.autoApply || true,
    showHelp: false,
    error: null,
  };

  static getDerivedStateFromProps({ value: newPropValue }, { value: currStateValue }) {
    const newState = {};
    if (newPropValue.trim() !== currStateValue.trim()) {
      newState.value = newPropValue;
    }
    return newState;
  }

  componentDidMount() {
    this.handleResize();
  }

  componentDidUpdate() {
    this.handleResize();
  }

  /*
  // eslint-disable-next-line no-unused-vars
  componentDidUpdate({ value: prevPropValue }, { value: prevStateValue }) {
    this.handleResize();

    const { value: currPropValue } = this.props;
    const { value: currStateValue } = this.state;
    const propChanged = currPropValue !== prevPropValue;
    // const stateChanged = currStateValue !== prevStateValue;
    const propStateMatch = currPropValue === currStateValue;
    // console.info('prop %s, state %s', propChanged, stateChanged, propStateMatch);

    if (propChanged && !propStateMatch) {
      console.info('Background update!!!');
      requestAnimationFrame(() => this.setState({ value: currPropValue }));
    }
  }
  */

  get edtiorSize() {
    const {
      clientWidth: width,
      clientHeight: height,
    } = this.editorHolder;
    return { width, height };
  }

  handleResize = () => requestAnimationFrame(() => {
    if (!this.editor) return;
    this.editor.layout(this.edtiorSize);
  });

  handleEditorMount = (getValue, editor) => {
    const { registerEditor } = this.props;
    this.editor = editor;
    this.getValue = getValue;
    if (editor) {
      editor.onDidLayoutChange(({ width, height }) => requestAnimationFrame(() => {
        const verif = this.edtiorSize;
        if (width !== verif.width || height !== verif.height) {
          this.editor.layout(verif);
        }
      }));
      editor.onDidChangeModelContent(ev => this.handleChange(ev, getValue()));
    }
    registerEditor(this);
    this.handleResize();
    window.editor = editor;
  };

  handleToggleApi = (evt) => {
    evt.preventDefault();
    this.setState(({ showHelp }) => ({ showHelp: !showHelp }));
    this.handleResize();
  };

  handleToggleAutoApply = () => this.setState(({ autoApply }) => ({
    autoApply: !autoApply,
  }));

  addSnippet = (text) => {
    const { editor } = this;
    const ops = [];
    const id = { major: 1, minor: 1 };

    const selections = editor.getSelections();
    if (selections.length) {
      selections.forEach(({
        selectionStartLineNumber: startLine,
        selectionStartColumn: startColumn,
        endLineNumber: endLine,
        endColumn,
      }) => {
        // eslint-disable-next-line no-undef
        const range = new monaco.Range(startLine, startColumn, endLine, endColumn);
        ops.push({ identifier: id, range, text, forceMoveMarkers: true });
      });
    } else {
      const {
        lineNumber: startLine,
        lineNumber: endLine,
        column: startColumn,
        column: endColumn,
      } = editor.getPosition();
      // eslint-disable-next-line no-undef
      const range = new monaco.Range(startLine, startColumn, endLine, endColumn);
      ops.push({ identifier: id, range, text, forceMoveMarkers: true });
    }
    editor.executeEdits('addSnippet', ops);
  };

  applyChange = (value = this.state.value) => {
    const { id, role, type, emit } = this.props;
    emit({
      type: type === 'signal' ? 'UPDATE_WORKER_SCRIPTS' : 'EDIT_LAYER',
      payload: {
        id,
        [role]: value,
      },
    });
  };

  handleChange = (ev, value) => {
    const { value: current } = this.state;
    if (current.trim() === value.trim()) return value;
    const { validateCode: validate, type } = this.props;
    const error = validate(value, argumentNames[type]);
    if (!error) this.applyChange(value);
    this.setState({ value, error });
    return value;
  }

  handleApplyClick=() => {
    const { value } = this.state;
    const { validateCode: validate, type } = this.props;
    const error = validate(value, argumentNames[type]);
    if (!error) this.applyChange(value);
    this.setState({ value, error });
  }

  refEditorHolder = (el) => {
    this.editorHolder = el;
  };

  render() {
    const { className, type, id, role, emit } = this.props;
    const {
      error,
      value,
      autoApply,
      showHelp,
    } = this.state;
    if (!id || !type || !role) return (<EmptyEditor />);
    return (
      <div className={`${className} fiha-editor`.trim()}>
        <div className="fiha-editor__toolbar">
          <span className="fiha-editor__toolbar-id">{id}</span>
          <span className="fiha-editor__toolbar-type">
            {type}{' '}
            <a
              title={`Browse the ${type} API`}
              href="#"
              className="fiha-editor__toolbar-help"
              onClick={this.handleToggleApi}
            >
              {!showHelp ? '?' : '!'}
            </a>
          </span>

          <ScriptRoleToggle
            emit={emit}
            type={type}
            id={id}
          />
        </div>
        <div className="fiha-editor__columns">
          <APIReference
            open={showHelp}
            type={type}
            addSnippet={this.addSnippet}
          />

          <div
            className="fiha-editor__column fiha-editor__column--editor"
            ref={this.refEditorHolder}
          >
            <Editor
              onChange={this.handleChange}
              editorDidMount={this.handleEditorMount}
              value={value}
            />
          </div>
        </div>

        <CodeError editorError={error} />

        <form className="fiha-editor__status form-inline">
          <div>
            <button
              disabled={(autoApply)}
              className="btn btn-primary"
              onClick={this.handleApplyClick}
              title="CTRL + ENTER"
            >
              Apply
            </button>
          </div>
          <div className="for-check ml-sm-2">
            <div className="custom-control custom-checkbox custom-control-inline">
              <input
                id="auto-apply-checkbox"
                className="custom-control-input"
                type="checkbox"
                onChange={this.handleToggleAutoApply}
                defaultChecked={autoApply}
              />
              <label
                htmlFor="auto-apply-checkbox"
                title="Automatically apply changes"
                className="custom-control-label"
              >
                auto
              </label>
            </div>
          </div>
        </form>
      </div>
    );
  }
}

FihaEditor.propTypes = {
  emit: PropTypes.func.isRequired,
  autoApply: PropTypes.bool,
  registerEditor: PropTypes.func.isRequired,
  value: PropTypes.string,
  role: PropTypes.string,
  type: PropTypes.string,
  id: PropTypes.string,
  validateCode: PropTypes.func,
  className: PropTypes.string,
  layer: PropTypes.shape({
    setup: PropTypes.string.isRequired,
    animation: PropTypes.string.isRequired,
  }),
  workerScripts: PropTypes.shape({
    setup: PropTypes.string.isRequired,
    animation: PropTypes.string.isRequired,
  }),
};

FihaEditor.defaultProps = {
  autoApply: true,
  value: '',
  validateCode,
  role: '',
  type: '',
  id: '',
  className: '',
  layer: null,
  workerScripts: {
    setup: null,
    animation: null,
  },
};

const mapStateToProps = ({
  editor: {
    role,
    id,
    type,
    autoApply = true,
  },
  layers,
  workerScripts,
}) => {
  let value = '';
  let layer;
  if (type && role) {
    layer = type !== 'signal' ? (layers.find(l => l.id === id) || {}) : null;
    value = type === 'signal'
      ? (workerScripts[role] || '')
      : (layer[role] || '');
  }

  return {
    role,
    id,
    type,
    value,
    autoApply,
    layer,
    workerScripts,
  };
};

export default connect(mapStateToProps)(FihaEditor);
