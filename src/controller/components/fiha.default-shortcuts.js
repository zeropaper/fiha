const defaultShortcuts = [
  {
    ctrl: true,
    key: 'p',
    name: 'toggleAnimation',
  },
  {
    ctrl: true,
    key: 'd',
    name: 'openDisplay',
  },
];

export default defaultShortcuts;
