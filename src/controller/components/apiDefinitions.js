import mathTools, { apiReference as mathToolsReference } from '../../display/math-tools';
import miscTools, { apiReference as miscToolsReference } from '../../display/misc-tools';

import {
  argumentNames as canvasArgumentNames,
  apiReference as canvasReference,
} from '../../display/components/canvas-display-layer';
import {
  argumentNames as threeArgumentNames,
  apiReference as threeReference,
} from '../../display/components/three-display-layer';
import {
  argumentNames as paperArgumentNames,
  apiReference as paperReference,
} from '../../display/components/paper-display-layer';

export const argumentNames = {
  canvas: canvasArgumentNames,
  three: threeArgumentNames,
  paper: paperArgumentNames,
  signal: Object.keys({
    ...mathTools,
    ...miscTools,
  }),
};

export const apiReferences = {
  canvas: canvasReference,
  three: threeReference,
  paper: paperReference,
  signal: {
    ...mathToolsReference,
    ...miscToolsReference,
  },
};
