import PropTypes from 'prop-types';
import React, { Component } from 'react';
import {
  SortableContainer,
  SortableElement,
  SortableHandle,
} from 'react-sortable-hoc';
import { connect } from 'react-redux';

import './ControlLayers.scss';
import {
  handleAddLayer as addLayer,
  handleEditLayer as editLayer,
  handleRemoveLayer as removeLayer,
  handleSortLayers as sortLayers,
} from '../../actions/layers';
import {
  handleEditSignal as editSignal,
} from '../../actions/setup';

import ScriptRoleToggle from '../ScriptToggle';

import ThreeDisplayLayer from '../../../display/components/three-display-layer';
import CanvasDisplayLayer from '../../../display/components/canvas-display-layer';
import PaperDisplayLayer from '../../../display/components/paper-display-layer';

const types = {
  three: ThreeDisplayLayer,
  canvas: CanvasDisplayLayer,
  paper: PaperDisplayLayer,
};

const ControlLayerHandle = () => (<span className="btn btn-sm btn-light drag-handle">↕</span>);

// eslint-disable-next-line babel/new-cap
const SortableControlLayerHandle = SortableHandle(ControlLayerHandle);

export function ControlLayer({
  id,
  type,
  active,
  invisible,
  onRemoveLayer,
  onEditLayer,
  emit,
  focused = false,
  hasSetup = false,
  hasAnimation = false,
}) {
  const htmlId = `ctrl-layer-${id}`;
  const focusedClass = focused ? ' control-layer--focused' : '';
  const activeClass = active ? ' control-layer--active' : '';
  const className = `row py-1 control-layer control-layer--${type}${focusedClass}${activeClass}`;
  return (
    <li className={className}>
      <div className="col">
        <button
          className="btn btn-sm btn-danger"
          onClick={() => onRemoveLayer(id)}
        >
          R
        </button>
      </div>

      <div className="col-4 control-layer__id">
        <div className="custom-control custom-checkbox custom-control-inline">
          <input
            id={`${htmlId}-active`}
            className="custom-control-input"
            type="checkbox"
            defaultChecked={active}
            onChange={() => onEditLayer({
              id,
              active: !active,
            })}
          />
          <label
            htmlFor={`${htmlId}-active`}
            title="Toggle layer"
            className="custom-control-label"
          >
            {id}
          </label>
        </div>
      </div>

      <div className="col text-right control-layer__invisible">
        <div className="custom-control custom-checkbox custom-control-inline">
          <input
            id={`${htmlId}-invisible`}
            className="custom-control-input"
            type="checkbox"
            defaultChecked={!invisible}
            onChange={() => onEditLayer({
              id,
              invisible: !invisible,
            })}
          />
          <label
            htmlFor={`${htmlId}-invisible`}
            title={invisible ? 'make visible' : 'make invisible'}
            className="custom-control-label"
          >
            V
          </label>
        </div>
      </div>

      <div className="col text-right">
        <SortableControlLayerHandle />
      </div>

      <div className="col text-right">
        <ScriptRoleToggle
          emit={emit}
          type={type}
          id={id}
          hasSetup={hasSetup}
          hasAnimation={hasAnimation}
        />
      </div>
    </li>
  );
}

ControlLayer.propTypes = {
  id: PropTypes.string.isRequired,
  type: PropTypes.string.isRequired,
  active: PropTypes.bool,
  onRemoveLayer: PropTypes.func.isRequired,
  onEditLayer: PropTypes.func.isRequired,
  emit: PropTypes.func.isRequired,
  focused: PropTypes.bool,
  invisible: PropTypes.bool,
  hasSetup: PropTypes.bool,
  hasAnimation: PropTypes.bool,
};

ControlLayer.defaultProps = {
  active: true,
  focused: false,
  invisible: null,
  hasSetup: false,
  hasAnimation: false,
};

// eslint-disable-next-line babel/new-cap
export const SortableLayer = SortableElement(ControlLayer);

export function ControlLayersList({
  edit,
  layers,
  emit,
  onEditLayer,
  onRemoveLayer,
  onEditCode,
}) {
  return (
    <ul className="control-layers__list">
      {layers.map((layer, index) => (
        <SortableLayer
          key={layer.id}
          index={index}
          {...layer}
          hasSetup={!!layer.setup}
          hasAnimation={!!layer.animation}
          emit={emit}
          edit={layer.id === edit.id ? edit.role : ''}
          onEditLayer={onEditLayer}
          onRemoveLayer={onRemoveLayer}
          onEditCode={onEditCode}
        />
      ))}
    </ul>
  );
}

ControlLayersList.propTypes = {
  edit: PropTypes.object.isRequired,
  layers: PropTypes.array.isRequired,
  emit: PropTypes.func.isRequired,
  onEditLayer: PropTypes.func.isRequired,
  onRemoveLayer: PropTypes.func.isRequired,
  onEditCode: PropTypes.func.isRequired,
};

// eslint-disable-next-line babel/new-cap
export const SortableLayers = SortableContainer(ControlLayersList);


class ControlLayers extends Component {
  handleSortEnd = info => this.props
    .emit(sortLayers(info.oldIndex, info.newIndex));

  handleEditLayer = (...args) => this.props
    .emit(editLayer(...args));

  handleRemoveLayer = (...args) => this.props
    .emit(removeLayer(...args));

  handleEditCode = (...args) => this.props
    .emit(editSignal(...args));

  handleAddLayer = (evt) => {
    evt.preventDefault(evt);
    const parent = evt.target.parentNode;
    const val = name => parent.querySelector(`[name=${name}]`).value;
    const type = val('type');
    this.props.emit(addLayer(type, val('layerId'), types[type].defaultSetupScript, types[type].defaultAnimationScript));
    parent.querySelector('[name="layerId"]').value = '';
  };

  render() {
    return (
      <section className="control-layers container-fluid">
        <form className="py-3 control-layers__add-form">
          <div className="input-group">
            <input placeholder="layer-id" name="layerId" className="form-control form-control-sm" type="text" />

            <select name="type" className="form-control form-control-sm control-layers__add-type">
              {Object.keys(types).map(type => (
                <option
                  key={type}
                  value={type}
                >
                  {type}
                </option>
              ))}
            </select>

            <button
              className="btn btn-sm btn-primary input-group-addon control-layers__add-button"
              onClick={this.handleAddLayer}
            >
              Add layer
            </button>
          </div>
        </form>

        <SortableLayers
          edit={this.props.edit}
          emit={this.props.emit}
          layers={this.props.layers}
          onEditLayer={this.handleEditLayer}
          onRemoveLayer={this.handleRemoveLayer}
          onEditCode={this.handleEditCode}
          onSortEnd={this.handleSortEnd}
          useDragHandle
          axis="y"
          lockAxis="y"
        />
      </section>
    );
  }
}

ControlLayers.propTypes = {
  edit: PropTypes.object.isRequired,
  layers: PropTypes.array.isRequired,
  emit: PropTypes.func.isRequired,
};

const mapStateToProps = ({
  layers,
  editor,
}) => ({
  layers,
  edit: editor,
});

export default connect(mapStateToProps)(ControlLayers);
