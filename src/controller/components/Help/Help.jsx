import React from 'react';
import PropTypes from 'prop-types';

import './Help.scss';

const ctrlLabel = navigator.platform.toLowerCase().indexOf('mac') > -1
  ? ' + cmd'
  : ' + ctrl';

const HelpTab = ({ shortcuts }) => (
  <div className="help">
    <dl className="help__shortcuts">
      {shortcuts.map(sc => (
        <React.Fragment key={`${sc.key}-${!!sc.shift}-${!!sc.ctrl}-${!!sc.alt}`}>
          <dt className="help__shortcuts-title">{sc.title || 'Missing title'}</dt>
          <dd className="help__shortcuts-keys">
            {sc.key}
            {sc.shift ? ' + shift' : ''}
            {sc.ctrl ? ctrlLabel : ''}
            {sc.alt ? ' + alt' : ''}
          </dd>
        </React.Fragment>
      ))}
    </dl>
  </div>
);

HelpTab.propTypes = {
  shortcuts: PropTypes.arrayOf(PropTypes.shape({
    key: PropTypes.string.isRequired,
    ctrl: PropTypes.bool,
    alt: PropTypes.bool,
    shift: PropTypes.bool,
  })).isRequired,
};

export default HelpTab;
