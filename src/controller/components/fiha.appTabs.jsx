import React from 'react';
import PropTypes from 'prop-types';
import { Tab, Tabs, TabList, TabPanel } from 'react-tabs';
import { connect } from 'react-redux';

import ControlLayers from './Layers/ControlLayers';
import Displays from './Displays/Displays';
import HelpTab from './Help/Help';
import ScriptRoleToggle from './ScriptToggle';

const AppTabs = ({
  read,
  controllerState,
  addSnippet,
  plugins,
  emit,
  shortcuts,
}) => {
  const { displays } = controllerState;

  const compiledTabs = {
    Scripts: (
      <div className="scripts-wrapper">
        <div className="container-fluid">
          <div className="pt-3 row align-items-center">
            <div className="col text-right">
              Worker scripts
            </div>

            <div className="col-md-auto text-right">
              <ScriptRoleToggle
                emit={emit}
                id="signal"
                type="signal"
                hasSetup
                hasAnimation
              />
            </div>
          </div>
        </div>

        <ControlLayers emit={emit} />
      </div>
    ),
    Displays: (
      <Displays displays={displays} />
    ),
    Help: (
      <HelpTab shortcuts={shortcuts} />
    ),
  };

  plugins.forEach((plugin) => {
    (plugin.tabs || []).forEach((tab) => {
      compiledTabs[tab.title] = (
        <tab.component
          read={read}
          emit={emit}
          controllerState={controllerState}
          addSnippet={addSnippet}
        />
      );
    });
  });

  return (
    <Tabs className="controls">
      <TabList>
        {Object.keys(compiledTabs).map(key => <Tab key={key}>{key}</Tab>)}
      </TabList>

      {Object.keys(compiledTabs).map(key => (
        <TabPanel
          key={key}
          className={`react-tabs__tab-panel react-tabs__tab-panel--${key.toLowerCase()}`}
        >
          {compiledTabs[key]}
        </TabPanel>
      ))}
    </Tabs>
  );
};

AppTabs.propTypes = {
  emit: PropTypes.func.isRequired,
  read: PropTypes.func.isRequired,
  plugins: PropTypes.arrayOf(PropTypes.shape({
    tabs: PropTypes.arrayOf(PropTypes.shape({
      title: PropTypes.string.isRequired,
      component: PropTypes.func.isRequired,
    })).isRequired,
  })).isRequired,
  controllerState: PropTypes.shape({
    layers: PropTypes.array.isRequired,
    edit: PropTypes.object.isRequired,
    displays: PropTypes.object.isRequired,
  }).isRequired,
  addSnippet: PropTypes.func.isRequired,
  shortcuts: PropTypes.arrayOf(PropTypes.shape({
    key: PropTypes.string.isRequired,
    ctrl: PropTypes.bool,
    alt: PropTypes.bool,
    shift: PropTypes.bool,
  })).isRequired,
};

const mapStateToProps = ({
  layers,
  editor,
  displays,
}, {
  controllerState,
}) => ({
  controllerState: {
    ...controllerState,
    layers,
    edit: editor,
    displays: displays || controllerState.displays || {},
  },
});

export default connect(mapStateToProps)(AppTabs);
