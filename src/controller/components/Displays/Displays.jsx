import React from 'react';
import PropTypes from 'prop-types';

import './Displays.scss';

const renderDisplay = display => (
  <li className="display" key={`${display.id}-${display.width}-${display.height}`}>
    <div className="display__preview">
      <div
        className="display__preview-inner"
        style={{
          height: `${(display.height / display.width) * 100}%`,
          top: `${((100 - ((display.height / display.width) * 100)) * 0.5) + 0}%`,
        }}
      >
        <div className="display__id">{display.id}</div>

        <div className="display__dimensions">
          {`${display.width || 0}x${display.height || 0}`}
        </div>
      </div>
    </div>
  </li>
);

const Displays = ({ displays }) => (
  <ul className="displays-list">
    {Object.values(displays || {}).map(renderDisplay)}
  </ul>
);

Displays.propTypes = {
  displays: PropTypes.object.isRequired,
};

export default Displays;
