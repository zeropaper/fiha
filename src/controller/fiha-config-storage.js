import localForage from 'localforage';

const exported = localForage.createInstance({
  // driver      : localforage.WEBSQL, // Force WebSQL; same as using setDriver()
  name: 'fihaconfig',
  version: 1.0,
  size: 4980736, // Size of database, in bytes. WebSQL-only for now.
  storeName: 'fiha_config', // Should be alphanumeric, with underscores.
  description: 'Visual Fiha configuration storage',
});

export default exported;
