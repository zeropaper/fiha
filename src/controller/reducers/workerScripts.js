import initialState from '../initialStoreState';

// eslint-disable-next-line max-len
export default function workerScriptsReducer(state = initialState.workerScripts, { type, payload }) {
  switch (type) {
    case 'REPLACE_WORKER_SCRIPT':
      return {
        ...initialState.workerScripts,
        ...payload,
      };

    case 'UPDATE_WORKER_SCRIPTS':
      return {
        setup: typeof payload.setup !== 'undefined'
          ? payload.setup
          : state.setup,
        animation: typeof payload.animation !== 'undefined'
          ? payload.animation
          : state.animation,
      };

    default:
      // // eslint-disable-next-line no-console
      // console.info(type, payload);
      return state;
  }
}
