export const findIndex = (array, id) => array.findIndex(obj => obj.id === id);

export const excludeActionType = (obj) => {
  const returned = {};

  Object.keys(obj).forEach((key) => {
    if (key === 'type') return;
    returned[key] = obj[key];
  });

  return returned;
};

export const mapReducer = (reducer, state, action) => state.map(obj => reducer(obj, action));
