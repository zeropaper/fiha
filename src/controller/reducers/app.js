import initialState from '../initialStoreState';

export default function appReducer(state = initialState.app, { type, payload }) {
  switch (type) {
    case 'DISPLAY_SCALE':
      return { ...state, scale: payload.scale };

    case 'TOGGLE_ANIMATION':
      return { ...state, animate: !state.animate };

    case 'SET_BROADCAST_ID':
      return {
        ...state,
        broadcastChannelId: payload.id,
      };

    case 'SET_SETUP_ID':
    case 'STORAGE_LOAD':
      return {
        ...state,
        setupId: payload.id,
        storageType: payload.storageType || 'localForage',
      };

    default:
      // // eslint-disable-next-line no-console
      // console.info(type, payload);
      return state;
  }
}
