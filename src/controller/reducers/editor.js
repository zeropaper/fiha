export default function editorReducer(state = {
  role: '',
  id: '',
  type: '',
}, action) {
  const { type, payload } = action;

  switch (type) {
    case 'SET_EXECUTION_ERROR':
      return {
        ...state,
        errors: {
          ...state.errors,
          [`${payload.id}__${payload.role}`]: payload,
        },
      };

    case 'UNSET_EXECUTION_ERROR':
      return {
        ...state,
        errors: {
          ...state.errors,
          [`${payload.id}__${payload.role}`]: undefined,
        },
      };

    case 'EDITOR_SWITCH_ROLE':
      return {
        ...state,
        role: payload,
      };

    case 'EDITOR_SCRIPT':
      return {
        ...state,
        ...payload,
      };

    default:
      return state;
  }
}
