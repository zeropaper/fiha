import arrayMove from 'array-move';
import { findIndex, mapReducer } from './utils';

/**
 * layers
 */
export function layerReducer(state, action) {
  if (state.id !== action.payload.id) return state;

  switch (action.type) {
    case 'EDIT_LAYER':
      return {
        active: true,
        setup: '',
        animation: '',
        ...state,
        ...action.payload,
      };

    default:
      return state;
  }
}

export default function layersReducer(state = [], action) {
  let index;

  switch (action.type) {
    case 'ADD_LAYER':
      return [
        ...state,
        action.payload,
      ];

    case 'EDIT_LAYER':
      return mapReducer(layerReducer, state, {
        ...action,
      });

    case 'REMOVE_LAYER':
      index = findIndex(state, action.payload.id);
      if (index > -1) {
        return [
          ...state.slice(0, index),
          ...state.slice(index + 1),
        ];
      }
      return state;

    case 'REPLACE_LAYERS':
      return action.payload;

    case 'SORT_LAYERS':
      return arrayMove(state, action.payload.oldIndex, action.payload.newIndex);

    default:
      return state;
  }
}
