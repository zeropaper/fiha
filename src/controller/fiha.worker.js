import Backend from './Backend';
import plugins from '../plugins/controllerWorkerPlugins';

const backend = worker => new Backend({
  worker,
  plugins,
});

// eslint-disable-next-line no-restricted-globals
backend(self);
