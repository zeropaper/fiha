export const toggleAnimation = () => ({
  type: 'TOGGLE_ANIMATION',
});

export const handleEditSignal = payload => ({
  type: 'REPLACE_WORKER_SCRIPT',
  payload,
});
