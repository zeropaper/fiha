export const mkId = id => id || `l${Date.now()}`;

export const handleAddLayer = (type, id, setup = '', animation = '') => ({
  type: 'ADD_LAYER',
  payload: {
    id: id || mkId(),
    type,
    setup,
    animation,
  },
});

export const handleEditLayer = layer => ({
  type: 'EDIT_LAYER',
  payload: layer,
});

export const handleRemoveLayer = id => ({
  type: 'REMOVE_LAYER',
  payload: {
    id,
  },
});

export const handleSortLayers = (oldIndex, newIndex) => ({
  type: 'SORT_LAYERS',
  payload: {
    oldIndex,
    newIndex,
  },
});
