export default {
  app: {
    broadcastChannelId: 'fiha',
    setupId: null, // 'default-setup',
    storageType: 'localForage', // git or gist too
    animate: true,
    scale: 1,
    bpm: 120,
  },
  editor: {
    id: null,
    type: null, // layer type or signal (for workerScript)
    role: null, // setup or animation
    autoApply: true,
    errors: {},
  },
  workerScripts: {
    setup: `const togglers = {
  'toggle-1': '1',
  'toggle-2': '2',
  'toggle-3': '3',
  'toggle-4': '4',
  'toggle-5': '5',
  'toggle-6': '6',
  'toggle-7': '7',
  'toggle-8': '8',
  'toggle-9': '9',
  'toggle-0': '0',
};

const toggles = {};
Object.keys(togglers).forEach(name => {
  toggles[name] = toggle(read, togglers[name]);
});
cache.toggles = toggles;
`,
    animation: `const bpm = read('bpm');
const now = read('now');
const vol = read('volume');
const frq = read('frequencies');

const data = {
  b2: beatPrct(now, bpm / 2),
  b4: beatPrct(now, bpm / 4),
  b8: beatPrct(now, bpm / 8),
  b16: beatPrct(now, bpm / 16),
  b32: beatPrct(now, bpm / 32),
  volAvg: arrayAvg(vol),
  frqAvg: arrayAvg(frq),
};

Object.keys(cache.toggles).forEach(name => {
  cache.toggles[name](() => {
    data[name] = true;
  }, () => {
    data[name] = false;
  });
});

console.info('data', data)

return data;
`,
  },
  assets: [],
  layers: [],
};
