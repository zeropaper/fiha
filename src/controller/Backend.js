import functionCompiler from '../display/function-compiler';
import mathTools from '../display/math-tools';
import miscTools from '../display/misc-tools';

import configureStore from './workerStore';

export default class Backend {
  constructor({
    worker,
    broadcastChannelId = 'fiha',
    plugins = [],
  }) {
    this.worker = worker;
    this.data = {};
    this.audio = {};
    this.cache = {};
    this.displays = {};

    this.started = performance.now();
    this.workerNow = this.started;

    this.plugins = plugins.map(Plugin => new Plugin(this));
    this.store = configureStore(this.plugins);
    this.storeUnsubscribe = this.store.subscribe(this.updateUI);

    this.setBroadcastId(broadcastChannelId, true);
    this.worker.addEventListener('message', evt => this.workerReducer(evt.data));

    this.processData();
    if (this.initialize) this.initialize();
  }

  setupCallback = null;
  animationCallback = null;

  get state() {
    return this.store.getState();
  }

  get scriptArguments() {
    /* eslint-disable no-underscore-dangle */
    if (this._cachedArgs) return this._cachedArgs;

    const additions = Object.values(this.hook('scriptArguments'))
      .reduce((acc, obj) => {
        if (!obj) return acc;
        return {
          ...acc,
          ...obj,
        };
      }, {});

    // console.info('scriptArguments plugin additions', additions);

    this._cachedArgs = {
      ...additions,
      ...mathTools,
      ...miscTools,
      read: this.read,
    };
    return this._cachedArgs;
    /* eslint-enable no-underscore-dangle */
  }

  get scriptArgumentNames() {
    return [...Object.keys(this.scriptArguments), 'cache'];
  }

  get scriptArgumentValues() {
    return [...Object.values(this.scriptArguments), this.cache];
  }

  get currentSetup() {
    const {
      layers,
      workerScripts: signal,
      app: { setupId: id, bpm },
    } = this.state;
    const additions = Object.values(this.hook('currentSetup'))
      .reduce((acc, obj) => ({ ...acc, ...obj }), {});

    return {
      ...additions,
      bpm,
      id,
      layers,
      signal,
    };
  }


  batchDispatch = (actions, triggerAll = false) => {
    if (!triggerAll) this.storeUnsubscribe();

    actions.forEach(action => this.store.dispatch(action));

    if (!triggerAll) {
      this.storeUnsubscribe = this.store.subscribe(this.updateUI);
      this.updateUI();
    }
  };

  hook = (name, ...args) => {
    const { plugins } = this;

    return plugins
      .map(plugin => plugin.executeHook(name, ...args))
      .reduce((acc, returned, idx) => {
        if (typeof returned === 'undefined') return acc;
        return {
          ...acc,
          [plugins[idx].pluginId]: returned,
        };
      }, {});
  };

  updateTime = () => {
    const before = this.workerNow;
    this.workerNow = performance.now();
    const now = this.workerNow - this.started;
    this.data.bpm = this.data.bpm || 120;

    this.data = {
      ...this.data,
      now,
      beat: mathTools.beatPrct(now, this.data.bpm),
      beatNum: Math.floor(now / ((60 / this.data.bpm) * 1000)),
      'worker-frame-duration': this.workerNow - before,
    };
  };

  resetWorkerData = () => {
    this.cache = {};
    this.started = performance.now();
    this.data = {
      bpm: this.data.bpm || 120,
      started: this.started,
      now: 0,
      beat: 0,
      beatNum: 0,
      'worker-frame-duration': 0,
    };
    this.updateTime();
  };

  read = (varName, defaultValue = 0) => {
    if (typeof this.data[varName] === 'undefined') return defaultValue;
    return this.data[varName];
  };

  set = (data) => {
    Object.keys(data || {}).forEach((key) => {
      if (typeof data[key] === 'function') return;
      this.data[key] = data[key];
    });
    return this.data;
  };

  reset = () => { this.data = {}; };

  get = () => this.data;

  executeSetupScript = () => {
    this.resetWorkerData();
    if (typeof this.setupCallback !== 'function') return;
    const result = this.setupCallback(...this.scriptArgumentValues);

    this.set(result);
  };

  executeAnimationScript = () => {
    if (typeof this.animationCallback !== 'function') return;
    const result = this.animationCallback(...this.scriptArgumentValues);

    this.set(result);
  };

  broadcast = action => this.broadcastChannel.postMessage(action);

  emit = action => this.worker.postMessage(action);

  notifySuccess = message => this.emit({
    type: 'NOTIFICATION',
    payload: {
      type: 'success',
      message,
    },
  });

  notifyFailure = error => this.emit({
    type: 'NOTIFICATION',
    payload: {
      type: 'error',
      message: error.message,
    },
  });

  compileAnimationScript = () => {
    const setup = this.currentSetup;
    if (!setup.signal || !setup.signal.animation) {
      this.animationCallback = null;
      return;
    }

    this.animationCallback = functionCompiler({
      name: 'workerAnimation',
      body: setup.signal.animation,
      fallback: this.animationCallback,
      onExecutionError: this.logAnimationExecutionError,
      argumentNames: this.scriptArgumentNames,
    });
  };

  compileSetupScript = () => {
    const setup = this.currentSetup;
    if (!setup.signal || !setup.signal.setup) {
      this.setupCallback = null;
      return;
    }

    this.setupCallback = functionCompiler({
      name: 'workerSetup',
      body: setup.signal.setup,
      fallback: this.setupCallback,
      onExecutionError: this.logSetupExecutionError,
      argumentNames: this.scriptArgumentNames,
    });
  };

  handleWorkerScriptChanges = (previous, payload) => {
    if (
      typeof payload.setup === 'string'
      && payload.setup !== previous.signal.setup
    ) {
      this.compileSetupScript();
      this.executeSetupScript();
    }

    if (
      typeof payload.animation === 'string'
      && payload.animation !== previous.signal.animation
    ) {
      this.compileAnimationScript();
    }
  };

  updateUI = () => {
    const current = this.store.getState();
    // eslint-disable-next-line no-underscore-dangle
    if (JSON.stringify(current) === this._prevState) return;
    this.emit({
      type: 'UPDATE_STATE',
      payload: current,
    });
    // eslint-disable-next-line no-underscore-dangle
    this._prevState = JSON.stringify(current);
  };

  resetStore = (data) => {
    const {
      layers = [],
      signal = { setup: '', animation: '' },
    } = data;
    this.batchDispatch([
      {
        type: 'REPLACE_LAYERS',
        payload: layers,
      },
      {
        type: 'REPLACE_WORKER_SCRIPT',
        payload: signal,
      },
    ]);
    this.hook('storeReset', data);
  };

  sendSetup = async (data) => {
    this.resetStore(data);
    const setup = this.currentSetup;


    this.compileSetupScript();
    this.compileAnimationScript();

    this.executeSetupScript();

    this.broadcast({
      type: 'UPDATE_DATA',
      payload: {
        signals: this.data,
        audio: this.audio,
      },
    });

    this.broadcast({
      type: 'REPLACE_LAYERS',
      payload: setup.layers,
    });

    this.broadcast({
      type: 'ASSETS_UPDATE',
      payload: setup.assets,
    });
  };

  handleExecutionError = ({
    id,
    display,
    role,
    stack,
    lineCorrection = 0,
  }) => {
    if (!stack) {
      return this.store.dispatch({
        type: 'UNSET_EXECUTION_ERROR',
        payload: {
          id,
          role,
          display,
        },
      });
    }

    return this.store.dispatch({
      type: 'SET_EXECUTION_ERROR',
      payload: {
        stack,
        id,
        role,
        display,
        lineCorrection,
      },
    });
  };

  handleWorkerExecutionError = role => err => this.handleExecutionError({
    id: 'signal',
    display: 'worker',
    role,
    stack: err ? err.stack : '',
    lineCorrection: err ? err.lineCorrection : 0,
  });

  logSetupExecutionError = this.handleWorkerExecutionError('setup');

  logAnimationExecutionError = this.handleWorkerExecutionError('animation');

  handleRegisterDisplay = (payload) => {
    const setup = this.currentSetup;

    this.displays[payload.id] = {};

    this.broadcast({
      type: 'BROADCAST_FRAME',
      payload: {
        signals: this.data,
        audio: this.audio,
      },
      meta: {
        target: payload.id,
      },
    });

    this.broadcast({
      type: 'REPLACE_LAYERS',
      payload: setup.layers,
      meta: {
        target: payload.id,
      },
    });
  };

  broadcastReducer = (action) => {
    this.hook('broadcastPreReducer', action);
    if (action.type === 'SET_EXECUTION_ERROR' || action.type === 'UNSET_EXECUTION_ERROR') {
      this.store.dispatch(action);
    } else if (action.type === 'REGISTER_DISPLAY') {
      this.handleRegisterDisplay(action.payload);
    } else if (action.type === 'RESIZE_DISPLAY') {
      const { id, ...rest } = action.payload;
      this.displays[id] = { ...this.displays[id], ...rest };
    }
    this.hook('broadcastPostReducer', action);
  };

  setBroadcastId = (id, silent) => {
    if (this.broadcastChannel && typeof this.broadcastChannel.close === 'function') {
      this.broadcastChannel.close();
    }
    this.broadcastChannel = new BroadcastChannel(id);
    this.broadcastChannel.addEventListener('message', evt => this.broadcastReducer(evt.data));
    if (!silent) this.notifySuccess(`Broadcasting on ${id}`);
  };

  actionCallback = (action, data = {}) => {
    if (
      !action.meta
      || !action.meta.callback
    ) return;

    this.emit({
      type: 'WORKER_CALLBACK',
      payload: {
        ...data,
      },
      meta: {
        callback: action.meta.callback,
      },
    });
  };

  workerReducer = (action) => {
    this.hook('workerPreReducer', action);

    const prevSetup = this.currentSetup;
    if (action.type !== 'UPDATE_DATA') {
      this.store.dispatch(action);
    }

    switch (action.type) {
      case 'APP_RELOAD':
        // eslint-disable-next-line no-console
        console.info('APP_RELOAD!');
        break;

      case 'UPDATE_WORKER_SCRIPTS':
        this.handleWorkerScriptChanges(prevSetup, action.payload);
        break;

      case 'REPLACE_LAYERS':
      case 'SORT_LAYERS':
      case 'EDIT_LAYER':
      case 'ADD_LAYER':
      case 'REMOVE_LAYER':
      case 'DISPLAY_SCALE':
      case 'TOGGLE_ANIMATION':
        this.broadcast(action);
        break;

      case 'SET_BPM':
        this.started = performance.now();
        this.data.bpm = action.payload.bpm;
        break;

      case 'SET_BROADCAST_ID':
        this.setBroadcastId(action.payload.id);
        break;

      case 'APPLY_SETUP':
        this.sendSetup(action.payload);
        this.notifySuccess('Setup applied');
        break;

      case 'BROADCAST':
        this.broadcast(action.payload);
        break;

      case 'UPDATE_DATA':
        this.data = {
          ...this.data,
          ...action.payload,
        };
        break;

      default:
        // eslint-disable-next-line no-console
        console.info('worker unknown action', action.type);
    }

    this.hook('workerPostReducer', action);
  };

  processData = () => {
    this.updateTime();

    this.hook('processData');

    this.executeAnimationScript();

    try {
      const payload = JSON.parse(JSON.stringify(this.data));
      this.broadcast({
        type: 'UPDATE_DATA',
        payload,
      });
    } catch (e) {
      // eslint-disable-next-line no-console
      console.warn(e);
    }

    setTimeout(this.processData, 1);
  };
}
