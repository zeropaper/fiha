import React from 'react';
import PropTypes from 'prop-types';
import { Provider } from 'react-redux';

import controllerPlugins from '../plugins/controllerPlugins';

import initWorker from './fiha.worker';
import store from './uiStore';
import Fiha from './components/fiha';

export default class FihaRedux extends React.Component {
  constructor(props) {
    super(props);
    this.worker = initWorker();
    this.plugins = controllerPlugins;
    this.store = store(controllerPlugins);
  }

  shouldComponentUpdate(nextProps) {
    const should = nextProps.renderTime !== this.props.renderTime;
    if (should) {
      this.worker.postMessage({
        type: 'APP_RELOAD',
      });
    }
    return should;
  }

  render() {
    // eslint-disable-next-line
    console.info('renderTime', this.props.renderTime);
    return (
      <Provider store={this.store}>
        <Fiha
          worker={this.worker}
          plugins={this.plugins}
        />
      </Provider>
    );
  }
}

FihaRedux.propTypes = {
  renderTime: PropTypes.string.isRequired,
};
