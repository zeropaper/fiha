import { createStore, applyMiddleware, combineReducers } from 'redux';
import reduxThunk from 'redux-thunk';

import initialState from './initialStoreState';

import appReducer from './reducers/app';
import editorReducer from './reducers/editor';
import layersReducer from './reducers/layers';
import workerScriptsReducer from './reducers/workerScripts';

const compileReducer = (plugins) => {
  const reducers = {
    layers: layersReducer,
    editor: editorReducer,
    app: appReducer,
    workerScripts: workerScriptsReducer,
  };

  plugins
    .filter(plugin => (plugin.storeId && plugin.reducer))
    .forEach((plugin) => {
      reducers[plugin.storeId] = plugin.reducer;
    });
  return combineReducers(reducers);
};

export default function configureStore(plugins) {
  const middleware = applyMiddleware(reduxThunk);

  const reducer = compileReducer(plugins);

  const store = createStore(
    reducer,
    initialState,
    middleware,
  );

  if (module.hot) {
    module.hot.accept(() => {
      store.replaceReducer(compileReducer(plugins));
    });
  }

  return store;
}
