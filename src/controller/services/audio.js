export default class AudioManagerService {
  constructor(mode = 'controller') {
    this.mode = mode;

    this.settings = {
      minDecibels: -180,
      maxDecibels: 120,
      smoothingTimeConstant: 0.85,
      fftSize: 1024,
    };

    this.buildAudio();
  }

  get frequencies() {
    const { analyser, freqArray } = this;
    analyser.getByteFrequencyData(freqArray);
    return Array.from(freqArray);
  }

  // aka timeDomain
  get volume() {
    const { analyser, volArray } = this;
    analyser.getByteTimeDomainData(volArray);
    return Array.from(volArray);
  }

  get data() {
    const { volume, frequencies } = this;
    return {
      volume,
      frequencies,
    };
  }

  resume = () => this.context.resume().then(() => {
    const analyser = this.context.createAnalyser();
    this.analyser = analyser;

    this.setupAnalyzer();
  });

  buildAudio = () => {
    const context = new AudioContext();
    this.context = context;
    const eventOpts = {
      capture: false,
      passive: true,
    };

    window.addEventListener('mousemove', this.resume, eventOpts);

    context.addEventListener('statechange', () => {
      if (context.state !== 'running') return;

      window.removeEventListener('mousemove', this.resume, eventOpts);
    }, eventOpts);
  };

  close = () => {
    // eslint-disable-next-line
    this.context.close().catch(err => console.warn(err));
  };

  setupAnalyzer = () => {
    const { analyser } = this;
    const {
      minDecibels,
      maxDecibels,
      smoothingTimeConstant,
      fftSize,
    } = this.settings;

    try {
      analyser.minDecibels = minDecibels;
      analyser.maxDecibels = maxDecibels;
      analyser.smoothingTimeConstant = smoothingTimeConstant;
      analyser.fftSize = fftSize;
    } catch (err) {
      // eslint-disable-next-line no-console
      console.warn(err);
    }

    this.freqArray = new Uint8Array(analyser.frequencyBinCount);
    this.volArray = new Uint8Array(analyser.frequencyBinCount);

    this.connectAudio();
  };

  connectAudio = () => {
    if (this.source) return;
    const capture = { audio: true };
    const { context, analyser } = this;

    const error = (err) => {
      // eslint-disable-next-line no-console
      console.warn(err);
    };
    const success = (stream) => {
      const source = context.createMediaStreamSource(stream);
      source.connect(analyser);
      this.source = source;
    };

    if (navigator.mediaDevices && navigator.mediaDevices.getUserMedia) {
      navigator.mediaDevices.getUserMedia(capture).then(success).catch(error);
    } else if (navigator.getUserMedia) {
      navigator.getUserMedia(capture, success, error);
    }
  };

  updateSettings = (settings) => {
    this.settings = {
      ...this.settings,
      ...settings,
    };

    this.setupAnalyzer();
  };
}
