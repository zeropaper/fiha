import querystring from 'querystring';

import blob2DataURI from '../../util/blob2DataURI';

export const readableFileType = (mimeType, filename) => [
  'text/plain',
  'image/svg',
  'image/svg+xml',
  'application/json',
].indexOf(mimeType) > -1 || [
  'gltf',
  'mtl',
  'obj',
].indexOf(filename.split('.').pop()) > -1;

export const getGistId = () => {
  const qs = querystring.parse(window.location.hash.slice(1));
  return qs.gid;
};

export const json2setup = (json) => {
  if (json.errors && json.errors.length) throw new Error(`Gist API error: ${json.errors[0].code}`);

  const layers = [];
  const meta = {
    layers: [],
    signal: {},
    assets: [],
    ...JSON.parse(json.files['fiha.json'].content),
  };

  Object.keys(meta.layers).forEach((id) => {
    const info = meta.layers[id];

    layers.push({
      id,
      type: info.type,
      setup: (json.files[info.setup] || {}).content || '',
      animation: (json.files[info.animation] || {}).content || '',
    });
  });

  const signal = {
    setup: (json.files[meta.signal.setup] || {}).content || '',
    animation: (json.files[meta.signal.animation] || {}).content || '',
  };

  const history = json.history.map(item => ({
    author: item.user.login,
    authorAvatar: item.user.avatar_url,
    updated: item.committed_at,
    version: item.version,
  }));

  const assetsPromises = meta.assets.map((name) => {
    const file = json.files[name];
    if (name.split('.').pop() === 'json') {
      return Promise.resolve(JSON.parse(file.content));
    }

    if (file.truncated) {
      return fetch(file.raw_url)
        .then(res => res.text())
        .then(text => ({
          name,
          type: file.type,
          dataURI: `data:${file.type};base64,${btoa(text)}`,
        }));
    }

    return Promise.resolve({
      dataURI: `data:${file.type};base64,${btoa(file.content)}`,
      name,
      type: file.type,
    });
  });

  return Promise
    .all(assetsPromises)
    .then(assets => ({
      signal,
      layers,
      assets,
      id: json.id,
      setup: { id: json.id },
      author: json.owner.login,
      authorAvatar: json.owner.avatar_url,
      description: json.description,
      created: json.created_at,
      updated: json.updated_at,
      comments: json.comments,
      forks: json.forks,
      history,
    }));
};

export const userQuery = `{
  viewer {
    login
    avatarUrl
    name
  }
}`;

export const listQuery = `{
  viewer {
    gists(orderBy: {field: CREATED_AT, direction: DESC}, last: 100, privacy: ALL) {
      nodes {
        files {
          name
        }
        id
        isPublic
        isFork
        description
        updatedAt
        createdAt
        pushedAt
        isPublic
      }
      pageInfo {
        startCursor
        endCursor
        hasNextPage
        hasPreviousPage
      }
      totalCount
    }
  }
  rateLimit {
    limit
    cost
    remaining
    resetAt
  }
}
`;

export const pluck = (arr, prop) => arr.map(item => item[prop]);

export const removed = (before, after) => before.filter(item => after.indexOf(item) < 0);

export const anonGistLoad = ({
  action,
  sendSetup,
  actionCallback = () => {},
  notifySuccess = () => {},
  notifyFailure = () => {},
}) => fetch(`https://api.github.com/gists/${action.payload.id}`)
  .then(response => response.json())
  .then(json2setup)
  .then((loaded) => {
    notifySuccess(`Gist "${action.payload.id}" loaded`);
    actionCallback(action, loaded);
    sendSetup(loaded);
  })
  .catch(notifyFailure);

export default class GistService {
  constructor(storage) {
    this.storage = storage;
  }

  get user() {
    return this.storage.getItem('gist');
  }

  get reqAuth() {
    return this.user
      .then(user => ({
        mode: 'cors',
        cache: 'no-cache',
        headers: {
          Authorization: `token ${user.token}`,
          'content-type': 'application/json',
        },
      }));
  }

  async credentials(info = {}) {
    if (info && info.token) {
      const response = await fetch('https://api.github.com/graphql', {
        method: 'POST',
        mode: 'cors',
        cache: 'no-cache',
        headers: {
          Authorization: `bearer ${info.token}`,
          'content-type': 'application/json',
        },
        body: JSON.stringify({ query: userQuery }),
      });

      const json = await response.json();
      const saved = {
        ...info,
        ...json.data.viewer,
      };

      return this.storage.setItem('gist', saved)
        .then(() => saved);
    }

    return this.storage.removeItem('gist');
  }

  list() {
    return this.user
      .then(user => fetch('https://api.github.com/graphql', {
        method: 'POST',
        mode: 'cors',
        cache: 'no-cache',
        headers: {
          Authorization: `bearer ${user.token}`,
        },
        body: JSON.stringify({ query: listQuery }),
      })
        .then(response => response.json()))
      .then((response) => {
        this.gists = response.data.viewer.gists.nodes
          .filter(gist => gist.files.find(file => file.name === 'fiha.json'));
        return this.gists;
      });
  }

  async fork(originalId, info) {
    return this.reqAuth
      .then(options => fetch(`https://api.github.com/gists/${originalId}/forks`, options)
        .then(response => response.json())
        .then(json => this.save({
          desciption: json.description,
          ...info,
          id: json.id,
        })));
  }

  processSave(info, remove = []) {
    const url = `https://api.github.com/gists${info.id ? `/${info.id}` : ''}`;
    const files = {};
    const meta = {
      api: 2,
      layers: {},
    };

    info.layers.forEach((layer) => {
      const id = layer.id.trim().replace(/[^a-z0-9]/ig, '-');

      if (layer.setup || layer.animation) {
        meta.layers[layer.id] = {
          active: layer.active,
          type: layer.type,
        };
      }

      if (layer.setup) {
        files[`layer__${id}__setup.js`] = { content: layer.setup };
        meta.layers[layer.id].setup = `layer__${layer.id}__setup.js`;
      }

      if (layer.animation) {
        files[`layer__${id}__animation.js`] = { content: layer.animation };
        meta.layers[layer.id].animation = `layer__${layer.id}__animation.js`;
      }
    });

    // instructs removal.. still needed?
    remove.forEach((id) => {
      files[`layer__${id}__setup.js`] = null;
      files[`layer__${id}__animation.js`] = null;
    });

    meta.signal = {};
    meta.signal.setup = 'signal__setup.js';
    files['signal__setup.js'] = { content: info.signal.setup || '/* empty */' };
    meta.signal.animation = 'signal__animation.js';
    files['signal__animation.js'] = { content: info.signal.animation || '/* empty */' };

    meta.assets = [];
    const assetPromises = info.assets.map(asset => fetch(asset.preview)
      .then((res) => {
        if (readableFileType(asset.type, asset.name)) {
          return res.text()
            .then((content) => {
              meta.assets.push(asset.name);
              files[asset.name] = { content };
            });
        }

        return res.blob()
          .then(blob => blob2DataURI(blob))
          .then((dataURI) => {
            let preview; // eslint-disable-line
            const assetName = `${asset.name}.json`;
            meta.assets.push(assetName);
            files[assetName] = { content: JSON.stringify({ ...asset, preview, dataURI }) };
          });
      }));

    return Promise.all(assetPromises)
      .then(() => {
        files['fiha.json'] = { content: JSON.stringify(meta, null, 2) };
      })
      .then(() => this.reqAuth
        .then(options => fetch(url, {
          ...options,
          method: info.id ? 'PATCH' : 'POST',
          body: JSON.stringify({
            public: info.public || false,
            description: info.description || 'Visual Fiha Setup',
            files,
          }),
        })
          .then(response => response.json())
          .then(json2setup)));
  }

  async save(info) {
    if (!info) throw new Error('Missing info');
    if (info.id) {
      const user = await this.user;
      const loaded = await this.load(info.id);

      // TODO: compare the loaded layers files with info.layers
      // to properly remove the file from the gist when obsolete
      if (loaded.author !== user.login) {
        return this.fork(info.id, info);
      }

      // update gist
      const remove = removed(pluck(loaded.layers, 'id'), pluck(info.layers, 'id'));
      return this.processSave(info, remove);
    }

    // save as new gist
    return this.processSave(info);
  }

  load(id) {
    return this.reqAuth
      .then(options => fetch(`https://api.github.com/gists/${id}`, {
        headers: options.headers,
      })
        .then(response => response.json())
        .then(json2setup));
  }
}
