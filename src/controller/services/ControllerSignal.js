const noop = () => {};

class ControllerSignal {
  constructor(emit, broadcast = noop, options = {}) {
    this.emit = emit;
    this.broadcast = broadcast;
    if (this.initialize) this.initialize(options);
  }
}

export default ControllerSignal;
