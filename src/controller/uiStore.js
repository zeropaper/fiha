import { createStore, applyMiddleware } from 'redux';
import reduxThunk from 'redux-thunk';
import { composeWithDevTools } from 'redux-devtools-extension';

import initialState from './initialStoreState';

const reducer = (state = initialState, { payload }) => ({
  ...state,
  ...payload,
});

export default function configureStore() {
  let middleware = applyMiddleware(reduxThunk);
  if (process.env.NODE_ENV !== 'production') {
    middleware = composeWithDevTools(middleware);
  }

  const store = createStore(
    reducer,
    initialState,
    middleware,
  );

  return store;
}
