const server = require('./server');

const host = process.env.HOST || '0.0.0.0';
const port = process.env.PORT || '9090';

server.listen(port, host, (err) => {
  if (err) {
    throw err;
  }
  // eslint-disable-next-line no-console
  console.info(`backend started on http://${host}:${port}`);
});
