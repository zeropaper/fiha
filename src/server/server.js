const fs = require('fs');
const path = require('path');
const express = require('express');
const cors = require('cors');

const actionCase = require('../util/actionCase');
// const workerReducer = require('../plugins/websocket/server/worker-reducer');

const {
  NODE_ENV,
  HOME,
  PUBLIC_URL: feOrigin = 'http://localhost:8080',
  FIHA_SETUP_REPOS_PATH,
} = process.env;

const reposDir = NODE_ENV === 'test'
  ? '/tmp'
  : (
    FIHA_SETUP_REPOS_PATH
    || path.join(HOME, 'fiha-repositories')
  );
const gitPlugin = require('../plugins/websocket/server/git-plugin');

const plugins = [gitPlugin];

const app = express();
const server = require('http').createServer(app);
const io = require('socket.io').listen(server, { origins: feOrigin });

app.use((req, res, next) => {
  res.setHeader('Access-Control-Allow-Credentials', 'true');
  next();
}, cors({
  origin: feOrigin,
}));

const jsonParser = express.json({
  limit: '200mb',
});

const filePath = ({
  repo,
  directory = 'assets',
  name,
}) => `${reposDir}/${repo}/${directory}/${name}`;

app.post('/upload', jsonParser, (req, res, next) => {
  try {
    const { repo, directory = 'assets', dataURI, name } = req.body;
    if (!dataURI) throw new Error(`No dataURI for ${name} (${repo}/${directory})`);
    fs.writeFileSync(filePath(req.body), dataURI.split('base64,').pop(), 'base64');
    res.status(204).end();
  } catch (err) { next(err); }
});

app.get('/download', (req, res, next) => res.sendFile(filePath(req.query), (err) => {
  if (err) next(err);
}));

const clientPath = path.dirname(require.resolve('socket.io-client/dist/socket.io'));
app.use(express.static(clientPath));
const assetsDir = path.resolve(__dirname, '../../assets');
app.use('/assets', express.static(assetsDir));
const publicDir = path.resolve(__dirname, '../../public');
if (fs.existsSync(publicDir) && NODE_ENV === 'production') {
  app.use('/', express.static(publicDir));
}

let workerSocket;

const sendWorker = (action) => {
  if (!workerSocket) {
    // eslint-disable-next-line no-console
    console.warn('no worker socket connection', action.type);
    return;
  }
  workerSocket.emit(action.type, action.payload);
};

const workerConnection = (socket) => {
  // eslint-disable-next-line no-console
  console.info('worker connection');
  if (workerSocket) {
    workerSocket.removeAllListeners();
  }
  workerSocket = socket;

  plugins.forEach((plugin) => {
    const actions = plugin.actions(sendWorker);
    Object.keys(actions).forEach((type) => {
      workerSocket.on(actionCase(type), actions[type]);
    });
  });

  plugins.forEach(plugin => plugin.onWorkerConnection(sendWorker));
};


const remoteConnection = (socket) => {
  sendWorker({
    type: 'NEW_REMOTE',
    payload: {
      id: socket.id,
    },
  });

  // socket.on('NEW_REMOTE', (payload) => {
  //   sendWorker('NEW_REMOTE', payload);
  // });

  socket.on('disconnect', () => {
    sendWorker('REMOVE_REMOTE', { id: socket.id });
  });

  socket.on('UPDATE_REMOTE', (payload) => {
    sendWorker({
      type: 'UPDATE_REMOTE',
      payload: {
        data: payload,
        id: socket.id,
      },
    });
  });
};

const isRemote = referer => referer && referer.includes('remote');

io.on('connection', (socket) => {
  const remote = isRemote(socket.handshake.headers.referer);
  if (remote) {
    remoteConnection(socket);
  } else {
    workerConnection(socket);
  }
});

module.exports = server;
