import paper from 'paper/dist/paper-full';
import ScriptableDisplayLayer from './scriptable-display-layer';

export const argumentNames = [];

export const apiReference = argumentNames.reduce((acc, val) => {
  acc[val] = acc[val] || {};
  return acc;
}, {});

export default class PaperDisplayLayer extends ScriptableDisplayLayer {
  constructor(props) {
    super(props);

    // inspired by https://github.com/aprowe/paper-loader/blob/master/index.js
    this.scope = new paper.PaperScope();
    paper.install(this.scope);

    this.scope.agent.chrome = false;
    this.scope.agent.firefox = false;

    this.setupArgNames = argumentNames;
    this.animationArgNames = argumentNames;

    this.offCanvas.setAttribute('data-paper-keepalive', 'true');

    this.initialize();
    this.execSetup();
  }

  componentDidMount() {
    super.componentDidMount();
    this.handleResize();
  }

  // eslint-disable-next-line camelcase
  UNSAFE_componentWillReceiveProps(nextProps) {
    const {
      setup,
      animation,
    } = this.props;

    const setupChanged = nextProps.setup && nextProps.setup !== setup;
    const animationChanged = nextProps.animation && nextProps.animation !== animation;

    if (setupChanged || animationChanged) {
      this.execSetup(nextProps);
    }
  }

  handleResize() {
    const { canvas } = this;
    if (!canvas) return;
    super.handleResize();
    this.execSetup();
  }

  compileSetup() {
    this.setupFunction = () => {};
  }

  execSetup(props = this.props) {
    if (!this.canvas) return;
    // needs to be done on a visible element in order
    // for paper.js to actaully render animations
    paper.setup(this.canvas);

    try {
      this.scope.execute(this.script(props));
    } catch (e) {
      console.warn(e); // eslint-disable-line
    }
  }

  compileAnimation() {
    this.animationFunction = () => {};
  }

  execAnimation() {} // eslint-disable-line

  script(props = this.props) {
    return `${props.setup}
function onFrame(event) {
  ${props.animation}
}`;
  }
}
