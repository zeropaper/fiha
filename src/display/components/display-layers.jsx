import React from 'react';
import PropTypes from 'prop-types';
import './display-layers.scss';

import layersReducer from '../../controller/reducers/layers';


const ThreeDisplayLayer = React.lazy(() => import('./three-display-layer'));
const CanvasDisplayLayer = React.lazy(() => import('./canvas-display-layer'));
const PaperDisplayLayer = React.lazy(() => import('./paper-display-layer'));


const {
  requestAnimationFrame,
  BroadcastChannel,
} = window;

const types = {
  three: ThreeDisplayLayer,
  canvas: CanvasDisplayLayer,
  paper: PaperDisplayLayer,
};

export function DisplayLayer(props) {
  const { id, invisible, type } = props;

  const LayerComponent = types[type];
  let className = 'display-layer';
  className += ` display-layer--${type}`;
  className += invisible ? ' display-layer--invisible' : '';

  if (LayerComponent) {
    return (
      <LayerComponent
        {...props}
        style={{ visibility: !invisible ? null : 'hidden' }}
        className={className}
      />
    );
  }

  return (
    <div className={className}>
      type {type} for {id} is unknown
    </div>
  );
}

DisplayLayer.propTypes = {
  id: PropTypes.string.isRequired,
  register: PropTypes.func.isRequired,
  type: PropTypes.string.isRequired,
  onExecutionError: PropTypes.func.isRequired,
  invisible: PropTypes.bool,
};

DisplayLayer.defaultProps = {
  invisible: null,
};

function LayerFallback() {
  return (<div style={{ display: 'none' }}>Loading...</div>);
}

const withKey = (Comp, register, onExecutionError, read, scale) => (props) => {
  const updatedProps = {
    ...props,
    animate: true,
    register,
    onExecutionError,
    read,
    scale,
  };

  // eslint-disable-next-line react/prop-types
  const key = props.id;
  return (
    <React.Suspense key={key} fallback={<LayerFallback />}>
      <Comp {...updatedProps} />
    </React.Suspense>
  );
};

export default class DisplayLayers extends React.Component {
  state = {
    scale: 1,
    layers: [],
    assets: [],
    fullscreen: false,
    animate: true,
  };

  componentDidMount() {
    if (this.broadcastChannel) return;

    this.broadcastChannel = new BroadcastChannel(this.props.broadcastChannelId);
    this.broadcastChannel.addEventListener('message', this.broadcastChannelListener, {
      passive: true,
      capture: false,
    });
    this.broadcastChannel.postMessage({
      type: 'REGISTER_DISPLAY',
      payload: {
        id: this.props.id,
      },
    });

    this.canvasEl = document.createElement('canvas');
    const { style } = this.canvasEl;
    style.zIndex = 1000;
    style.position = 'fixed';
    style.top = 0;
    style.left = 0;
    style.width = '100%';
    style.height = '100%';
    // if not set so, right-click menu doesn't offer casting...
    style.pointerEvents = 'none';
    style.background = 'black';
    style.transformOrigin = 'top left';
    document.body.appendChild(this.canvasEl);

    this.resizeListener();
    this.animate();
  }

  componentWillUnmount() {
    if (!this.broadcastChannel) return;
    this.broadcastChannel.close();
    this.broadcastChannel = null;
  }

  get activeLayers() {
    return (this.state.layers || [])
      .filter(layer => layer && layer.active !== false);
  }

  data = {};

  readCache = {};

  instances = {};

  register = (id, instance) => {
    this.instances[id] = instance;
  };

  refDom = (el) => {
    if (!el) {
      if (this.dom) {
        document.body.removeEventListener('dblclick', this.toggleFullscreen, {
          passive: true,
          capture: false,
        });
        window.removeEventListener('resize', this.resizeListener, {
          passive: true,
          capture: false,
        });
      }
    } else {
      document.body.addEventListener('dblclick', this.toggleFullscreen, {
        passive: true,
        capture: false,
      });
      window.addEventListener('resize', this.resizeListener, {
        passive: true,
        capture: false,
      });
      window.addEventListener('beforeunload', () => {
        if (this.broadcastChannel) {
          this.broadcastChannel.postMessage({
            type: 'REMOVE_DISPLAY',
            payload: {
              id: this.props.id,
            },
          });
        }
      }, { capture: false, passive: true });
    }
    this.dom = el;
  };

  resizeListener = () => {
    const { scale } = this.state;
    const width = this.canvasEl.clientWidth * scale * devicePixelRatio;
    const height = this.canvasEl.clientHeight * scale * devicePixelRatio;

    this.canvasEl.width = width;
    this.canvasEl.height = height;

    this.broadcastChannel.postMessage({
      type: 'RESIZE_DISPLAY',
      payload: {
        id: this.props.id,
        width,
        height,
      },
    });
  };

  toggleFullscreen = () => {
    const { fullscreen: prev } = this.state;
    if (!prev) {
      this.canvasEl.requestFullscreen()
        .then(() => this.setState({ fullscreen: true }, this.resizeListener))
        .catch(() => this.setState({ fullscreen: false }, this.resizeListener));
    } else {
      document.exitFullscreen()
        .then(() => this.setState({ fullscreen: false }, this.resizeListener))
        .catch(() => this.setState({ fullscreen: true }, this.resizeListener));
    }
  };

  eachInstances = (func) => {
    let instance;
    const keys = Object.keys(this.instances);
    for (let k = 0; k < keys.length; k += 1) {
      instance = this.instances[keys[k]];
      if (instance) func(instance, keys[k]);
    }
  };

  // eslint-disable-next-line consistent-return
  animate = () => {
    const { animate } = this.state;
    if (!animate) return requestAnimationFrame(this.animate);

    const x = 0;
    const y = 0;
    const w = this.canvasEl.width;
    const h = this.canvasEl.height;
    const ctx = this.canvasEl.getContext('2d');
    ctx.clearRect(x, y, w, h);

    [...this.activeLayers]
      .reverse()
      .forEach((layer) => {
        const instance = this.instances[layer.id];
        if (instance && layer.active !== false) {
          instance.execAnimation();
          if (instance.canvas.width && !layer.invisible) {
            ctx.drawImage(instance.canvas, x, y, w, h, x, y, w, h);
          }
        }
      });

    requestAnimationFrame(this.animate);
  };

  updateLayers = (data) => {
    const prevLayers = this.state.layers;
    const newLayers = layersReducer(prevLayers, data);
    const sameLayers = prevLayers === newLayers;
    if (!sameLayers) {
      this.setState({
        layers: newLayers,
      });
    }
  };

  // eslint-disable-next-line react/sort-comp
  onExecutionError = (layerRole, layerId) => (err, lineCorrection = 0) => {
    const { id } = this.props;
    if (!err) {
      return this.broadcastChannel.postMessage({
        type: 'UNSET_EXECUTION_ERROR',
        payload: {
          display: id,
          id: layerId,
          role: layerRole,
        },
      });
    }

    return this.broadcastChannel.postMessage({
      type: 'SET_EXECUTION_ERROR',
      payload: {
        display: id,
        id: layerId,
        role: layerRole,
        stack: err ? err.stack : '',
        lineCorrection,
      },
    });
  };

  broadcastChannelListener = (evt) => {
    const { type, payload } = evt.data;

    switch (type) {
      case 'CAPTURE_DISPLAY':
        this.captureImage();
        break;
      case 'CAPTURED_DISPLAY':
        break;

      case 'UPDATE_DATA':
        this.data = payload;
        break;

      case 'DISPLAY_SCALE':
        this.setState({ scale: payload.scale || 1 }, this.resizeListener);
        break;

      case 'REPLACE_LAYERS':
      case 'SORT_LAYERS':
      case 'EDIT_LAYER':
      case 'ADD_LAYER':
      case 'REMOVE_LAYER':
        this.updateLayers(evt.data);
        break;

      case 'ASSETS_UPDATE':
        this.setState(prevState => ({
          ...prevState,
          assets: evt.data.payload,
        }));
        break;

      case 'BROADCAST_FRAME':
      case 'REGISTER_DISPLAY':
      case 'RESIZE_DISPLAY':
      case 'REMOVE_DISPLAY':
        break;

      case 'TOGGLE_ANIMATION':
        this.setState(prev => ({ animate: !prev.animate }));
        break;

      default:
        // // eslint-disable-next-line no-console
        // console.warn('default update layer for', type);
    }
  };

  read = (id = '', defaultValue = 0, format = null) => {
    if (typeof this.readCache[id] !== 'undefined') return this.readCache[id];

    if (id.indexOf('asset:') === 0) {
      const assetId = id.slice('asset:'.length);
      const found = this.state.assets.find(asset => asset.name === assetId);
      if (!found) return defaultValue;

      if (found.type.indexOf('video/') === 0) {
        const vid = document.createElement('video');
        vid.loop = true;
        vid.muted = true;

        // this.readCache[id] = vid;
        vid.addEventListener('canplaythrough', () => {
          if (vid.paused) {
            vid.play();
          }
        });

        vid.src = found.dataURI || found.preview;
        return vid;
      }

      if (found.type.indexOf('image/') !== 0) return found;
      if (format === 'dataURI' && found.dataURI) return found.dataURI;

      const img = new Image();
      img.src = found.preview;
      this.readCache[id] = img;
      return img;
    }

    if (id.indexOf('layer:') === 0) {
      const layerId = id.slice('layer:'.length);
      const layer = this.instances[layerId];

      return layer && layer.canvas ? layer.canvas : null;
    }

    const { data } = this;
    if (!data || typeof data[id] === 'undefined') return defaultValue;
    return data[id];
  };

  captureImage = () => {
    this.broadcastChannel.postMessage({
      type: 'CAPTURED_DISPLAY',
      payload: {
        id: this.props.id,
        url: this.canvasEl.toDataURL(),
      },
    });
  };

  render() {
    const {
      register,
      onExecutionError,
      read,
      state: {
        scale,
      },
    } = this;
    const layerInstances = [...this.activeLayers]
      .reverse()
      .map(withKey(DisplayLayer, register, onExecutionError, read, scale));

    return (
      <div
        ref={this.refDom}
        className="display-layers"
      >
        {layerInstances}
      </div>
    );
  }
}

DisplayLayers.propTypes = {
  id: PropTypes.string.isRequired,
  broadcastChannelId: PropTypes.string.isRequired,
};
