import ScriptableDisplayLayer from './scriptable-display-layer';
import canvasTools from '../canvas-tools';
import { apiReference as mathToolsReference } from '../math-tools';
import miscTools, { apiReference as miscToolsReference } from '../misc-tools';


const initCtx = document.createElement('canvas').getContext('2d');
const canvasToolsObj = canvasTools(initCtx);
export const argumentNames = Object.keys(canvasToolsObj);

export const apiReference = argumentNames.reduce((acc, val) => {
  if (!acc[val]) {
    acc[val] = canvasToolsObj[val] ?
      {
        category: canvasToolsObj[val].category || 'canvas',
        type: canvasToolsObj[val].type || '',
        description: canvasToolsObj[val].description || '',
        link: canvasToolsObj[val].link || '',
        snippet: canvasToolsObj[val].snippet || '',
        usage: canvasToolsObj[val].usage || canvasToolsObj[val].snippet || '',
      } :
      {
        description: '',
        link: '',
        snippet: '',
        usage: '',
      };
  }
  return acc;
}, {
  ...mathToolsReference,
  ...miscToolsReference,
});

export default class CanvasDisplayLayer extends ScriptableDisplayLayer {
  constructor(props) {
    super(props);

    this.addContextMethods();

    this.initialize();
  }

  addContextMethods = () => {
    this.ctx = this.offCanvas.getContext('2d');
    const { ctx } = this;
    const tools = canvasTools(ctx);
    const toolNames = [...Object.keys(tools), ...Object.keys(miscTools)];
    toolNames.forEach((name) => {
      this[name] = tools[name] || miscTools[name];
    });

    this.setupArgNames = toolNames;
    this.animationArgNames = toolNames;
  }

  handleResize = () => {
    const { canvas, offCanvas } = this;
    if (!canvas) return;

    const width = canvas.parentNode.clientWidth;
    const height = canvas.parentNode.clientHeight;

    const scale = Math.max(Math.min(1, (this.props.scale || 0.5)), 0.5)
      * devicePixelRatio;
    offCanvas.width = width * scale;
    offCanvas.height = height * scale;
    canvas.width = width * scale;
    canvas.height = height * scale;

    this.ctx = offCanvas.getContext('2d');
    this.destinationCtx = canvas.getContext('2d');

    this.addContextMethods();
    this.destinationCtx = canvas.getContext('2d');

    this.execSetup();
  }

  reset() {
    this.handleResize();
  }
}

