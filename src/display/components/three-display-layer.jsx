import * as THREE from 'three';

import ScriptableDisplayLayer from './scriptable-display-layer';
import mathTools, { apiReference as mathToolsReference } from '../math-tools';
import miscTools, { apiReference as miscToolsReference } from '../misc-tools';
import logger from '../../logger';

if (typeof window !== 'undefined' && typeof window.THREE === 'undefined') {
  window.THREE = THREE;
  Promise.all([
    import('three/examples/js/loaders/GLTFLoader'),
    import('three/examples/js/loaders/DRACOLoader'),
    import('three/examples/js/loaders/SVGLoader'),
    import('three/examples/js/loaders/OBJLoader'),
    import('three/examples/js/loaders/MTLLoader'),
    // import('three/examples/js/loaders/ImageBitmapLoader'),
    import('three/examples/js/loaders/TTFLoader'),
    // import('three/examples/js/postprocessing/GlitchPass'),
  ])
    // eslint-disable-next-line no-console
    .catch(err => console.error(err.message));
}

const log = logger('SL', 'green');

const {
  devicePixelRatio,
} = window;

const toolNames = [...Object.keys(mathTools), ...Object.keys(miscTools)];
export const argumentNames = ['scene', 'camera', 'renderer', 'THREE'].concat(toolNames);

export const apiReference = {
  THREE: {
    category: 'three',
    type: 'object',
    description: 'The global THREE.js object',
    link: 'https://threejs.org/docs/index.html',
  },
  scene: {
    category: 'three',
    type: 'object',
    description: 'The global THREE.js scene',
    link: 'https://threejs.org/docs/index.html#api/scenes/Scene',
  },
  camera: {
    category: 'three',
    type: 'object',
    description: 'The default camera',
    link: 'https://threejs.org/docs/index.html#api/cameras/Camera',
  },
  renderer: {
    category: 'three',
    type: 'object',
    description: 'The default canvas renderer',
    link: 'https://threejs.org/docs/index.html#examples/renderers/CanvasRenderer',
  },
  ...mathToolsReference,
  ...miscToolsReference,
};

export const setupExample = `const grid = new THREE.GridHelper(20, 20);
const axes = new THREE.AxesHelper(3);

scene.add(grid);
scene.add(axes);

const ambientLight = new THREE.AmbientLight( 0xffffff );
ambientLight.name = 'ambientLight';
scene.add(ambientLight);

const directionalLight = new THREE.DirectionalLight(0xffdddd);
directionalLight.position.set(-25, 15, 20);
directionalLight.lookAt(0, 0, 0);
directionalLight.name = 'directionalLight';
scene.add(directionalLight);`;

export const animationExample = `camera.position.x = 10;
camera.position.y = 40;
camera.position.z = 10;

const now = read('now') * 0.0002;
camera.lookAt(sin(now), 0, cos(now));

renderer.render(scene, camera);`;

export default class ThreeDisplayLayer extends ScriptableDisplayLayer {
  constructor(props) {
    super(props);

    toolNames.forEach((name) => {
      this[name] = mathTools[name] || miscTools[name];
    });

    this.setupArgNames = argumentNames;
    this.animationArgNames = argumentNames;

    this.THREE = THREE;
    this.setupThree = this.setupThree.bind(this);
    this.initialize();
  }

  // componentDidMount(...args) {
  //   super.componentDidMount(...args);
  // }

  setupThree() {
    const { canvas, offCanvas } = this;
    if (!this.canvas) return;
    const { width, height } = canvas;

    log('setup THREE', canvas.width, offCanvas.width);

    this.renderer = this.renderer || new THREE.WebGLRenderer({
      alpha: true,
      canvas,
    });
    this.scene = this.scene || new THREE.Scene();
    this.camera = this.camera || new THREE.PerspectiveCamera(20, width / height, 1, 1000);

    const { renderer, camera } = this;

    renderer.setPixelRatio(devicePixelRatio);
    renderer.setSize(width, height);
    renderer.setClearColor(0x000000, 0);

    camera.position.z = 400;
    camera.position.x = 400;
    camera.position.y = 100;
    camera.lookAt(0, 0, 0);
  }

  handleResize() {
    const { canvas, camera, renderer } = this;
    if (!canvas) return;
    super.handleResize();

    const { width, height } = canvas;

    if (!camera) return;
    camera.aspect = width / height;
    camera.updateProjectionMatrix();
    renderer.setSize(width, height);
  }

  reset() {
    if (!this.canvas) return;

    if (this.aFRID) {
      log('reset cancel animation frame');
      cancelAnimationFrame(this.aFRID);
    }

    if (this.scene) {
      log('reset remove children');
      this.scene.remove(...this.scene.children);
    }

    this.setupThree();
    this.handleResize();
    this.execSetup();
  }
}

ThreeDisplayLayer.defaultSetupScript = `// ThreeJS setup script

camera.position.x = 10;
camera.position.y = 10;
camera.position.z = 10;
camera.setFocalLength(47);

camera.lookAt(0, 0, 0);

const ambientLight = new THREE.AmbientLight( 0x666666 );
ambientLight.name = 'ambientLight';
scene.add(ambientLight);


const directionalLight = new THREE.SpotLight(0xffffff, 1);
directionalLight.name = 'directionalLight';
scene.add(directionalLight);

directionalLight.angle = 40;
cache.directionalLight = directionalLight;

directionalLight.position.x = 5;
directionalLight.position.y = 7;
directionalLight.position.z = 7;

directionalLight.target.position.x = 0;
directionalLight.target.position.y = 0;
directionalLight.target.position.z = 0;
directionalLight.lookAt(0, 0, 0);


const grid = new THREE.GridHelper(20, 20);
const axes = new THREE.AxesHelper(3);

/*
*/
const directionalLightHelper = new THREE.SpotLightHelper(directionalLight);
cache.directionalLightHelper = directionalLightHelper;
scene.add(directionalLightHelper);
scene.add(grid);
scene.add(axes);`;

ThreeDisplayLayer.defaultAnimationScript = `// ThreeJS animation script

renderer.render(scene, camera);`;
