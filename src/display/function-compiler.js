import { JSHINT as jshint } from 'jshint/dist/jshint';

const noop = () => {};

const prologue = 'let global, frame, window, top, document, history;\n';

export function toCode(asyncFn, body, argumentNames) {
  return `${asyncFn ? 'async ' : ''}(${argumentNames.join(', ')}) => {
    'use strict';
    ${prologue}
    //
    ${body}
  }; // jshint ignore:line`;
}


const toleratedCodes = [
  'W033',
  'W032',
  'W087',
];

// TODO: find a way to detect undefined variables at validation
export function validateCode(body, argumentNames) {
  const now = performance.now();
  const scriptGlobals = argumentNames.reduce((acc, val) => { acc[val] = true; return acc; }, {});

  jshint(body, {
    esversion: 9,
    laxbreak: false,
  }, {
    ...scriptGlobals,
    cache: false,
  });
  // const messages = jshint.data().errors;
  // const errors = (messages || []).filter(err => err.code[0] !== 'W');

  const errors = (jshint.data().errors || [])
    .filter(err => toleratedCodes.indexOf(err.code) < 0);

  if (errors.length) {
    // eslint-disable-next-line no-console
    console.warn(`Error ${performance.now() - now}`, errors);
    const err = new Error('Validation error');
    err.details = errors;
    return err;
  }
  return null;
}

const printedErrors = {};
export default function functionCompiler({
  name,
  body,
  fallback = noop,
  onExecutionError = noop,
  argumentNames = [],
}) {
  let compiled;
  printedErrors[name] = typeof printedErrors[name] === 'undefined'
    ? true
    : printedErrors[name];

  const code = toCode(false, body, argumentNames);
  try {
    // eslint-disable-next-line
    compiled = (eval(code));
  } catch (evalError) {
    // eslint-disable-next-line no-console
    console.log('Evaluation error', evalError.message);
    compiled = fallback;
  }

  return (...args) => {
    let result;

    try {
      // eslint-disable-next-line prefer-spread
      result = compiled.apply(null, args);
      if (result instanceof Error) throw result;
      if (printedErrors[name]) onExecutionError();
      printedErrors[name] = false;
    } catch (execErr) {
      if (!printedErrors[name]) {
        // eslint-disable-next-line no-console
        console.log('Execution error', execErr.message);
        const lineCorrection = code.split('\n').length - body.split('\n').length - 1;
        onExecutionError(execErr, lineCorrection);
        printedErrors[name] = true;
      }
      result = execErr;
    }

    return result;
  };
}
