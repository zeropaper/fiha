export default function uniqueBy(arr, by) {
  const checked = [];
  return arr.reverse().reduce((acc, val) => {
    const id = val[by];
    if (checked.indexOf(id) < 0) {
      acc.push(val);
      checked.push(val[by]);
    }
    return acc;
  }, []);
}

