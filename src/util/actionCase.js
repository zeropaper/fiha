const snakeCase = require('lodash.snakecase');

module.exports = str => snakeCase(str).toUpperCase();

