/* eslint-disable */
const INIT_CWD = process && process.env && process.env.INIT_CWD;

export default function (scope, color) {
  return (...args) => {
    if (process && process.env && process.env.NODE_ENV === 'test') {
      return;
    } else if (INIT_CWD) {
      console.log(`[[${scope}]]: ${args.shift()}${args.length ? ':\n' : ''}`, ...args);
      return;
    }

    console.log(`%c[[${scope}]]: ${args.shift()}${args.length ? ':\n' : ''}`, `color:${color}`, ...args);
  };
}
