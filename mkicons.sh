#!/bin/bash
mkdir -p ./src/static/images/homescreen
inkscape -e ./src/static/favicon.png -w 16 -h 16 ./src/static/vf.icon.svg
inkscape -e ./src/static/images/homescreen/48.png -w 48 -h 48 ./src/static/vf.icon.svg
inkscape -e ./src/static/images/homescreen/72.png -w 72 -h 72 ./src/static/vf.icon.svg
inkscape -e ./src/static/images/homescreen/96.png -w 96 -h 96 ./src/static/vf.icon.svg
inkscape -e ./src/static/images/homescreen/144.png -w 144 -h 144 ./src/static/vf.icon.svg
inkscape -e ./src/static/images/homescreen/168.png -w 168 -h 168 ./src/static/vf.icon.svg
inkscape -e ./src/static/images/homescreen/192.png -w 192 -h 192 ./src/static/vf.icon.svg
inkscape -e ./src/static/images/homescreen/512.png -w 512 -h 512 ./src/static/vf.icon.svg
