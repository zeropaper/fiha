const { Neutrino } = require('neutrino');

module.exports = Neutrino({
  env: {
    NODE_ENV: 'production',
  },
})
  .use(`${__dirname}/.neutrinorc.js`)
  .call('eslintrc');
